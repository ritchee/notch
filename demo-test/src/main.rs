//use egui::{containers::Frame, epaint::*, plot::*};
use macroquad::prelude::*;
use notch::dynamics::{body::*, constraint::*, force::*, particle::*};
use notch::math;
use notch::shape::shape2d::*;
use std::convert::*;
use std::default::*;

const MASS: f32 = 50.0;
const REST_LEN: f32 = 200.0;
const LENGTH: f32 = 100.0;
const KS: f32 = 50.0;
const KD: f32 = 50.0;
const PI: f32 = std::f32::consts::PI;
const G_ACCEL: f32 = PI * PI;
/*
const DRAG: f32 = 15.0;
*/

fn window_conf() -> Conf {
    Conf {
        window_title: "test".to_owned(),
        window_width: 1920,
        window_height: 1080,
        ..Default::default()
    }
}

struct RigidBody {
    body: Body,
    spring: MassSpring,
    joint: Joint,
    local_r: Vec<math::Vec3>,
}

impl RigidBody {
    fn new() -> Self {
        let bb = AABB2d::new(
            math::Vec2::new(screen_width() / 2.0, 100.0),
            math::Vec2::new(LENGTH, LENGTH / 2.0),
        );
        let pivot = math::Vec3::new(screen_width() / 2.0, 5.0, 0.0);

        let mut spring = MassSpring::new(KS * 10.0, KD * 10.0, REST_LEN);
        spring.pivot = pivot;
        let joint = Joint::new(REST_LEN, pivot);

        let corners = bb.corners();
        let mut corners3: Vec<math::Vec3> = Vec::new();
        for i in 0..corners.len() {
            corners3.push(math::Vec3::from(corners[i]));
        }

        let body = Body::from_uniform(&corners3, MASS);

        for r in corners3.iter_mut() {
            *r -= body.r;
        }

        Self {
            body,
            spring,
            joint,
            local_r: corners3,
        }
    }

    fn update(&mut self, dt: f32) {
        let mut sys: Vec<Particle> = Vec::new();
        let g = Gravity::new(G_ACCEL * 4.0, math::Vec3::y());

        for r in self.local_r.iter() {
            let mut point = Particle::new(*r, MASS);
            point.r = self.body.transform(*r);
            point.v = self.body.velocity_rel(*r);
            sys.push(point);
        }

        for p in sys.iter_mut() {
            g.accumulate(p);
        }

        self.joint.accumulate(&mut sys[0]);
        self.spring.accumulate(&mut sys[0]);

        for p in sys.iter() {
            self.body.add_force(p.force, p.r);
        }

        self.body.integrate(dt);
    }

    fn draw(&self) {
        let mut corners = self.local_r.clone();

        let corners: Vec<math::Vec3> = corners
            .iter_mut()
            .map(|r| self.body.transform(*r))
            .collect();

        let r0 = corners[0];
        let mut diff = r0 - self.spring.pivot;
        let offset = (diff.len() - REST_LEN) / 2.0;
        diff.normalize();
        let begin = self.spring.pivot + offset * diff;
        let end = self.spring.pivot + (REST_LEN + offset) * diff;

        let math::Mat3 {
            v1: x,
            v2: y,
            v3: _,
        } = self.body.local_coords();

        let x = x * LENGTH / 2.0 + self.body.r;
        let y = y * LENGTH / 2.0 + self.body.r;
        draw_line(self.body.r.x, self.body.r.y, x.x, x.y, 3.0, RED);
        draw_line(self.body.r.x, self.body.r.y, y.x, y.y, 3.0, GREEN);

        draw_line(
            r0.x,
            r0.y,
            self.spring.pivot.x,
            self.spring.pivot.y,
            3.0,
            BLACK,
        );
        draw_line(begin.x, begin.y, end.x, end.y, 1.5, YELLOW);

        for i in 1..corners.len() {
            draw_line(
                corners[i - 1].x,
                corners[i - 1].y,
                corners[i].x,
                corners[i].y,
                3.0,
                BLACK,
            );
        }
        draw_line(
            corners[corners.len() - 1].x,
            corners[corners.len() - 1].y,
            corners[0].x,
            corners[0].y,
            3.0,
            BLACK,
        );

        for p in corners.iter() {
            draw_circle(p.x, p.y, 3.0, BLUE);
        }
        draw_circle(self.body.r.x, self.body.r.y, 4.0, BLUE);
    }
}

impl AsRef<Body> for RigidBody {
    fn as_ref(&self) -> &Body {
        &self.body
    }
}

impl AsMut<Body> for RigidBody {
    fn as_mut(&mut self) -> &mut Body {
        &mut self.body
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut pause = false;
    let mut body = RigidBody::new();

    loop {
        if is_key_pressed(KeyCode::Escape) {
            break;
        }

        if is_key_pressed(KeyCode::Space) {
            pause = !pause;
        }

        let dt = get_frame_time();

        if !pause {
            body.update(dt);
        }

        /*
        let m = math::Mat3::from(body.body.orient).transpose();
        let x_local = m.v1.normal();
        let y_local = m.v2.normal();
        let x_local = body.body.r + x_local * (LENGTH / 2.0);
        let y_local = body.body.r + y_local * (LENGTH / 2.0);
        */

        clear_background(WHITE);

        body.draw();

        next_frame().await
    }
}
