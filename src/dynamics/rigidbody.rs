use super::{particle::*, Body};
use crate::math::*;
use std::convert::*;
use std::default::Default;
use std::ops::Index;

/// dynamic RigidBody that has numerous point mass constituents
pub struct RigidBody {
    /// particle system
    pub p_system: ParticleSystem,
    pub body: Body,
    // initial local r of particles
    //initial_r: Vec<Vec3>,
    /// initial first point position, for debugging
    initial_r: Vec3,
}

impl RigidBody {
    /// # panic if particle system `p_system` has no point mass (particle)
    pub fn new(p_system: ParticleSystem) -> Self {
        if p_system.len() == 0 {
            panic!("particle system has 0 point mass!");
        }

        let mut body = Self {
            p_system,
            ..Self::default()
        };

        body.compute_center();
        body.initial_r = body.p_system[0].r - body.r;

        body
    }

    /*
    pub fn to_world(&self, r: Vec3) -> Vec3 {

    }
    */

    /// get local position of a particle with respect to body's CM and local
    /// coordinate axes
    ///
    /// use `get_relative` if you need coordinates in x, y, z
    pub fn get_local(&self, i: usize) -> Vec3 {
        let local_coords = self.local_coords();
        let r_rel = self.get_relative(i);

        Vec3::new(
            r_rel * local_coords.v1,
            r_rel * local_coords.v2,
            r_rel * local_coords.v3,
        )
    }

    /// get local position of a particle with respect to body's CM
    pub fn get_relative(&self, i: usize) -> Vec3 {
        self[i].r - self.r
    }

    /// compute the numerical drift/error of the body since initiation
    pub fn compute_error(&self) -> f32 {
        let r = self.orient.rotate(self.initial_r) + self.r;
        let error = r - self[0].r;

        error.x.max(error.y).max(error.z)
    }

    /// compute local coordiates of the body, local axes are the rows of the
    /// returned matrix since it's faster this way
    pub fn local_coords(&self) -> Mat3 {
        let x = Vec3::x();
        let y = Vec3::y();
        let z = Vec3::z();

        let local_x = self.orient.rotate(x);
        let local_y = self.orient.rotate(y);
        let local_z = self.orient.rotate(z);

        Mat3 {
            v1: local_x,
            v2: local_y,
            v3: local_z,
        }
    }

    /// add a point mass to the body
    pub fn push(&mut self, p: Particle) -> usize {
        self.p_system.push(p)
    }

    /// return an iterator to the internal particles that make up
    /// the body
    pub fn iter(&self) -> std::slice::Iter<'_, Particle> {
        self.p_system.iter()
    }

    /// return a mutable iterator to the internal particles that make up
    /// the body
    pub fn iter_mut(&mut self) -> std::slice::IterMut<'_, Particle> {
        self.p_system.iter_mut()
    }

    /// compute the center of mass and the orientation of the body
    fn compute_center(&mut self) {
        let mut m = 0.0;
        let mut r = Vec3::zero();
        let mut local_r: Vec<Vec3> = Vec::new();

        for p in self.p_system.iter() {
            m += p.mass;
            r += p.mass * p.r;
        }

        r /= m;
        self.r = r;
        self.mass = m;

        for p in self.p_system.iter() {
            local_r.push(p.r - r);
        }

        let mut ibody = Mat3::zero();
        for i in 0..local_r.len() {
            let t0 = local_r[i].len2() * Mat3::identity();
            let t1 = local_r[i].tensor();
            ibody += self.p_system[i].mass * (t0 - t1);
        }

        //self.ibody = ibody;
        self.ibody_inv = ibody.inverse();

        self.orient = Quat::versor(0.0, Vec3::z());
    }

    fn compute_body(&mut self, dt: f32) {
        let mut f = Vec3::zero();
        let mut t = Vec3::zero();
        let rot = Matrix3::from(self.orient);
        let old_r = self.r;

        self.p_system.compute_force();

        for p in self.p_system.iter() {
            f += p.force;
            t += (p.r - self.r).cross(p.force);
        }

        //let i_inv = rot * ibody.inverse() * rot.transpose();
        let i_inv = rot * self.ibody_inv * rot.transpose();
        let a = f / self.mass;
        let a_angular = i_inv * t;
        let kv = self.v + 0.5 * a * dt;
        let omega = self.omega + a_angular * dt;

        self.v += a * dt;
        self.r += kv * dt;

        let mut q = self.orient + (Quat::from(self.omega * dt) * self.orient) * 0.5;
        // it will build up errors at some point, so normalize it once it
        // reaches a threshold
        //
        // the angle does not change due to the fact that versor scaling
        // retains its rotation
        if q.w.abs() > 1.5 {
            q.normalize();
        }

        let q_diff = q.diff(self.orient);
        self.omega = omega;
        self.orient = q;

        // TODO: reduce rotation error from numerical drift
        // the buildup is very slow so it's fine for now

        for p in self.p_system.iter_mut() {
            p.r = q_diff.rotate(p.r - old_r) + self.r;
        }
    }

    pub fn integrate(&mut self, dt: f32) {
        if dt == 0.0 {
            return;
        }

        //let dt = self.p_system.adapt_step(dt);

        self.compute_body(dt);
        self.clear_force();
    }

    pub fn clear_force(&mut self) {
        for p in self.p_system.iter_mut() {
            p.clear_force();
        }
    }
}

impl Default for RigidBody {
    fn default() -> Self {
        Self {
            p_system: ParticleSystem::new(),
            mass: 1.0,
            r: Vec3::zero(),
            v: Vec3::zero(),
            orient: Quat::zero(),
            p_angular: Vec3::zero(),
            omega: Vec3::zero(),
            //initial_r: Vec::new(),
            //ibody: Mat3::identity(),
            ibody_inv: Mat3::identity(),
            initial_r: Vec3::zero(),
        }
    }
}

impl From<&RigidBody> for Mat4 {
    fn from(body: &RigidBody) -> Self {
        let mut r = Mat4::from(body.orient);

        r.v1.w = body.r.x;
        r.v2.w = body.r.y;
        r.v3.w = body.r.z;

        r
    }
}

impl AsRef<ParticleSystem> for RigidBody {
    fn as_ref(&self) -> &ParticleSystem {
        &self.p_system
    }
}

impl AsMut<ParticleSystem> for RigidBody {
    fn as_mut(&mut self) -> &mut ParticleSystem {
        &mut self.p_system
    }
}

impl Index<usize> for RigidBody {
    type Output = Particle;

    fn index(&self, i: usize) -> &Self::Output {
        &self.p_system[i]
    }
}
