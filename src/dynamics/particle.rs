use crate::dynamics::force::*;
use crate::math::vec3::*;
use std::borrow::*;
use std::cell::*;
use std::collections::HashMap;
use std::convert::*;
use std::ops::Index;
use std::vec::*;

/// point mass or particle structure
#[derive(Debug, Default, Copy, Clone)]
pub struct Particle {
    /// position
    pub r: Vec3,
    /// velocity
    pub v: Vec3,
    //a: Vec3,
    /// mass
    pub mass: f32,
    /// accumulated force
    pub force: Vec3,
}

impl Particle {
    /// new particle with initial position, velocity, and mass
    /// mass must be greater than 0, use set_free() is a particle is independent
    pub fn new(r: Vec3, mass: f32) -> Self {
        assert!(mass > 0.0);

        Self {
            r,
            v: Vec3::zero(),
            //a: Vec3::zero(),
            mass,
            force: Vec3::zero(),
        }
    }

    /// integrate a particle assuming forces haved been applied
    pub fn integrate(&mut self, dt: f32) {
        if self.free() {
            return;
        }

        let a = self.force / self.mass;

        /*
        // Verlet integration
        let r = self.r + self.v * dt + 0.5 * self.a * dt * dt;
        let v = self.v + 0.5 * (self.a + a) * dt;

        self.r = r;
        self.v = v;
        self.a = a;

        // 3rd order Runge Kutta, less stable than 2nd order numerically
        // should be more precise than 2nd order in theory, but produces errors
        // in step size more frequently due to a significant amount of
        // computation that it goes through
        let v = self.v;
        let r = self.r;
        let kv = a * dt;
        let k1r = v * dt;
        let k2r = k1r + 0.5 * kv * dt;
        let k3r = k1r - kv * dt;

        self.v = v + kv;
        self.r = r + (k1r + 4.0 * k2r + k3r) / 6.0;
        */

        // 2nd order Runge Kutta aka midpoint method
        // very fast and stable
        let kv = self.v + 0.5 * a * dt;
        self.v += a * dt;
        self.r += kv * dt;
    }

    /// Try integrate a particle, is the particle is free, None is returned
    /// else integrated position and velocity are returned, respectively
    pub fn try_integrate(&self, dt: f32) -> Option<(Vec3, Vec3)> {
        if self.free() || self.force == Vec3::zero() {
            None
        } else {
            let a = self.force / self.mass;
            let kv = self.v + 0.5 * a * dt;
            let v = self.v + a * dt;
            let r = self.r + kv * dt;
            Some((r, v))
        }
    }

    /// Check if a particle is valid, since the mass of a free particle is
    /// 0.0, any operation that involves inverse mass will be faulty
    pub fn free(&self) -> bool {
        self.mass <= 0.0
    }

    /// Change a particle state to free
    pub fn set_free(&mut self) {
        if self.mass > 0.0 {
            self.mass = -self.mass;
        }
    }

    /// Change a particle state from free to integrable
    pub fn reset(&mut self) {
        if self.mass <= 0.0 {
            self.mass = -self.mass;
        }
    }

    /// return acceleration
    pub fn accel(&self) -> Vec3 {
        if self.mass > 0.0 {
            self.force / self.mass
        } else {
            Vec3::zero()
        }
    }

    /// add impulse to particle
    pub fn add_impulse(&mut self, i: Vec3) {
        self.v += i;
    }

    /// add a force
    pub fn add_force(&mut self, f: Vec3) {
        self.force += f;
    }
    /// clear accumulated force
    pub fn clear_force(&mut self) {
        self.force = Vec3::zero();
    }
}

pub(crate) enum Applicable {
    Indices(Vec<usize>),
    Global,
}

impl From<&[usize]> for Applicable {
    fn from(indices: &[usize]) -> Self {
        Self::Indices(Vec::from(indices))
    }
}

pub(crate) struct ForcePair {
    pub(crate) force: Box<dyn Accumulator>,
    pub(crate) affected: Applicable,
}

impl ForcePair {
    pub(crate) fn with_indices(force: Box<dyn Accumulator>, indices: &[usize]) -> Self {
        Self {
            force,
            affected: Applicable::Indices(Vec::from(indices)),
        }
    }

    pub(crate) fn global(force: Box<dyn Accumulator>) -> Self {
        Self {
            force,
            affected: Applicable::Global,
        }
    }

    pub(crate) fn append(&mut self, indices: &[usize]) {
        match self.affected {
            Applicable::Indices(ref mut affected) => affected.append(&mut Vec::from(indices)),
            _ => self.affected = Applicable::Indices(Vec::from(indices)),
        }
    }

    #[allow(dead_code)]
    pub(crate) fn is_global(&self) -> bool {
        match self.affected {
            Applicable::Global => true,
            _ => false,
        }
    }

    pub(crate) fn set_global(&mut self) {
        self.affected = Applicable::Global;
    }
}

/// a system that is responsible for managing particles
///
/// WARNING: under no circircumstance is a particle moved or removed from the
/// TODO: add adaptive step size to euler step
#[derive(Default)]
pub struct ParticleSystem {
    pub(crate) particles: Vec<Particle>,
    pub(crate) force_registry: RefCell<HashMap<String, ForcePair>>,
    pub(crate) force_once: Vec<ForcePair>,
    /// error in step size, call adapt_step to compute this value
    pub step_error: f32,
}

impl ParticleSystem {
    pub fn new() -> Self {
        ParticleSystem {
            particles: Vec::new(),
            force_registry: HashMap::new().into(),
            force_once: Vec::new(),
            step_error: 0.0,
        }
    }

    /// compute the force that each particle is being influenced by
    /// applying all forces in the registry on each particle registered
    /// with them
    pub fn compute_force(&mut self) {
        for (_, force_pair) in self.force_registry.borrow().iter() {
            match force_pair.affected {
                Applicable::Global => force_pair.force.accumulate_slice(&mut self.particles),
                Applicable::Indices(ref affected) => {
                    // TODO: change this to get_many_mut() once it's in
                    // stable rust
                    let mut particle_slice: Vec<Particle> = Vec::new();
                    for i in affected.iter() {
                        particle_slice.push(self.particles[*i]);
                    }

                    force_pair.force.accumulate_slice(&mut particle_slice);

                    for (i, index) in affected.iter().enumerate() {
                        self.particles[*index].add_force(particle_slice[i].force);
                    }
                }
            }
        }

        let force_once = self.force_once.split_off(0);
        self.force_once.shrink_to_fit();

        for force_pair in force_once.into_iter() {
            match force_pair.affected {
                Applicable::Global => force_pair.force.accumulate_slice(&mut self.particles),
                Applicable::Indices(ref affected) => {
                    // TODO: change this to get_many_mut() once it's in
                    // stable rust
                    let mut particle_slice: Vec<Particle> = Vec::new();
                    for i in affected.iter() {
                        particle_slice.push(self.particles[*i]);
                    }

                    force_pair.force.accumulate_slice(&mut particle_slice);

                    for (i, index) in affected.iter().enumerate() {
                        self.particles[*index].add_force(particle_slice[i].force);
                    }
                    /*
                    self
                    .particles
                    .iter_mut()
                    .enumerate()
                    .filter(|(i, _)| {
                    if let Ok(_) = affected.binary_search(&i) {
                    true
                    } else {
                    false
                    }
                    })
                    .map(|(_, p_ref)| p_ref)
                    .collect();
                    */

                    /*
                    for (i, index) in affected.iter().enumerate() {
                    self.particles[*index].add_force(particle_slice[i].force);
                    }
                    */
                }
            }
        }
    }

    /// clear all force on each particle
    pub fn clear_force(&mut self) {
        for p in self.particles.iter_mut() {
            p.clear_force();
        }
    }

    #[allow(dead_code)]
    pub fn reduce_and_integrate(&mut self, dt: f32) {
        struct OldState {
            r: Vec3,
            v: Vec3,
            r_full: Vec3,
            force: Vec3,
        }

        if dt == 0.0 {
            return;
        }

        // make a state vector for half step integration
        let mut old_state: Vec<OldState> = Vec::new();
        let half_dt = dt / 2.0;
        let mut error_avg: f64 = 0.0;

        self.clear_force();

        self.compute_force();
        for p in self.particles.iter_mut() {
            // save the initial state
            let mut state = OldState {
                r: p.r,
                v: p.v,
                r_full: Vec3::zero(),
                force: p.force,
            };

            // integrate full step
            p.integrate(dt);

            // save first r
            state.r_full = p.r;

            p.clear_force();

            // restore state
            p.r = state.r;
            p.v = state.v;

            old_state.push(state);
        }

        for (i, p) in self.particles.iter_mut().enumerate() {
            // restore the initial force
            p.force = old_state[i].force;

            // integrate half step
            p.integrate(half_dt);
            p.clear_force();
        }

        // compute force at half step
        self.compute_force();
        for (i, p) in self.particles.iter_mut().enumerate() {
            // integrate at half step again
            p.integrate(half_dt);
            p.clear_force();

            // compute the error in the output r
            let r_diff = (p.r - old_state[i].r_full).abs();
            let error = r_diff.x.max(r_diff.y).max(r_diff.z);

            let dt = if error > 1e-2 {
                println!("step error {} in particle {}", error, i);
                // might need to clamp the scale if it's huge
                let tolerance = 1e-4;
                let scale = (tolerance / error).sqrt();

                dt * scale
            } else {
                dt
            };

            p.r = old_state[i].r;
            p.v = old_state[i].v;
            p.force = old_state[i].force;
            p.integrate(dt);

            error_avg += dt as f64;
        }

        self.step_error = (error_avg / self.particles.len() as f64) as f32;
    }

    /// a function that step size based on the error between a full step
    /// integration and a half step integration twice
    ///
    /// `p` is a particle that needs step error reducing with the system's
    /// forces
    pub fn adapt_step(&mut self, dt: f32, p: &mut Particle) -> f32 {
        if dt == 0.0 {
            return dt;
        }

        // begin computing adaptive step size
        let half_dt = dt / 2.0;

        let r = p.r;
        let v = p.v;
        let mut r_diff: Vec3;

        p.clear_force();

        {
            let force_reg = self.force_registry.borrow();
            // compute r at full step size
            for (_, f) in force_reg.iter() {
                f.force.accumulate(p);
            }
            p.integrate(dt);
            r_diff = p.r;
            p.r = r;
            p.v = v;

            // compute r after two steps
            p.integrate(half_dt);
            p.clear_force();
            for (_, f) in force_reg.iter() {
                f.force.accumulate(p);
            }
            p.integrate(half_dt);
        }

        r_diff = (r_diff - p.r).abs();
        let error = r_diff.x.max(r_diff.y).max(r_diff.z);

        let dt = if error > 1e-2 {
            // might need to clamp the scale if it's huge
            let tolerance = 1e-4;
            let scale = (tolerance / error).sqrt();

            dt * scale
        } else {
            dt
        };

        p.r = r;
        p.v = v;

        dt
    }

    /// update the entire particle system using Euler step
    /// procedure:
    ///     compute adaptive step size (not anymore)
    ///     clear previously accumulated forces
    ///     compute new accumulated force
    ///     compute acceleration with euler step
    ///     update each particle with its computed acceleration
    pub fn update(&mut self, dt: f32) {
        self.clear_force();

        // compute force and acceleration
        self.compute_force();

        // integrate
        for p in self.particles.iter_mut() {
            p.integrate(dt);
        }

        //self.reduce_and_integrate(dt);
    }

    /// add particle to the system
    ///
    /// return the index of the particles as id
    /// it will become invalid after the internal particles vector is modified
    pub fn push(&mut self, p: Particle) -> usize {
        let id = self.particles.len();
        self.particles.push(p);

        id
    }

    /// get a reference of a particle from the system
    pub fn get(&self, index: usize) -> &Particle {
        if index > self.particles.len() {
            panic!("index is out of range");
        }

        &self.particles[index]
    }

    /// get a mutable reference of a particle from the system
    pub fn get_mut(&mut self, index: usize) -> &mut Particle {
        if index > self.particles.len() {
            panic!("index is out of range");
        }

        &mut self.particles[index]
    }

    /// borrow particles slice
    pub fn borrow(&self) -> &Vec<Particle> {
        &self.particles
    }

    /// borrow particles slice mutably
    pub fn borrow_mut(&mut self) -> &mut Vec<Particle> {
        &mut self.particles
    }

    /// iterate each particle
    pub fn iter(&self) -> std::slice::Iter<'_, Particle> {
        self.particles.iter()
    }

    /// iterate each particle mutably
    pub fn iter_mut(&mut self) -> std::slice::IterMut<'_, Particle> {
        self.particles.iter_mut()
    }

    /// get the number of particles currently in the system
    pub fn len(&self) -> usize {
        self.particles.len()
    }

    /// insert a force of the system
    /// forces are identified with a name, e.g: 'gravity', 'drag', etc..
    /// user is expected to remember those names
    ///
    /// `name`: name of the force
    /// `force`: a force that has Accumulator traits
    /// `indices`: ids of particles that are affected, None for global
    pub fn add_force<T: Accumulator + 'static>(
        &mut self,
        name: &str,
        force: T,
        indices: Option<&[usize]>,
    ) {
        let name = String::from(name);
        let force = Box::new(force);

        let force_pair = if let Some(indices) = indices {
            ForcePair::with_indices(force, indices)
        } else {
            ForcePair::global(force)
        };

        self.force_registry.borrow_mut().insert(name, force_pair);
    }

    /// modify a force of the system
    /// forces are identified with a name, e.g: 'gravity', 'drag', etc..
    /// user is expected to remember those names
    ///
    /// if force is not in the system, it is added
    pub fn set_force<T: Accumulator + 'static>(&mut self, name: &str, force: T) {
        let name = String::from(name);
        let force = Box::new(force);
        let mut force_reg = self.force_registry.borrow_mut();
        if let Some(force_pair) = force_reg.get_mut(&name) {
            force_pair.force = force;
        } else {
            force_reg.insert(String::from(name), ForcePair::global(force));
        }
    }

    /// remove a force from the system
    pub fn remove_force(&mut self, name: &str) {
        let name = String::from(name);
        self.force_registry.borrow_mut().remove(&name);
    }

    /// push a pair of force and its affected particles
    ///
    /// `name`: name of the force
    /// `indices`: indices of the affected particles
    ///
    /// some forces are required to be accompanied by a certain number of
    /// particles
    ///
    /// # panic if ids are greater than the length of particles within the
    /// system, that is, the particle has not been added to the system yet.
    pub fn push_indices(&mut self, name: &str, indices: &[usize]) {
        let name = String::from(name);
        for id in indices.iter() {
            if *id > self.particles.len() {
                panic!("particle {} is not in the system", id);
            }
        }

        let mut force_reg = self.force_registry.borrow_mut();

        if let Some(force_pair) = force_reg.get_mut(&name) {
            force_pair.append(indices);
        }
    }

    /// change a force to global
    pub fn set_force_global(&mut self, name: &str) {
        let name = String::from(name);
        let mut force_reg = self.force_registry.borrow_mut();

        if let Some(force_pair) = force_reg.get_mut(&name) {
            force_pair.set_global();
        }
    }

    /// add a force that is only applied once, this can forces such as
    /// impulse or normal
    pub fn add_once_force<T: Accumulator + 'static>(
        &mut self,
        force: T,
        indices: Option<&[usize]>,
    ) {
        let force = Box::new(force);
        let force_pair = if let Some(indices) = indices {
            ForcePair::with_indices(force, indices)
        } else {
            ForcePair::global(force)
        };

        self.force_once.push(force_pair);
    }

    pub fn compute_cm(&self) -> Vec3 {
        let mut m = 0.0;
        let mut r = Vec3::zero();

        for p in self.particles.iter() {
            r += p.mass * p.r;
            m += p.mass;
        }

        r / m
    }
}

impl Index<usize> for ParticleSystem {
    type Output = Particle;

    fn index(&self, i: usize) -> &Self::Output {
        &self.particles[i]
    }
}

impl AsRef<Vec<Particle>> for ParticleSystem {
    fn as_ref(&self) -> &Vec<Particle> {
        &self.particles
    }
}

impl AsMut<Vec<Particle>> for ParticleSystem {
    fn as_mut(&mut self) -> &mut Vec<Particle> {
        &mut self.particles
    }
}

impl Borrow<Vec<Particle>> for ParticleSystem {
    fn borrow(&self) -> &Vec<Particle> {
        &self.particles
    }
}

impl BorrowMut<Vec<Particle>> for ParticleSystem {
    fn borrow_mut(&mut self) -> &mut Vec<Particle> {
        &mut self.particles
    }
}
