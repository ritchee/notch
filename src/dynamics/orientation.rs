use crate::math::*;
use std::default::Default;

pub struct Orientation {
    pub y: Vec3,
    pub z: Vec3,
}

impl Orientation {
    pub fn new(up: Vec3, tangent: Vec3) -> Self {
        let mut config = Self {
            y: up,
            z: Vec3::zero(),
        };

        config.y.normalize();

        if (up * tangent).eq_zero() {
            config.z = tangent.normal();
        } else {
            config.z = if up.x == 0.0 && up.y == 1.0 && up.z == 0.0 {
                config.y.cross(Vec3::z()).normal()
            } else {
                config.y.cross(Vec3::y()).normal()
            };
        }

        config
    }

    pub fn world() -> Self {
        Self {
            y: Vec3::y(),
            z: Vec3::z(),
        }
    }

    pub fn x(&self) -> Vec3 {
        self.y.cross(self.z)
    }

    pub fn rotate_at(&mut self, angle: f32, pivot: Vec3) {
        let q = Quat::versor(angle, pivot);

        self.y = q.rotate(self.y);
        self.z = q.rotate(self.z);
    }

    pub fn replace_y(&mut self, y: Vec3) {
        self.y.normalize();

        self.z = if y.x == 0.0 && y.y == 1.0 && y.z == 0.0 {
            self.y.cross(Vec3::z()).normal()
        } else {
            self.y.cross(Vec3::y()).normal()
        };
    }

    pub fn to_matrix(&self) -> Mat3 {
        let x = self.x();
        Mat3::from_columns(x, self.y, self.z)
    }
}

impl From<Mat3> for Orientation {
    fn from(mat: Mat3) -> Self {
        let y = Vec3::new(mat[1].x, mat[1].y, mat[1].z).normal();
        let z = Vec3::new(mat[2].x, mat[2].y, mat[2].z).normal();

        Self { y, z }
    }
}

impl Default for Orientation {
    fn default() -> Self {
        Self::world()
    }
}
