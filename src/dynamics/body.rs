use super::particle::*;
use crate::math::*;
use std::default::Default;

/// Dynamic body that has numerous point mass constituents
///
/// Body always uses local coordinates, so make sure a body's initial local
/// point mass positions are in local coordinate by subtracting them with
/// body CM and stored somewhere like an Rc or your custom body. Check
/// `transform` for an example.
///
/// Computing their current coordinate is very simple and precise by calling
/// `body.transform` on each point mass
pub struct Body {
    /// Total mass
    pub mass: f32,
    /// CM (Center of mass)
    pub r: Vec3,
    /// Linear velocity
    pub v: Vec3,
    /// World orientation
    pub orient: Quat,
    /// Angular momentum
    pub p_angular: Vec3,
    /// Rotational pivot
    pub omega: Vec3,
    /// Inverse of inertia tensor
    pub ibody_inv: Mat3,
    /// Total force on body
    pub force: Vec3,
    /// Total torque on body
    pub torque: Vec3,
}

impl Body {
    /// # panic if `mass` is not positive
    pub fn new(r: Vec3, mass: f32) -> Self {
        if mass < 0.0 {
            panic!("Body mass is not positive");
        }

        Self {
            r,
            mass,
            ..Self::default()
        }
    }

    /// Construct a body from uniform point masses
    /// # panic if `point_mass` is not positive
    pub fn from_uniform(points: &[Vec3], point_mass: f32) -> Self {
        let mut r = Vec3::zero();
        let m = point_mass * points.len() as f32;
        let mut local_r: Vec<Vec3> = Vec::new();

        for p in points.iter() {
            r += point_mass * *p;
        }

        r /= m;
        let mut body = Self::new(r, m as f32);

        for p in points.iter() {
            local_r.push(Vec3::from(*p) - r);
        }

        let mut ibody = Mat3::zero();
        for i in 0..local_r.len() {
            let t0 = local_r[i].len2() * Mat3::identity();
            let t1 = local_r[i].tensor();
            ibody += point_mass * (t0 - t1);
        }

        body.ibody_inv = ibody.inverse();

        body
    }

    /// Construct a body from probably nonuniform point masses
    /// `points` is a slice of Vec4 where w is the mass and first 3 is
    /// the world position
    /// # panic if `point_mass` is not positive
    pub fn from_point_masses(points: &[Vec4]) -> Self {
        let mut m = 0.0;
        let mut r = Vec3::zero();
        let mut local_r: Vec<Vec3> = Vec::new();

        for p in points.iter() {
            m += p.w;
            r += p.w * Vec3::from(*p);
        }

        r /= m;
        let mut body = Self::new(r, m);

        for p in points.iter() {
            local_r.push(Vec3::from(*p) - r);
        }

        let mut ibody = Mat3::zero();
        for i in 0..local_r.len() {
            let t0 = local_r[i].len2() * Mat3::identity();
            let t1 = local_r[i].tensor();
            ibody += points[i].w * (t0 - t1);
        }

        body.ibody_inv = ibody.inverse();

        body
    }

    /// compute local coordiates of the body, local axes are the rows of the
    /// returned matrix since it's faster this way
    pub fn local_coords(&self) -> Mat3 {
        let x = Vec3::x();
        let y = Vec3::y();
        let z = Vec3::z();

        let local_x = self.orient.rotate(x);
        let local_y = self.orient.rotate(y);
        let local_z = self.orient.rotate(z);

        Mat3 {
            v1: local_x,
            v2: local_y,
            v3: local_z,
        }
    }

    pub fn add_force(&mut self, force: Vec3, contact_point: Vec3) {
        self.force += force;
        self.torque += (contact_point - self.r).cross(force);
    }

    pub fn integrate(&mut self, dt: f32) {
        let rot = Mat3::from(self.orient);
        let i_inv = rot * self.ibody_inv * rot.transpose();
        let a = self.force / self.mass;
        let a_angular = i_inv * self.torque;
        let kv = self.v + 0.5 * a * dt;
        let omega = self.omega + a_angular * dt;

        self.v += a * dt;
        self.r += kv * dt;

        //let mut q = self.orient + (Quat::from(self.omega * dt) * self.orient) * 0.5;
        let mut q = self.orient;
        q.add_rotation(self.omega * dt);

        // it will build up errors at some point, so normalize it
        //
        // the angle does not change due to the fact that versor scaling
        // retains its rotation
        /*
        if q.w.abs() > 1.5 {
            q.normalize();
        }
        */

        //let q_diff = q.diff(self.orient);
        self.omega = omega;
        self.orient = q;

        //println!("omega: {}", omega);

        // TODO: reduce rotation error from numerical drift
        // the buildup is very slow so it's fine for now

        self.clear_force();
    }

    /// Compute the relative velocity of a point of the body
    /// `r` is in local coordinate
    pub fn velocity_rel(&self, r: Vec3) -> Vec3 {
        //self.v + self.omega.cross(r - self.r)
        self.v + self.omega.cross(r)
    }

    /// Clear body force and torque
    pub fn clear_force(&mut self) {
        self.force = Vec3::zero();
        self.torque = Vec3::zero();
    }

    /// Convert body transform to homogeneous matrix
    pub fn to_matrix(&self) -> Mat4 {
        Mat4::from(self)
    }

    /// Transform a point local to body CM to world coordinate
    /// Use `Mat4::from(&body)` or `to_matrix` for the equivalent matrix
    /// The reason the argument is in local coordinate is that the body does not
    /// store initial CM, which means that a point will remain the same after
    /// this transformation
    ///
    /// # Example:
    /// ```
    /// let body = Body::new(&world_r, some_mass);
    /// // compute local point masses to store in a rigid body
    /// let local_r = world_r.iter().map(|r| r - body.r).collect();
    /// let rigidbody = SomeRigidBody { body, local_r, ..default() };
    /// // drawing/doing something that needs current world coordinates
    /// for r in rigidbody.local_r.iter() {
    ///     let r_world = rigidbody.body.transform(*r);
    ///     some_draw_fn(r_world, some_color);
    /// }
    /// ```
    pub fn transform(&self, r: Vec3) -> Vec3 {
        self.orient.rotate(r) + self.r
    }

    pub fn to_particle(&self, r: Vec3, mass: f32) -> Particle {
        let mut p = Particle::new(r, mass);
        p.r = self.transform(r);
        p.v = self.velocity_rel(r);

        p
    }
}

impl Default for Body {
    fn default() -> Self {
        Self {
            mass: 1.0,
            r: Vec3::zero(),
            v: Vec3::zero(),
            orient: Quat::versor(0.0, Vec3::y()),
            p_angular: Vec3::zero(),
            omega: Vec3::zero(),
            ibody_inv: Mat3::identity(),
            force: Vec3::zero(),
            torque: Vec3::zero(),
        }
    }
}

impl From<&Body> for Mat4 {
    fn from(body: &Body) -> Self {
        let mut r = Mat4::from(body.orient);

        r.v1.w = body.r.x;
        r.v2.w = body.r.y;
        r.v3.w = body.r.z;

        r
    }
}
