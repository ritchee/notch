use crate::dynamics::particle::*;
use crate::math::vec3::*;

/// Accumulator trait should be implmented for forces such as gravity, spring,
/// etc..
///
/// implement `accumulate_all` or `accumulate_slice` if a force is n-ary the
/// layout of the slice is how the particle's ids are appended by you.
///
/// # Example:
/// ```
/// pub struct Gravity(pub Vec3);
///
/// impl Accumulator for Gravity {
///     fn accumulate(&self, particles: &mut Particle) {
///         let m = p.mass;
///         // add gravity to the particle's accumulated force
///         p.add_force(self.0 * m);
///     }
/// }
/// ```
pub trait Accumulator {
    fn accumulate(&self, _particle: &mut Particle) {}

    fn accumulate_all(&self, particles: &mut [&mut Particle]) {
        for p in particles.iter_mut() {
            self.accumulate(p);
        }
    }

    fn accumulate_slice(&self, particles: &mut [Particle]) {
        for p in particles.iter_mut() {
            self.accumulate(p);
        }
    }
}

/// gravity (not Newton gravitational force), internal vector is its acceleration
#[derive(Copy, Clone)]
pub struct Gravity(pub Vec3);

impl Gravity {
    pub fn new(accel: f32, dir: Vec3) -> Self {
        Self(dir.normal() * accel)
    }

    /// change gravity acceleration
    pub fn set_accel(&mut self, accel: f32) {
        let dir = self.0.normal();
        self.0 = dir * accel;
    }

    /// change gravity direction
    pub fn set_direction(&mut self, dir: Vec3) {
        let accel = self.0.len();
        self.0 = dir * accel;
    }
}

impl Default for Gravity {
    fn default() -> Self {
        Self(Vec3::y() * std::f32::consts::PI.powf(2.0))
    }
}

impl Accumulator for Gravity {
    fn accumulate(&self, particle: &mut Particle) {
        let m = particle.mass;
        // add gravity to the particle's accumulated force
        particle.add_force(self.0 * m);
    }
}

/// Mass spring system
///
/// this is a binary force, therefore, affected particle ids need to
/// always come in pair for example
/// ```
/// some_particle_system.add_force("spring", some_spring, &[]);
/// some_particle_system.push_indices("spring", &[id1, id2]);
/// ```
#[derive(Copy, Clone)]
pub struct MassSpring {
    /// stiffness coefficient
    pub ks: f32,
    /// damping coefficient
    pub kd: f32,
    /// rest length
    pub rest_len: f32,
    /// a pivot in case there's only one particle to accumulate
    pub pivot: Vec3,
}

impl MassSpring {
    /// `ks`: spring stiffness
    /// `kd`: damping coefficient
    /// `rest_len`: spring rest length
    pub fn new(ks: f32, kd: f32, rest_len: f32) -> Self {
        Self {
            ks,
            kd,
            rest_len,
            pivot: Vec3::zero(),
        }
    }

    /// compute the output spring force between two particles
    pub fn compute(&self, p0: &Particle, p1: &Particle) -> Vec3 {
        if p0.r == p1.r {
            Vec3::zero()
        } else {
            let r_diff = p0.r - p1.r;
            let v_diff = p0.v - p1.v;
            let x = r_diff.len();
            let f =
                r_diff * (-(self.ks * (x - self.rest_len) + self.kd * (v_diff * r_diff) / x)) / x;

            f
        }
    }
}

impl Accumulator for MassSpring {
    fn accumulate(&self, particle: &mut Particle) {
        let dummy = Particle::new(self.pivot, 10.0);
        let f = self.compute(particle, &dummy);

        particle.add_force(f);
    }

    fn accumulate_all(&self, particles: &mut [&mut Particle]) {
        if particles.len() == 0 {
            return;
        }

        if particles.len() % 2 != 0 {
            self.accumulate(particles[particles.len() - 1]);
        }

        for i in 1..particles.len() {
            if i % 2 == 0 {
                continue;
            }

            let f = self.compute(particles[i - 1], particles[i]);
            particles[i - 1].add_force(f);
            particles[i].add_force(-f);
        }
    }

    fn accumulate_slice(&self, particles: &mut [Particle]) {
        if particles.len() == 0 {
            return;
        }

        if particles.len() % 2 != 0 {
            self.accumulate(&mut particles[particles.len() - 1]);
        }

        for i in 1..particles.len() {
            if i % 2 == 0 {
                continue;
            }

            let f = self.compute(&particles[i - 1], &particles[i]);
            particles[i - 1].add_force(f);
            particles[i].add_force(-f);
        }
    }
}

/// viscous drag, first member is drag coefficient
#[derive(Debug, Copy, Clone)]
pub struct Drag(pub f32);

impl Drag {
    pub fn new(kd: f32) -> Self {
        Self(kd)
    }

    /// compute the output force
    pub fn compute(&self, v: Vec3) -> Vec3 {
        -self.0 * v
    }
}

impl Accumulator for Drag {
    fn accumulate(&self, p: &mut Particle) {
        let v = p.v;

        p.add_force(self.compute(v));
    }
}
