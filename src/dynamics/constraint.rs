use crate::dynamics::{force::*, particle::*};
use crate::math::*;

#[derive(Debug, Copy, Clone, Default)]
pub struct Contact {
    pub restitution: f32,
    pub penetration: f32,
    pub dt: f32,
    pub normal: Vec3,
}

impl Contact {
    pub fn new(restitution: f32, penetration: f32, normal: Vec3, dt: f32) -> Self {
        Self {
            restitution,
            penetration,
            dt,
            normal,
        }
    }

    pub fn resolve_contact(&self, p0: &mut Particle, p1: &mut Particle) {
        self.resolve_restitution(p0, p1);
        self.resolve_penetration(p0, p1);
    }

    fn resolve_restitution(&self, p0: &mut Particle, p1: &mut Particle) {
        let v_diff = (p0.v - p1.v) * self.normal;

        if v_diff > 0.0 {
            return;
        }

        let mut v_sep = -v_diff * self.restitution;

        let v_accum = (p0.accel() - p1.accel()) * self.normal * self.dt;

        if v_accum < 0.0 {
            v_sep += self.restitution * v_accum;
            v_sep = v_sep.max(0.0);
        }

        let dv = v_sep - v_diff;
        let m0_inv = p0.mass.recip();
        let m1_inv = p0.mass.recip();
        let mut m_inv = m0_inv;
        if p1.mass > 0.0 {
            m_inv += m1_inv;
        }

        if m_inv.is_infinite() {
            return;
        }

        let impulse = dv / m_inv;
        let ipm = self.normal * impulse;

        p0.v = p0.v + ipm * m0_inv;

        if p1.mass > 0.0 {
            p1.v = p1.v - ipm * m1_inv;
        }
    }

    fn resolve_penetration(&self, p0: &mut Particle, p1: &mut Particle) {
        if self.penetration <= 0.0 {
            return;
        }

        let m0_inv = p0.mass.recip();
        let m1_inv = p1.mass.recip();
        let mut m_inv = m0_inv;
        if p1.mass > 0.0 {
            m_inv += m1_inv;
        }

        if m_inv.is_infinite() {
            return;
        }

        let rpm = self.normal * (self.penetration / m_inv);

        p0.r = p0.r + rpm * m0_inv;
        if p1.mass > 0.0 {
            p1.r = p1.r - rpm * m0_inv;
        }
    }
}

impl Accumulator for Contact {
    fn accumulate(&self, p: &mut Particle) {
        let mut dummy = Particle::default();
        dummy.mass = 0.0;
        self.resolve_contact(p, &mut dummy);
    }

    fn accumulate_all(&self, particles: &mut [&mut Particle]) {
        if particles.len() == 0 {
            return;
        }

        if particles.len() % 2 != 0 {
            self.accumulate(particles[particles.len() - 1]);
        }

        for i in 1..particles.len() {
            if i % 2 == 0 {
                continue;
            }

            let mut dummy0 = *particles[i - 1];
            let mut dummy1 = *particles[i];

            self.resolve_contact(&mut dummy0, &mut dummy1);
            particles[i - 1].r = dummy0.r;
            particles[i - 1].v = dummy0.v;
            particles[i].r = dummy1.r;
            particles[i].v = dummy1.v;
        }
    }

    fn accumulate_slice(&self, particles: &mut [Particle]) {
        if particles.len() == 0 {
            return;
        }

        if particles.len() % 2 != 0 {
            self.accumulate(&mut particles[particles.len() - 1]);
        }

        for i in 1..particles.len() {
            if i % 2 == 0 {
                continue;
            }

            let mut dummy0 = particles[i - 1];
            let mut dummy1 = particles[i];

            self.resolve_contact(&mut dummy0, &mut dummy1);
            particles[i - 1].r = dummy0.r;
            particles[i - 1].v = dummy0.v;
            particles[i].r = dummy1.r;
            particles[i].v = dummy1.v;
        }
    }
}

#[derive(Copy, Clone)]
pub struct Rope {
    pub len: f32,
    pub dt: f32,
}

impl Rope {
    pub fn new(len: f32, dt: f32) -> Self {
        Self { len, dt }
    }

    fn check_contact(&self, p0: &Particle, p1: &Particle) -> Option<Contact> {
        let mut link = Contact::default();
        let x = (p0.r - p1.r).len();

        if x == self.len {
            return None;
        }

        link.dt = self.dt;
        let normal = (p1.r - p0.r) / x;

        if x > self.len {
            link.normal = normal;
            link.penetration = x - self.len;
        } else {
            link.normal = -normal;
            link.penetration = self.len - x;
        }

        Some(link)
    }
}

impl Accumulator for Rope {
    fn accumulate(&self, p: &mut Particle) {
        let dummy = Particle::default();
        if let Some(link) = self.check_contact(p, &dummy) {
            link.accumulate(p);
        }
    }

    fn accumulate_all(&self, particles: &mut [&mut Particle]) {
        if particles.len() == 0 {
            return;
        }

        if particles.len() % 2 != 0 {
            self.accumulate(particles[particles.len() - 1]);
        }

        for i in 1..particles.len() {
            if i % 2 == 0 {
                continue;
            }

            if let Some(link) = self.check_contact(&particles[i - 1], &particles[i]) {
                let mut dummy0 = *particles[i - 1];
                let mut dummy1 = *particles[i];

                link.resolve_contact(&mut dummy0, &mut dummy1);

                particles[i - 1].r = dummy0.r;
                particles[i - 1].v = dummy0.v;
                particles[i].r = dummy1.r;
                particles[i].v = dummy1.v;
            }
        }
    }

    fn accumulate_slice(&self, particles: &mut [Particle]) {
        if particles.len() == 0 {
            return;
        }

        if particles.len() % 2 != 0 {
            self.accumulate(&mut particles[particles.len() - 1]);
        }

        for i in 1..particles.len() {
            if i % 2 == 0 {
                continue;
            }

            if let Some(link) = self.check_contact(&particles[i - 1], &particles[i]) {
                //println!("constraint violated");
                let mut dummy0 = particles[i - 1];
                let mut dummy1 = particles[i];

                link.resolve_contact(&mut dummy0, &mut dummy1);

                particles[i - 1].r = dummy0.r;
                particles[i - 1].v = dummy0.v;
                particles[i].r = dummy1.r;
                particles[i].v = dummy1.v;
            }
        }
    }
}

#[derive(Copy, Clone)]
pub struct Joint {
    pub legal_x: f32,
    pub pivot: Vec3,
}

impl Joint {
    pub fn new(legal_x: f32, pivot: Vec3) -> Self {
        Self { legal_x, pivot }
    }

    pub fn compute(&self, p0: &Particle, p1: &Particle) -> Vec3 {
        let r_diff = p0.r - p1.r;
        let r = r_diff.normal();
        let r_diff = r * self.legal_x;
        let v = (p0.v - p1.v).project_n(r);

        -p0.force.project_n(r) - p0.mass * v.len2() * r_diff / r_diff.len2()
    }
}

impl Accumulator for Joint {
    fn accumulate(&self, particle: &mut Particle) {
        let dummy = Particle::new(self.pivot, 10.0);
        let f = self.compute(particle, &dummy);

        particle.add_force(f);
    }

    fn accumulate_all(&self, particles: &mut [&mut Particle]) {
        if particles.len() == 0 {
            return;
        }

        if particles.len() % 2 != 0 {
            self.accumulate(particles[particles.len() - 1]);
        }

        for i in 1..particles.len() {
            if i % 2 == 0 {
                continue;
            }

            let f1 = self.compute(particles[i - 1], particles[i]);
            let f2 = self.compute(particles[i], particles[i - 1]);
            particles[i - 1].add_force(f1);
            particles[i].add_force(f2);
        }
    }

    fn accumulate_slice(&self, particles: &mut [Particle]) {
        if particles.len() == 0 {
            return;
        }

        if particles.len() % 2 != 0 {
            self.accumulate(&mut particles[particles.len() - 1]);
        }

        for i in 1..particles.len() {
            if i % 2 == 0 {
                continue;
            }

            let f1 = self.compute(&particles[i - 1], &particles[i]);
            let f2 = self.compute(&particles[i], &particles[i - 1]);
            particles[i - 1].add_force(f1);
            particles[i].add_force(f2);
        }
    }
}
