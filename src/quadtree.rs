use crate::{
    math::vec2::*,
    shape::{shape2d::AABB2d, *},
};
use bumpalo::{boxed::Box, Bump};
use std::rc::Rc;

type BBox = AABB2d;
type QuadData = Vec2;
type Handle<'a, T> = bumpalo::boxed::Box<'a, T>;

/// Iterator type for quadtree
pub struct QuadIter<'a>(Vec<&'a QuadNode<'a>>);

impl<'a> Iterator for QuadIter<'a> {
    type Item = &'a QuadData;

    fn next(&mut self) -> Option<Self::Item> {
        if self.0.is_empty() {
            None
        } else {
            loop {
                if let Some(node_ref) = self.0.pop() {
                    if let Some(ref leaf_data) = node_ref.data {
                        return Some(leaf_data);
                    } else {
                        if let Some(ref quad_ref) = node_ref.quads[0] {
                            self.0.push(quad_ref);
                        }
                        if let Some(ref quad_ref) = node_ref.quads[1] {
                            self.0.push(quad_ref);
                        }
                        if let Some(ref quad_ref) = node_ref.quads[2] {
                            self.0.push(quad_ref);
                        }
                        if let Some(ref quad_ref) = node_ref.quads[3] {
                            self.0.push(quad_ref);
                        }
                    }
                } else {
                    return None;
                }
            }
        }
    }
}

/// A node of quadtree, which can be a tree itself if it's root
pub struct QuadNode<'a> {
    /// Shape data, it's just a point for now
    pub data: Option<QuadData>,
    /// Bound of current node
    pub bound: BBox,
    /// Children quads, use `make_quads()` to get the bounds of children
    pub quads: [Option<Handle<'a, QuadNode<'a>>>; 4],
    pool: Rc<Bump>,
}

impl<'a> QuadNode<'a> {
    /// Make a new tree, pool can be shared with other trees if desired,
    /// `bound` should of the entire tree
    pub fn tree_new(bound: BBox, pool_ref: Option<&Rc<Bump>>) -> Self {
        let pool = if let Some(pool_ref) = pool_ref {
            Rc::clone(pool_ref)
        } else {
            Rc::new(Bump::new())
        };

        Self {
            data: None,
            bound,
            quads: [None, None, None, None],
            pool,
        }
    }

    fn new(data: QuadData, bound: BBox, pool: &Rc<Bump>) -> Self {
        Self {
            data: Some(data),
            bound,
            quads: [None, None, None, None],
            pool: Rc::clone(pool),
        }
    }

    /// Get the bounds of the children
    pub fn make_quads(&self) -> Vec<BBox> {
        let extent = self.bound.extent / 2.0;
        let top_right = self.bound.max() - extent;
        let bot_left = self.bound.min() + extent;
        let bot_right = Vec2::new(top_right.x, bot_left.y);
        let top_left = Vec2::new(bot_left.x, top_right.y);

        vec![
            BBox::new(top_left, extent),
            BBox::new(top_right, extent),
            BBox::new(bot_right, extent),
            BBox::new(bot_left, extent),
        ]
    }

    /// Get data of a leaf
    pub fn leaf_data(&self) -> QuadData {
        if let Some(data) = self.data {
            data
        } else {
            panic!("This is not a leaf");
        }
    }

    /// Return true if current node is a leaf
    pub fn is_leaf(&self) -> bool {
        self.data.is_some()
    }

    /// Insert a shape into tree, can work on a node but tree is absolutely
    /// encouraged
    pub fn insert<'b: 'a>(&'b mut self, data: QuadData) {
        let quads = self.make_quads();

        if let Some(leaf_data) = self.data {
            for (i, quad) in quads.iter().enumerate() {
                if quad.contains(&leaf_data) {
                    let new_node =
                        Box::new_in(QuadNode::new(leaf_data, *quad, &self.pool), &self.pool);
                    self.quads[i] = Some(new_node);
                    self.data = None;

                    break;
                }
            }

            for (i, quad) in quads.iter().enumerate() {
                if quad.contains(&data) {
                    if let Some(ref mut node_ref) = self.quads[i] {
                        node_ref.insert(data);
                    } else {
                        let new_node =
                            Box::new_in(QuadNode::new(data, *quad, &self.pool), &self.pool);
                        self.quads[i] = Some(new_node);
                    }

                    break;
                }
            }
        } else {
            self.data = Some(data);
        }
    }

    /// Iterate each shape in the tree
    pub fn iter<'b: 'a>(&'a self) -> QuadIter<'_> {
        QuadIter(vec![self])
    }
}
