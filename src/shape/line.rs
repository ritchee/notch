use super::*;
use crate::math::FloatEq;

#[derive(Debug)]
pub struct Line {
    pub begin: Vec3,
    pub line: f32,
}

impl Line {
    pub fn new(begin: Vec3, line: Vec3) -> Self {
        Self { begin, line }
    }

    pub fn from_ends(p: Vec3, q: Vec3) -> Self {
        let normal = Vec3::new(p.y - q.y, q.x - p.x);

        Self {
            normal,
            scalar: p * normal,
        }
    }
}

impl Collider for Line {
    type PointType = Point3;

    fn contains(&self, p: &Self::PointType) -> bool {
        let dot = self.normal * *p;

        dot.eq_approx(self.scalar)
    }
}

/*
impl Intersect<Segment> for Line {
    fn intersects(&self, rhs: &Rhs) -> bool;
}
*/
