use super::*;
use crate::math::*;
//use crate::shape::*;

#[derive(Debug, Copy, Clone)]
pub struct OBB {
    pub center: Vec3,
    pub extent: Vec3,
    pub x: Vec3,
    pub y: Vec3,
    pub z: Vec3,
}

impl OBB {
    pub fn new(center: Vec3, extent: Vec3) -> Self {
        Self {
            center,
            extent,
            ..Self::default()
        }
    }

    pub fn from_mat(center: Vec3, extent: Vec3, m: &Mat3) -> Self {
        Self {
            center,
            extent,
            x: m.v1,
            y: m.v2,
            z: m.v3,
        }
    }

    pub fn local_axes(&self) -> Mat3 {
        Mat3 {
            v1: self.x,
            v2: self.y,
            v3: self.z,
        }
    }

    pub fn closest_point(&self, p: Vec3) -> Vec3 {
        let mut r = self.center;
        let local_r = p - self.center;

        let mut r_x = local_r * self.x;
        r_x = r_x.clamp(self.extent.x, -self.extent.x);
        r += r_x * self.x;

        let mut r_y = local_r * self.y;
        r_y = r_y.clamp(self.extent.y, -self.extent.y);
        r += r_y * self.y;

        let mut r_z = local_r * self.z;
        r_z = r_z.clamp(self.extent.z, -self.extent.z);
        r += r_z * self.z;

        r
    }

    pub fn corners(&self) -> Vec<Vec3> {
        let mut corners: Vec<Vec3> = Vec::new();
        let x_comp = self.extent.x * 2.0 * self.x;
        let y_comp = self.extent.y * 2.0 * self.y;
        let z_comp = self.extent.z * 2.0 * self.z;

        // x+, y+, z+
        let mut c =
            self.center + self.extent.x * self.x + self.extent.y * self.y + self.extent.z * self.z;
        corners.push(c);
        // x+, y+, z-
        c -= z_comp;
        corners.push(c);
        // x-, y+, z-
        c -= x_comp;
        corners.push(c);
        // x-, y+, z+
        c += z_comp;
        corners.push(c);

        // x-, y-, z+
        c -= y_comp;
        corners.push(c);
        // x-, y-, z-
        c -= z_comp;
        corners.push(c);
        // x+, y-, z-
        c += x_comp;
        corners.push(c);
        // x+, y-, z+
        c += z_comp;
        corners.push(c);

        corners
    }

    pub fn interval(&self, axis: Vec3) -> Interval {
        let vertices = self.corners();
        let d = vertices[0] * axis;
        let mut interval = Interval::new(d, d);

        for v in vertices.iter().skip(1) {
            let d = *v * axis;
            if d < interval.min {
                interval.min = d;
            }
            if d > interval.max {
                interval.max = d;
            }
        }

        interval
    }
}

/// Default cube at (0.0, 0.0) with length of 1.0
impl Default for OBB {
    fn default() -> Self {
        Self {
            center: Vec3::zero(),
            extent: Vec3::new(0.5, 0.5, 0.5),
            x: Vec3::x(),
            y: Vec3::y(),
            z: Vec3::z(),
        }
    }
}

impl Contain<Point3d> for OBB {
    fn contains(&self, p: &Point3d) -> bool {
        let local_r = *p - self.center;

        let r_x = local_r * self.x;
        if r_x.abs() > self.extent.x {
            return false;
        }

        let r_y = local_r * self.y;
        if r_y.abs() > self.extent.y {
            return false;
        }

        let r_z = local_r * self.z;
        if r_z.abs() > self.extent.z {
            return false;
        }

        true
    }
}

impl Intersect for OBB {
    fn intersects(&self, obb: &OBB) -> bool {
        let mut axes = vec![self.x, self.y, self.z, obb.x, obb.y, obb.z];

        for i in 0..3 {
            axes.push(axes[i].cross(axes[3]));
            axes.push(axes[i].cross(axes[4]));
            axes.push(axes[i].cross(axes[5]));
        }

        for axis in axes.iter() {
            let i1 = self.interval(*axis);
            let i2 = obb.interval(*axis);
            if i1.intersects(&i2) {
                return true;
            }
        }

        false
    }
}

impl Intersect<Sphere> for OBB {
    fn intersects(&self, sphere: &Sphere) -> bool {
        sphere.intersects(self)
    }
}

impl Intersect<AABB> for OBB {
    fn intersects(&self, aabb: &AABB) -> bool {
        aabb.intersects(self)
    }
}

impl Intersect<Plane> for OBB {
    fn intersects(&self, plane: &Plane) -> bool {
        let len = self.extent.x * (plane.normal * self.x).abs()
            + self.extent.y * (plane.normal * self.y).abs()
            + self.extent.z * (plane.normal * self.z).abs();
        let d = (self.center * plane.normal) - plane.scalar;

        d.abs() <= len
    }
}
