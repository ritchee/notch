use super::*;
//use crate::shape::*;

/// Interval structure used in SAT tests
///
/// An interval is constructed by projecting a volume onto a axis
/// Only interval constructed on an axis should be tested against each other,
/// testing intervals from different axes will end up in faulty results
#[repr(C)]
#[derive(Debug, Copy, Clone, Default)]
pub struct Interval {
    /// min component
    pub min: f32,
    /// max component
    pub max: f32,
}

impl Interval {
    /// Simple construction function
    pub fn new(min: f32, max: f32) -> Self {
        Self { min, max }
    }

    /// Length of an interval
    pub fn len(&self) -> f32 {
        self.max - self.min
    }
}

impl Intersect for Interval {
    fn intersects(&self, other: &Self) -> bool {
        self.min <= other.max && other.min <= self.max
    }
}
