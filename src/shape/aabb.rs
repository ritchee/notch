use crate::shape::*;

#[derive(Debug, Copy, Clone)]
pub struct AABB {
    pub center: Vec3,
    pub extent: Vec3,
}

impl AABB {
    pub fn new(center: Vec3, extent: Vec3) -> Self {
        Self { center, extent }
    }

    /// Construct an AABB from 2 end points, use `from_minmax` if they
    /// are top right and bottom left for quicker construction
    pub fn from_ends(begin: Vec3, end: Vec3) -> Self {
        let extent = (end - begin).abs() / 2.0;
        let center = begin.min_vec(end) + extent;

        Self { center, extent }
    }

    /// Construct an AABB from a min point and max point
    /// `min` should be the bottom left of a rectangle in xy, z- plane
    /// and `max` should be the top right in an xy, z+ plane
    pub fn from_minmax(min: Vec3, max: Vec3) -> Self {
        let extent = (max - min) / 2.0;
        let center = min + extent;

        Self { center, extent }
    }

    pub fn min(&self) -> Vec3 {
        self.center - self.extent
    }

    pub fn max(&self) -> Vec3 {
        self.center + self.extent
    }

    pub fn to_points(&self) -> Vec<Vec3> {
        let size = self.extent * 2.0;
        let mut c = self.max();
        let mut corners: Vec<Vec3> = Vec::new();

        // x+, y+, z+
        corners.push(c);
        // x+, y+, z-
        c.z -= size.z;
        corners.push(c);
        // x-, y+, z-
        c.x -= size.x;
        corners.push(c);
        // x-, y+, z+
        c.z += size.z;
        corners.push(c);

        // x-, y-, z+
        c.y -= size.y;
        corners.push(c);
        // x-, y-, z-
        c.z -= size.z;
        corners.push(c);
        // x+, y-, z-
        c.x += size.x;
        corners.push(c);
        // x+, y-, z+
        c.z += size.z;
        corners.push(c);

        corners
    }

    pub fn closest_point(&self, p: Vec3) -> Vec3 {
        let min = self.min();
        let max = self.max();

        let r = p.max_vec(min);
        let r = r.min_vec(max);

        r
    }

    pub fn interval(&self, axis: Vec3) -> Interval {
        let vertices = self.to_points();
        let d = vertices[0] * axis;
        let mut interval = Interval::new(d, d);

        for v in vertices.iter().skip(1) {
            let d = *v * axis;
            if d < interval.min {
                interval.min = d;
            }
            if d > interval.max {
                interval.max = d;
            }
        }

        interval
    }
}

/// Default cube at (0.0, 0.0) with length of 1.0
impl Default for AABB {
    fn default() -> Self {
        Self {
            center: Vec3::zero(),
            extent: Vec3::new(0.5, 0.5, 0.5),
        }
    }
}

impl Contain<Point3d> for AABB {
    fn contains(&self, p: &Point3d) -> bool {
        let r = *p - self.center;

        r.x.abs() <= self.extent.x && r.y.abs() <= self.extent.y && r.z.abs() <= self.extent.z
    }
}

impl Contain for AABB {
    fn contains(&self, aabb: &AABB) -> bool {
        if !self.contains(&aabb.center) {
            return false;
        }

        let r = aabb.center - self.center;

        if (r.x + aabb.extent.x).abs() > self.extent.x {
            false
        } else if (r.y + aabb.extent.y).abs() > self.extent.y {
            false
        } else if (r.z + aabb.extent.z).abs() > self.extent.z {
            false
        } else {
            true
        }
    }
}

impl Intersect for AABB {
    fn intersects(&self, aabb: &AABB) -> bool {
        let min1 = self.min();
        let max1 = self.max();

        let min2 = aabb.min();
        let max2 = aabb.max();

        (min1.x <= max2.x && max1.x >= min2.x)
            && (min1.y <= max2.y && max1.y >= min2.y)
            && (min1.z <= max2.z && max1.z >= min2.z)
    }
}

impl Intersect<Sphere> for AABB {
    fn intersects(&self, sphere: &Sphere) -> bool {
        sphere.intersects(self)
    }
}

impl Intersect<OBB> for AABB {
    fn intersects(&self, obb: &OBB) -> bool {
        let mut axes = vec![Vec3::x(), Vec3::y(), Vec3::z(), obb.x, obb.y, obb.z];

        for i in 0..3 {
            axes.push(axes[i].cross(axes[3]));
            axes.push(axes[i].cross(axes[4]));
            axes.push(axes[i].cross(axes[5]));
        }

        for axis in axes.iter() {
            let i1 = self.interval(*axis);
            let i2 = obb.interval(*axis);
            if i1.intersects(&i2) {
                return true;
            }
        }

        false
    }
}

impl Intersect<Plane> for AABB {
    fn intersects(&self, plane: &Plane) -> bool {
        let len = self.extent * plane.normal.abs();
        let d = (self.center * plane.normal) - plane.scalar;

        d <= len
    }
}
