use super::*;
use crate::math::*;

#[derive(Debug, Copy, Clone)]
pub struct Ray {
    pub begin: Vec3,
    pub dir: Vec3,
}

impl Ray {
    pub fn new(begin: Vec3, dir: Vec3) -> Self {
        Self {
            begin,
            dir: dir.normal(),
        }
    }

    pub fn from_ends(begin: Vec3, end: Vec3) -> Self {
        Self {
            begin,
            dir: (end - begin).normal(),
        }
    }

    pub fn closest_point(&self, p: Vec3) -> Vec3 {
        let t = (p - self.begin) * self.dir;

        let t = t.max(0.0);

        self.begin + self.dir * t
    }
}

impl Default for Ray {
    fn default() -> Self {
        Self {
            begin: Vec3::zero(),
            dir: Vec3::y(),
        }
    }
}

impl Contain<Point3d> for Ray {
    fn contains(&self, p: &Point3d) -> bool {
        if *p == self.begin {
            true
        } else {
            let n = (*p - self.begin).normal();
            let d = n * self.dir;

            d.eq_abs(1.0)
        }
    }
}
