use crate::math::*;

#[derive(Debug, Copy, Clone)]
pub struct Triangle {
    pub points: [Vec3; 3],
}

impl Triangle {
    pub fn new(a: Vec3, b: Vec3, c: Vec3) -> Self {
        Self { points: [a, b, c] }
    }
}
