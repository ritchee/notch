use super::*;
//use crate::math::{FloatEq, Vec3};
use crate::math::Vec3;

#[derive(Debug, Copy, Clone, Default)]
pub struct Sphere {
    pub center: Vec3,
    pub radius: f32,
}

impl Sphere {
    pub fn new(center: Vec3, radius: f32) -> Self {
        Self { center, radius }
    }

    pub fn radius2(&self) -> f32 {
        self.radius * self.radius
    }

    pub fn closest_point(&self, p: Vec3) -> Vec3 {
        let r = p - self.center;

        r.normal() * self.radius
    }
}

impl Contain<Point3d> for Sphere {
    fn contains(&self, p: &Point3d) -> bool {
        let r = *p - self.center;

        r.len2() <= self.radius2()
    }
}

impl Intersect for Sphere {
    fn intersects(&self, sphere: &Sphere) -> bool {
        let radii = self.radius + sphere.radius;
        let d = (self.center - sphere.center).len2();

        d < radii * radii
    }
}

impl Intersect<AABB> for Sphere {
    fn intersects(&self, aabb: &AABB) -> bool {
        let closest_point = aabb.closest_point(self.center);
        let d = (self.center - closest_point).len2();

        d < self.radius2()
    }
}

impl Intersect<OBB> for Sphere {
    fn intersects(&self, obb: &OBB) -> bool {
        let closest_point = obb.closest_point(self.center);
        let d = (self.center - closest_point).len2();

        d < self.radius2()
    }
}

impl Intersect<Plane> for Sphere {
    fn intersects(&self, plane: &Plane) -> bool {
        let closest_point = plane.closest_point(self.center);
        let d = (self.center - closest_point).len2();

        d < self.radius2()
    }
}
