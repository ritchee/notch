use super::*;
use crate::math::*;

#[derive(Debug, Copy, Clone)]
pub struct Plane {
    pub normal: Vec3,
    pub scalar: f32,
}

impl Plane {
    pub fn new(normal: Vec3, scalar: f32) -> Self {
        Self {
            normal: normal.normal(),
            scalar,
        }
    }

    pub fn from_point(normal: Vec3, point: Vec3) -> Self {
        let normal = normal.normal();

        Self {
            normal,
            scalar: normal * point,
        }
    }

    pub fn dot(&self, point: Vec3) -> f32 {
        self.normal * point - self.scalar
    }

    pub fn closest_point(&self, p: Vec3) -> Vec3 {
        let d = p * self.normal;

        p - self.normal * (d - self.scalar)
    }
}

impl Default for Plane {
    fn default() -> Self {
        Self {
            normal: Vec3::y(),
            scalar: 0.0,
        }
    }
}

impl Contain<Point3d> for Plane {
    fn contains(&self, p: &Point3d) -> bool {
        let d = *p * self.normal;

        (d - self.scalar).eq_zero()
    }
}

impl Intersect for Plane {
    fn intersects(&self, plane: &Plane) -> bool {
        let d = self.normal.cross(plane.normal);

        !d.len2().eq_zero()
    }
}

impl Intersect<Sphere> for Plane {
    fn intersects(&self, sphere: &Sphere) -> bool {
        sphere.intersects(self)
    }
}

impl Intersect<AABB> for Plane {
    fn intersects(&self, aabb: &AABB) -> bool {
        aabb.intersects(self)
    }
}
