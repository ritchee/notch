use crate::math::vec2::Vec2;

#[derive(Debug)]
pub struct Rectangle {
    pub begin: Vec2,
    pub size: Vec2,
}

impl Rectangle {
    pub fn new(begin: Vec2, size: Vec2) -> Self {
        assert!(size.x > 0.0 && size.y > 0.0);

        Self { begin, size }
    }

    pub fn from_ends(begin: Vec2, end: Vec2) -> Self {
        Self {
            begin: Vec2::new(begin.x.min(end.x), begin.y.min(end.y)),
            size: Vec2::new((end.x - begin.x).abs(), (end.y - begin.y).abs()),
        }
    }
}
