use crate::math::Vec2;

pub mod aabb;
pub mod boundingshape;
pub mod circle;
pub mod line;
pub mod obb;
pub mod ray;
pub mod segment;

use super::*;
pub use aabb::*;
pub use boundingshape::*;
pub use circle::*;
pub use line::*;
pub use obb::*;
pub use ray::*;
pub use segment::*;

/// 2d point type
pub type Point2d = Vec2;

/// Volumes that contains 2d points also intersects them
impl<T: Contain<Point2d>> Intersect<Point2d> for T {
    fn intersects(&self, rhs: &Point2d) -> bool {
        self.contains(rhs)
    }
}
