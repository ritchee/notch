use super::*;
use crate::math::FloatEq;

#[derive(Debug, Copy, Clone, Default)]
pub struct Segment {
    pub begin: Vec3,
    pub end: Vec3,
}

impl Segment {
    pub fn new(begin: Vec3, end: Vec3) -> Self {
        Self { begin, end }
    }

    pub fn len(&self) -> f32 {
        (self.end - self.begin).len()
    }

    pub fn len2(&self) -> f32 {
        (self.end - self.begin).len2()
    }

    /// Get the segment's direction vecto
    pub fn dir(&self) -> Vec3 {
        self.end - self.begin
    }

    /// Projection of a point onto the segment
    pub fn image(&self, p: Vec3) -> Vec3 {
        let dir = self.dir();
        p.project(dir)
    }

    pub fn closest_point(&self, p: Vec3) -> Vec3 {
        let dir = self.dir();
        let t = (p - self.begin) * dir / dir.len2();

        let t = t.clamp(0.0, 1.0);

        self.begin + dir * t
    }
}

impl Contain<Point3d> for Segment {
    fn contains(&self, p: &Point3d) -> bool {
        let closest_r = self.closest_point(*p);
        let d = (closest_r - *p).len2();

        d.eq_zero()
    }
}

/*
impl Intersect<Segment> for Segment {
    fn intersects(&self, rhs: &Rhs) -> bool;
}
*/
