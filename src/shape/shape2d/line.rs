use super::*;
use crate::math::FloatEq;
use crate::shape::*;

/// 2d infinite extending line, for double-ended line, refer to segment2d
#[derive(Debug)]
pub struct Line2d {
    /// Normal of line, it's more effective than a line's direction vector
    pub normal: Vec2,
    /// Component scalar, point_on_line * normal = scalar
    pub scalar: f32,
}

impl Line2d {
    /// Construct a line from a point `begin` on the line and the line's
    /// direction vector `line`
    pub fn new(begin: Vec2, line: Vec2) -> Self {
        let normal = line.ortho().normal();

        Self {
            normal,
            scalar: begin * normal,
        }
    }

    /// Construct a line from 2 points
    pub fn from_ends(p: Vec2, q: Vec2) -> Self {
        let normal = Vec2::new(p.y - q.y, q.x - p.x);

        Self {
            normal,
            scalar: p * normal,
        }
    }
}

impl Contain<Point2d> for Line2d {
    fn contains(&self, p: &Point2d) -> bool {
        let dot = self.normal * *p;

        dot.eq_approx(self.scalar)
    }
}

/*
impl Intersect<Segment> for Line2d {
    fn intersects(&self, rhs: &Rhs) -> bool;
}
*/
