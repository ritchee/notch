use super::*;

/// A bounding shape contains various smaller and cheaper bounding volumes
/// such as AABBs and circles
#[derive(Debug)]
pub struct BoundingShape {
    circles: Vec<Circle2d>,
    aabbs: Vec<AABB2d>,
}

impl BoundingShape {
    /// Construction an empty shape
    pub fn new() -> Self {
        Self {
            circles: Vec::new(),
            aabbs: Vec::new(),
        }
    }

    /// Add a new circle to the current shape
    pub fn push_circle(&mut self, circle: Circle2d) {
        self.circles.push(circle);
    }

    /// Add a new AABB to the current shape
    pub fn push_aabb(&mut self, aabb: AABB2d) {
        self.aabbs.push(aabb);
    }
}

impl Contain<Point2d> for BoundingShape {
    fn contains(&self, p: &Point2d) -> bool {
        for c in self.circles.iter() {
            if c.contains(&p) {
                return true;
            }
        }

        for r in self.aabbs.iter() {
            if r.contains(p) {
                return true;
            }
        }

        false
    }
}
