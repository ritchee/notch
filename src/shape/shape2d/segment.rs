use super::*;
use crate::math::FloatEq;
use crate::shape::*;

/// The ubiquitous double-ended line structure, aka segment
#[derive(Debug, Copy, Clone)]
pub struct Segment2d {
    /// Begin point
    pub begin: Vec2,
    /// End point, interchangeable with `begin`
    pub end: Vec2,
}

impl Segment2d {
    pub fn new(begin: Vec2, end: Vec2) -> Self {
        Self { begin, end }
    }

    /// Compute the segment's length
    pub fn len(&self) -> f32 {
        (self.end - self.begin).len()
    }

    /// Square of the segment's length
    pub fn len2(&self) -> f32 {
        (self.end - self.begin).len2()
    }

    /// Slope of the segment
    pub fn slope(&self) -> f32 {
        let s = self.end - self.begin;

        s.y / s.x
    }

    /// Get the segment's direction vecto
    pub fn dir(&self) -> Vec2 {
        self.end - self.begin
    }

    /// Projection of a point onto the segment
    pub fn image(&self, p: &Point2d) -> Point2d {
        let dir = self.dir();
        p.project(dir)
    }
}

impl Into<AABB2d> for Segment2d {
    fn into(self) -> AABB2d {
        let extent = (self.end - self.begin).abs() / 2.0;

        AABB2d::new(self.begin + extent, extent)
    }
}

impl Contain<Point2d> for Segment2d {
    fn contains(&self, p: &Point2d) -> bool {
        let m = self.slope();
        let y_intercept = self.begin.y - m * self.begin.x;

        let (ymin, ymax) = if self.begin.y < self.end.y {
            (self.begin.y, self.end.y)
        } else {
            (self.end.y, self.begin.y)
        };

        p.y.eq_approx(m * p.x + y_intercept) && p.y >= ymin && p.y <= ymax
    }
}

impl Intersect<Circle2d> for Segment2d {
    fn intersects(&self, circle: &Circle2d) -> bool {
        let r = self.end - self.begin;
        let d = ((circle.center - self.begin) * r) / r.len2();

        if d < 0.0 || d > 1.0 {
            false
        } else {
            let closest_point = self.begin + r * d;

            (circle.center - closest_point).len2() < circle.radius2()
        }
    }
}

impl Intersect<AABB2d> for Segment2d {
    fn intersects(&self, aabb: &AABB2d) -> bool {
        if aabb.contains(&self.begin) || aabb.contains(&self.end) {
            return true;
        }

        let mut dir = self.dir().normal();
        dir.x = if dir.x != 0.0 { dir.x.recip() } else { 0.0 };
        dir.y = if dir.y != 0.0 { dir.y.recip() } else { 0.0 };

        let mut min = aabb.min() - self.begin;
        min.x *= dir.x;
        min.y *= dir.y;

        let mut max = aabb.max() - self.begin;
        max.x *= dir.x;
        max.y *= dir.y;

        let tmin = min.x.min(max.x);
        let tmin = (min.y.min(max.y)).max(tmin);
        let tmax = min.x.max(max.x);
        let tmax = (min.y.max(max.y)).min(tmax);

        if tmax < 0.0 || tmin > tmax {
            false
        } else {
            let t = if tmin < 0.0 { tmax } else { tmin };

            t > 0.0 && t * t < self.len2()
        }
    }
}

impl Intersect<OBB2d> for Segment2d {
    fn intersects(&self, obb: &OBB2d) -> bool {
        let theta = -obb.angle;
        let mut local_begin = self.begin;
        let mut local_end = self.end;
        local_begin.rotate_at(theta, obb.center);
        local_end.rotate_at(theta, obb.center);
        let aabb = AABB2d::from(obb);

        Segment2d::new(local_begin, local_end).intersects(&aabb)
    }
}
