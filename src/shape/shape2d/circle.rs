use super::*;
use crate::shape::*;

/// Circle structure
#[derive(Debug)]
pub struct Circle2d {
    /// Center of the circle
    pub center: Vec2,
    /// Radius
    pub radius: f32,
}

impl Circle2d {
    /// Construct a circle from a point and radius
    pub fn new(center: Vec2, radius: f32) -> Self {
        Self { center, radius }
    }

    /// Compute square of the circle radius since it's used multiple times
    /// in intersection tests
    pub fn radius2(&self) -> f32 {
        self.radius * self.radius
    }
}

/// Construct the minimum bounding circle given points in space
impl From<&[Point2d]> for Circle2d {
    fn from(points: &[Point2d]) -> Self {
        let mut center = points[0];
        for p in points.iter().skip(1) {
            center += *p;
        }
        center /= points.len() as f32;

        let mut r: f32 = 0.0;
        for p in points.iter() {
            r = r.max((*p - center).len2());
        }

        Self {
            center,
            radius: r.sqrt(),
        }
    }
}

impl Contain<Point2d> for Circle2d {
    fn contains(&self, p: &Point2d) -> bool {
        let len2 = (self.center - *p).len2();

        len2 < self.radius.powi(2)
    }
}

impl Intersect for Circle2d {
    fn intersects(&self, rhs: &Self) -> bool {
        let d = self.radius + rhs.radius;

        (self.center - rhs.center).len2() < d * d
    }
}

impl Intersect<Segment2d> for Circle2d {
    fn intersects(&self, segment: &Segment2d) -> bool {
        segment.intersects(self)
    }
}

impl Intersect<AABB2d> for Circle2d {
    fn intersects(&self, aabb: &AABB2d) -> bool {
        let min = aabb.min();
        let max = aabb.max();

        let mut closest_point = self.center;

        if closest_point.x < min.x {
            closest_point.x = min.x;
        } else if closest_point.x > max.x {
            closest_point.x = max.x;
        }

        if closest_point.y < min.y {
            closest_point.y = min.y;
        } else if closest_point.y > max.y {
            closest_point.y = max.y;
        }

        Segment2d::new(self.center, closest_point).len2() <= self.radius2()
    }
}

impl Intersect<OBB2d> for Circle2d {
    fn intersects(&self, obb: &OBB2d) -> bool {
        let mut local_center = self.center;
        local_center.rotate_at(-obb.angle, obb.center);
        let local_circle = Circle2d::new(local_center, self.radius);

        local_circle.intersects(&AABB2d::from(obb))
    }
}
