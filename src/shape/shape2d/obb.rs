use super::{aabb::AABB2d, *};
use crate::math::*;
use crate::shape::*;

/// Oriented Bounding Box, similar to AABB in term of `center` and `extent`,
/// with an exception of `angle` which denotes the counterclockwise rotation of
/// the OBB from x axis
#[derive(Debug)]
pub struct OBB2d {
    /// Center of OBB
    pub center: Vec2,
    /// Half of width and height of OBB
    pub extent: Vec2,
    /// Counterclockwise rotation from x axis
    pub angle: f32,
}

impl OBB2d {
    /// Construction function
    pub fn new(center: Vec2, extent: Vec2, angle: f32) -> Self {
        Self {
            center,
            extent,
            angle,
        }
    }

    /// Get OBB's local x and y axes
    pub fn local_axes(&self) -> Mat2 {
        let local_x = Vec2::new(self.angle.cos(), self.angle.sin());
        let local_y = Vec2::new(-local_x.y, local_x.x);

        Mat2 {
            v1: local_x,
            v2: local_y,
        }
    }

    /// Get min point of OBB
    pub fn min(&self) -> Vec2 {
        if self.angle == 0.0 {
            self.center - self.extent
        } else {
            let Mat2 {
                v1: x_axis,
                v2: y_axis,
            } = self.local_axes();
            self.center + self.extent.x * x_axis + self.extent.y * y_axis
        }
    }

    /// Get max point of OBB
    pub fn max(&self) -> Vec2 {
        if self.angle == 0.0 {
            self.center + self.extent
        } else {
            let Mat2 {
                v1: x_axis,
                v2: y_axis,
            } = self.local_axes();
            self.center - self.extent.x * x_axis - self.extent.y * y_axis
        }
    }

    /// Construct an interval of OBB on an axis
    pub fn interval(&self, axis: Vec2) -> Interval {
        let aabb = AABB2d::from(self);

        let min = aabb.min();
        let max = aabb.max();

        let mut verts = [min, max, Vec2::new(min.x, max.y), Vec2::new(max.x, min.y)];
        let rotation = rotate(self.angle);

        for vert in verts.iter_mut() {
            let r = *vert - aabb.center;
            *vert = rotation * r + aabb.center;
        }

        let d = verts[0] * axis;
        let mut interval = Interval::new(d, d);
        for vert in verts.iter().skip(1) {
            let d = *vert * axis;
            interval.min = interval.min.min(d);
            interval.max = interval.max.max(d);
        }

        interval
    }
}

/// Default square at (0.0, 0.0) with length of 1.0
impl Default for OBB2d {
    fn default() -> Self {
        Self {
            center: Vec2::zero(),
            extent: Vec2::new(0.5, 0.5),
            angle: 0.0,
        }
    }
}

impl Contain<Point2d> for OBB2d {
    fn contains(&self, p: &Point2d) -> bool {
        /*
        let (x_axis, y_axis) = self.local_axes();
        let local_v = *p - self.center;
        let dot_x = local_v * x_axis;
        let dot_y = local_v * y_axis;
        */

        let mut local_p = *p - self.center;
        local_p.rotate(-self.angle);
        local_p += self.center;

        AABB2d::from(self).contains(&local_p)
    }
}

impl Intersect<Segment2d> for OBB2d {
    fn intersects(&self, segment: &Segment2d) -> bool {
        segment.intersects(self)
    }
}

impl Intersect<Circle2d> for OBB2d {
    fn intersects(&self, circle: &Circle2d) -> bool {
        circle.intersects(self)
    }
}

impl Intersect<AABB2d> for OBB2d {
    fn intersects(&self, aabb: &AABB2d) -> bool {
        aabb.intersects(self)
    }
}

impl Intersect for OBB2d {
    fn intersects(&self, obb: &Self) -> bool {
        let mut center2 = obb.center;
        center2.rotate_at(-self.angle, self.center);
        let obb = OBB2d::new(center2, obb.extent, obb.angle - self.angle);

        AABB2d::from(self).intersects(&obb)
    }
}
