use super::*;
use crate::math::{FloatEq, Vec2};
use crate::shape::*;

/// A singly-ended line structure, aka ray
#[derive(Debug)]
pub struct Ray2d {
    /// Begin point
    pub begin: Vec2,
    /// Ray's normal is the left orthogonal vector of it's direction vector
    pub normal: Vec2,
}

impl Ray2d {
    /// Construct a ray from a point and a direction vector
    pub fn new(begin: Vec2, direction: Vec2) -> Self {
        Self {
            begin,
            normal: direction.left_ortho(),
        }
    }

    /// Get the ray's direction
    pub fn direction(&self) -> Vec2 {
        self.normal.right_ortho()
    }
}

impl Contain<Point2d> for Ray2d {
    fn contains(&self, p: &Point2d) -> bool {
        let dir = *p - self.begin;

        (dir * self.normal).eq_approx(0.0) && dir.cross(self.normal) > 0.0
    }
}
