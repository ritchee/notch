use super::{obb::OBB2d, *};
use crate::math::transform2::*;
use crate::shape::*;

/// Axis Aligned Bounding Box
/// Simply a rectangle that stores its center and half of its dimension instead
/// of conventional begin point and width and height
#[derive(Debug, Copy, Clone)]
pub struct AABB2d {
    /// Center of the box
    pub center: Vec2,
    /// The extent of the box from center, or half of its width and length
    pub extent: Vec2,
}

impl AABB2d {
    /// Construction function
    pub fn new(center: Vec2, extent: Vec2) -> Self {
        Self { center, extent }
    }

    /// Construct an AABB from a rectangle
    pub fn from_rect(x: f32, y: f32, w: f32, h: f32) -> Self {
        let extent = Vec2::new(w / 2.0, h / 2.0);

        Self {
            center: Vec2::new(x + extent.x, y + extent.y),
            extent,
        }
    }

    /// Construct an AABB from 2 end points, use `from_minmax` if they
    /// are top right and bottom left for quicker construction
    pub fn from_ends(begin: Vec2, end: Vec2) -> Self {
        let extent = (end - begin).abs() / 2.0;
        let center = begin.min_vec(end) + extent;

        Self { center, extent }
    }

    /// Construct an AABB from a min point and max point
    /// `min` should be the bottom left corner of a rectangle and `max` should
    /// be the top right in an xy plane
    pub fn from_minmax(min: Vec2, max: Vec2) -> Self {
        let extent = (max - min) / 2.0;
        let center = min + extent;

        Self { center, extent }
    }

    /// Get the min point of AABB
    pub fn min(&self) -> Vec2 {
        self.center - self.extent
    }

    /// Get the max point of AABB
    pub fn max(&self) -> Vec2 {
        self.center + self.extent
    }

    /// Get all four corners of AABB, the returned array begins with min point
    /// and the subsequent points goes counterclockwise until top left point
    pub fn to_points(&self) -> Vec<Vec2> {
        let min = self.min();
        let max = self.max();

        vec![min, Vec2::new(max.x, min.y), max, Vec2::new(min.x, max.y)]
    }

    /// Construct an interval of AABB on an axis
    pub fn interval(&self, axis: Vec2) -> Interval {
        let min = self.min();
        let max = self.max();
        let corners = [min, Vec2::new(min.x, max.y), max, Vec2::new(max.x, min.y)];

        let d = axis * corners[0];
        let mut dmin = d;
        let mut dmax = d;

        for p in corners.iter().skip(1) {
            let d = axis * *p;
            if d < dmin {
                dmin = d;
            }
            if d > dmax {
                dmax = d;
            }
        }

        Interval::new(dmin, dmax)
    }

    /*
    pub fn overlap_axis(&self, aabb: &Self, axis: Vec2) -> bool {
        let interval0 = self.interval(axis);
        let interval1 = aabb.interval(axis);

        (interval1.min <= interval0.max) && (interval0.min <= interval1.max)
    }

    pub fn sat_test(&self, aabb: &Self) -> bool {
        let x = Vec2::x();
        let y = Vec2::y();

        if !self.overlap_axis(aabb, x) {
            return false;
        }
        if !self.overlap_axis(aabb, y) {
            return false;
        }

        true
    }
    */
}

/// Default square at (0.0, 0.0) with length of 1.0
impl Default for AABB2d {
    fn default() -> Self {
        Self {
            center: Vec2::zero(),
            extent: Vec2::new(0.5, 0.5),
        }
    }
}

/// Construct a minimum AABB from a set of points in space
impl From<&[Point2d]> for AABB2d {
    fn from(points: &[Point2d]) -> Self {
        let mut max = points[0];
        let mut min = max;

        for p in points.iter().skip(1) {
            max.x = max.x.max(p.x);
            min.x = min.x.min(p.x);
            max.y = max.y.max(p.y);
            min.y = min.y.min(p.y);
        }

        Self::from_minmax(min, max)
    }
}

/// Construct an OBB from an AABB, `center` and `extent` are preserved as if
/// the OBB un-rotates itself
impl From<OBB2d> for AABB2d {
    fn from(value: OBB2d) -> Self {
        Self::new(value.center, value.extent)
    }
}

/// Similar to `From<OBB2d>` but takes a reference
impl From<&OBB2d> for AABB2d {
    fn from(value: &OBB2d) -> Self {
        Self::new(value.center, value.extent)
    }
}

impl Contain<Point2d> for AABB2d {
    fn contains(&self, p: &Point2d) -> bool {
        let local_v = *p - self.center;

        local_v.x.abs() <= self.extent.x && local_v.y.abs() <= self.extent.y
    }
}

impl Contain for AABB2d {
    fn contains(&self, aabb: &AABB2d) -> bool {
        if !self.contains(&aabb.center) {
            return false;
        }

        let r = aabb.center - self.center;

        if (r.x + aabb.extent.x).abs() > self.extent.x {
            false
        } else if (r.y + aabb.extent.y).abs() > self.extent.y {
            false
        } else {
            true
        }
    }
}

impl Intersect for AABB2d {
    fn intersects(&self, aabb: &AABB2d) -> bool {
        let (min0, max0) = (self.min(), self.max());
        let (min1, max1) = (aabb.min(), aabb.max());

        let on_x = (min1.x <= max0.x) && (min0.x <= max1.x);
        let on_y = (min1.y <= max0.y) && (min0.y <= max1.y);

        on_x && on_y
    }
}

impl Intersect<Segment2d> for AABB2d {
    fn intersects(&self, segment: &Segment2d) -> bool {
        segment.intersects(self)
    }
}

impl Intersect<Circle2d> for AABB2d {
    fn intersects(&self, circle: &Circle2d) -> bool {
        circle.intersects(self)
    }
}

impl Intersect<OBB2d> for AABB2d {
    fn intersects(&self, obb: &OBB2d) -> bool {
        let mut axes = vec![Vec2::x(), Vec2::y()];
        let rotation = rotate(obb.angle);
        let axis = rotation * Vec2::x();
        axes.push(axis);
        let axis = rotation * Vec2::y();
        axes.push(axis);

        for axis in axes.iter() {
            let i1 = self.interval(*axis);
            let i2 = obb.interval(*axis);

            if !i1.intersects(&i2) {
                return false;
            }
        }

        true
    }
}
