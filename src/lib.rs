pub mod dynamics;
pub mod math;
pub mod octree;
pub mod quadtree;
pub mod shape;

pub mod prelude {
    pub use crate::dynamics::*;
    pub use crate::math;
    //pub use crate::math::*;
    pub use crate::shape::shape2d;
    pub use crate::shape::*;
}
