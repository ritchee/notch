use crate::{math::vec3::*, shape::*};
use bumpalo::{boxed::Box, Bump};
use std::rc::Rc;

type BBox = AABB;
type OctData = Vec3;
type Handle<'a, T> = bumpalo::boxed::Box<'a, T>;

/// Iterator type for octree
pub struct OctIter<'a>(Vec<&'a OctNode<'a>>);

impl<'a> Iterator for OctIter<'a> {
    type Item = &'a OctData;

    fn next(&mut self) -> Option<Self::Item> {
        if self.0.is_empty() {
            None
        } else {
            loop {
                if let Some(node_ref) = self.0.pop() {
                    if let Some(ref leaf_data) = node_ref.data {
                        return Some(leaf_data);
                    } else {
                        for octs in node_ref.octs.iter() {
                            if let Some(ref oct_ref) = octs {
                                self.0.push(oct_ref);
                            }
                        }
                    }
                } else {
                    return None;
                }
            }
        }
    }
}

/// A node of octree, which can be a tree itself if it's root
pub struct OctNode<'a> {
    /// Shape data, it's just a point for now
    pub data: Option<OctData>,
    /// Bound of current node
    pub bound: BBox,
    /// Children octs, use `make_octs()` to get the bounds of children
    pub octs: [Option<Handle<'a, OctNode<'a>>>; 8],
    pool: Rc<Bump>,
}

impl<'a> OctNode<'a> {
    /// Make a new tree, pool can be shared with other trees if desired,
    /// `bound` should of the entire tree
    pub fn tree_new(bound: BBox, pool_ref: Option<&Rc<Bump>>) -> Self {
        let pool = if let Some(pool_ref) = pool_ref {
            Rc::clone(pool_ref)
        } else {
            Rc::new(Bump::new())
        };

        let octs: [Option<Handle<'a, OctNode<'a>>>; 8] =
            [None, None, None, None, None, None, None, None];

        Self {
            data: None,
            bound,
            octs,
            pool,
        }
    }

    fn new(data: OctData, bound: BBox, pool: &Rc<Bump>) -> Self {
        let octs: [Option<Handle<'a, OctNode<'a>>>; 8] =
            [None, None, None, None, None, None, None, None];

        Self {
            data: Some(data),
            bound,
            octs,
            pool: Rc::clone(pool),
        }
    }

    /// Get the bounds of the children
    pub fn make_octs(&self) -> Vec<BBox> {
        let extent = self.bound.extent / 2.0;
        let mut octs: Vec<BBox> = Vec::new();

        // x+, y+, z+
        let mut center = self.bound.max() - extent;
        octs.push(AABB::new(center, extent));
        // x+, y-, z+
        center.y -= self.bound.extent.y;
        octs.push(AABB::new(center, extent));
        // x-, y-, z+
        center.x -= self.bound.extent.x;
        octs.push(AABB::new(center, extent));
        // x-, y+, z+
        center.y += self.bound.extent.y;
        octs.push(AABB::new(center, extent));

        // x-, y+, z-
        center.z += self.bound.extent.z;
        octs.push(AABB::new(center, extent));
        // x+, y+, z-
        center.x += self.bound.extent.x;
        octs.push(AABB::new(center, extent));
        // x+, y-, z-
        center.y -= self.bound.extent.y;
        octs.push(AABB::new(center, extent));
        // x-, y-, z-
        center.x -= self.bound.extent.x;
        octs.push(AABB::new(center, extent));

        octs
    }

    /// Get data of a leaf
    pub fn leaf_data(&self) -> OctData {
        if let Some(data) = self.data {
            data
        } else {
            panic!("This is not a leaf");
        }
    }

    /// Return true if current node is a leaf
    pub fn is_leaf(&self) -> bool {
        self.data.is_some()
    }

    /// Insert a shape into tree, can work on a node but tree is absolutely
    /// encouraged
    pub fn insert<'b: 'a>(&'b mut self, data: OctData) {
        let octs = self.make_octs();

        if let Some(leaf_data) = self.data {
            for (i, oct) in octs.iter().enumerate() {
                if oct.contains(&leaf_data) {
                    let new_node =
                        Box::new_in(OctNode::new(leaf_data, *oct, &self.pool), &self.pool);
                    self.octs[i] = Some(new_node);
                    self.data = None;

                    break;
                }
            }

            for (i, oct) in octs.iter().enumerate() {
                if oct.contains(&data) {
                    if let Some(ref mut node_ref) = self.octs[i] {
                        node_ref.insert(data);
                    } else {
                        let new_node =
                            Box::new_in(OctNode::new(data, *oct, &self.pool), &self.pool);
                        self.octs[i] = Some(new_node);
                    }

                    break;
                }
            }
        } else {
            self.data = Some(data);
        }
    }

    /// Iterate each shape in the tree
    pub fn iter<'b: 'a>(&'a self) -> OctIter<'_> {
        OctIter(vec![self])
    }
}
