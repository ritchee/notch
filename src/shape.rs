use crate::math::Vec3;

pub mod aabb;
pub mod interval;
pub mod obb;
pub mod plane;
pub mod ray;
pub mod segment;
pub mod shape2d;
pub mod sphere;
pub mod triangle;
/*
pub mod line;
pub mod circle;
pub mod rectangle;
*/

pub use aabb::*;
pub use interval::*;
pub use obb::*;
pub use plane::*;
pub use ray::*;
pub use segment::*;
pub use sphere::*;
pub use triangle::*;

/// 3d point type
pub type Point3d = Vec3;

/// Contain trait will check if the given shape (self) contains a another shape
/// entirely within its volume
///
/// Volumes must implement at least `Collider<Point2d>` for 2d shapes and
/// `Collider<Point3d>` for 3d shapes
pub trait Contain<Rhs = Self> {
    fn contains(&self, p: &Rhs) -> bool;
}

/// Intersect trait will check for the intersection between 2 bouding volumes
pub trait Intersect<Rhs = Self> {
    fn intersects(&self, rhs: &Rhs) -> bool;
}

/// Volumes that contains 3d points also intersects them
impl<T: Contain<Point3d>> Intersect<Point3d> for T {
    fn intersects(&self, rhs: &Point3d) -> bool {
        self.contains(rhs)
    }
}
/*
/// Implement commutative property for Intersect trait
impl<T, U: Intersect<T>> Intersect<U> for T {
    fn intersects(&self, rhs: &U) -> bool {
        rhs.intersects(self)
    }
}
*/
