use crate::math::{mat4::*, quat::Quaternion, vec2::Vector2, vec3::Vector3, *};

/// R4 vector
#[derive(Debug, Copy, Clone, Default)]
#[repr(C)]
pub struct Vector4<T> {
    pub x: T,
    pub y: T,
    pub z: T,
    /// 4th component
    pub w: T,
}

/// Int32 Vector
pub type IVec4 = Vector4<i32>;
/// UInt32 Vector
pub type UVec4 = Vector4<u32>;
/// Float32 Vector
pub type Vec4 = Vector4<f32>;
/// Float64 Vector
pub type DVec4 = Vector4<f64>;

impl<T: PrimNum> Vector4<T> {
    /// Construct a new vector
    #[inline(always)]
    pub fn new(x: T, y: T, z: T, w: T) -> Self {
        Self { x, y, z, w }
    }

    /// Construct a new vector with all 4 zeroes
    #[inline(always)]
    pub fn zero() -> Self {
        Self {
            x: Zero::zero(),
            y: Zero::zero(),
            z: Zero::zero(),
            w: Zero::zero(),
        }
    }

    /// Construct a new vector with all 1.0
    #[inline(always)]
    pub fn one() -> Self {
        Self {
            x: One::one(),
            y: One::one(),
            z: One::one(),
            w: One::one(),
        }
    }

    /// Computes the tensor matrix
    #[inline(always)]
    pub fn tensor(&self) -> Matrix4<T> {
        Matrix4 {
            v1: *self * self.x,
            v2: *self * self.y,
            v3: *self * self.z,
            v4: *self * self.w,
        }
    }
}

impl<T: FloatNum> Vector4<T> {
    /// produce an x vector
    #[inline(always)]
    pub fn x() -> Self {
        Self {
            x: One::one(),
            y: Zero::zero(),
            z: Zero::zero(),
            w: Zero::zero(),
        }
    }

    /// produce an y vector
    #[inline(always)]
    pub fn y() -> Self {
        Self {
            x: Zero::zero(),
            y: One::one(),
            z: Zero::zero(),
            w: Zero::zero(),
        }
    }

    /// produce an z vector
    #[inline(always)]
    pub fn z() -> Self {
        Self {
            x: Zero::zero(),
            y: Zero::zero(),
            z: One::one(),
            w: Zero::zero(),
        }
    }

    #[inline(always)]
    pub fn is_zero(&self) -> bool {
        self.x.eq_zero() && self.y.eq_zero() && self.z.eq_zero() && self.w.is_zero()
    }

    /// normalize current vector
    #[inline(always)]
    pub fn normalize(&mut self) {
        let length = self.len();

        self.x /= length;
        self.y /= length;
        self.z /= length;
        self.w /= length;
    }

    /// Make a normalized vector from `self`
    #[inline(always)]
    pub fn normal(&self) -> Self {
        let length = self.len();

        Self {
            x: self.x / length,
            y: self.y / length,
            z: self.z / length,
            w: self.z / length,
        }
    }

    /// Square of magnitude
    #[inline(always)]
    pub fn len2(&self) -> T {
        self.x * self.x + self.y * self.y + self.z * self.z + self.w * self.w
    }

    /// Vector magnitude
    #[inline(always)]
    pub fn len(&self) -> T {
        self.len2().sqrt()
    }

    /// Project `self` onto `v`
    #[inline(always)]
    pub fn project(&self, v: Self) -> Self {
        v * ((*self * v) / (v * v))
    }

    // Project `self` onto unit vector `n`
    #[inline(always)]
    pub fn project_n(&self, n: Self) -> Self {
        n * (*self * n)
    }

    /// Compute the reject vector of a projection
    #[inline(always)]
    pub fn reject(&self, v: Self) -> Self {
        *self - v * ((*self * v) / (v * v))
    }

    /// Compute distance between 2 vectors
    #[inline(always)]
    pub fn distance(&self, v: Self) -> T {
        (*self - v).len()
    }

    /// Compute angle between 2 vectors
    #[inline(always)]
    pub fn angle(&self, v: Self) -> T {
        let dot = *self * v;
        let denom = (self.len2() * v.len2()).sqrt();

        (dot / denom).acos()
    }

    /// Computes counterclockwise normalized orthogonal of vector
    #[inline(always)]
    pub fn ortho(&self) -> Self {
        self.left_ortho().normal()
    }

    /// Computes counterclockwise unnormalized orthogonal of vector
    #[inline(always)]
    pub fn left_ortho(&self) -> Self {
        Self {
            x: -self.y,
            y: self.x,
            z: Zero::zero(),
            w: Zero::zero(),
        }
    }

    /// Computes clockwise unnormalized orthogonal of vector
    #[inline(always)]
    pub fn right_ortho(&self) -> Self {
        Self {
            x: self.y,
            y: -self.x,
            z: Zero::zero(),
            w: Zero::zero(),
        }
    }

    /// Make an abosolute vector of `self`
    #[inline(always)]
    pub fn abs(&self) -> Self {
        Self {
            x: self.x.abs(),
            y: self.y.abs(),
            z: self.z.abs(),
            w: self.w.abs(),
        }
    }

    /// Returns a vector with minimum values
    #[inline(always)]
    pub fn min_vec(&self, v: Self) -> Self {
        Self {
            x: self.x.min(v.x),
            y: self.y.min(v.y),
            z: self.z.min(v.z),
            w: self.w.min(v.w),
        }
    }

    /// Returns a vector with maximum values
    #[inline(always)]
    pub fn max_vec(&self, v: Self) -> Self {
        Self {
            x: self.x.max(v.x),
            y: self.y.max(v.y),
            z: self.z.max(v.z),
            w: self.w.max(v.w),
        }
    }

    /// Make a cross product matrix: v1.cross(v2) == v1.to_cross() * v2
    #[inline(always)]
    pub fn to_cross(&self) -> Matrix4<T> {
        Matrix4::new(
            Zero::zero(),
            -self.z,
            self.y,
            Zero::zero(),
            self.z,
            Zero::zero(),
            -self.x,
            Zero::zero(),
            -self.y,
            self.x,
            Zero::zero(),
            Zero::zero(),
            Zero::zero(),
            Zero::zero(),
            Zero::zero(),
            One::one(),
        )
    }

    /// Compare if 2 vectors are approximately equal
    #[inline(always)]
    pub fn eq_approx(&self, v: Self) -> bool {
        self.x.eq_approx(v.x)
            && self.y.eq_approx(v.y)
            && self.z.eq_approx(v.z)
            && self.w.eq_approx(v.w)
    }
}

impl<T: fmt::Display> fmt::Display for Vector4<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}, {}, {}, {}]", self.x, self.y, self.z, self.w)
    }
}

impl<T: PrimNum> From<[T; 4]> for Vector4<T> {
    #[inline(always)]
    fn from(arr: [T; 4]) -> Self {
        Self {
            x: arr[0],
            y: arr[1],
            z: arr[2],
            w: arr[3],
        }
    }
}

impl<T: PrimNum> From<&[T; 4]> for Vector4<T> {
    #[inline(always)]
    fn from(arr: &[T; 4]) -> Self {
        Self {
            x: arr[0],
            y: arr[1],
            z: arr[2],
            w: arr[3],
        }
    }
}

/// Converts a tuple of primitive numbers to Vector4
impl<T: PrimNum> From<(T, T, T, T)> for Vector4<T> {
    #[inline(always)]
    fn from(arg: (T, T, T, T)) -> Self {
        Self {
            x: arg.0,
            y: arg.1,
            z: arg.2,
            w: arg.3,
        }
    }
}

impl<T: PrimNum> From<T> for Vector4<T> {
    #[inline(always)]
    fn from(s: T) -> Self {
        Self {
            x: s,
            y: s,
            z: s,
            w: s,
        }
    }
}

impl<T: PrimNum> From<Vector2<T>> for Vector4<T> {
    #[inline(always)]
    fn from(v: Vector2<T>) -> Self {
        Self {
            x: v.x,
            y: v.y,
            z: Zero::zero(),
            w: Zero::zero(),
        }
    }
}

impl<T: PrimNum> From<Vector3<T>> for Vector4<T> {
    #[inline(always)]
    fn from(v: Vector3<T>) -> Self {
        Self {
            x: v.x,
            y: v.y,
            z: v.z,
            w: Zero::zero(),
        }
    }
}

impl<T: FloatNum> From<Quaternion<T>> for Vector4<T> {
    #[inline(always)]
    fn from(q: Quaternion<T>) -> Self {
        Self {
            x: q.x,
            y: q.y,
            z: q.z,
            w: q.w,
        }
    }
}

impl From<Vec4> for DVec4 {
    #[inline(always)]
    fn from(v: Vec4) -> Self {
        Self {
            x: v.x as f64,
            y: v.y as f64,
            z: v.z as f64,
            w: v.w as f64,
        }
    }
}

impl From<DVec4> for Vec4 {
    #[inline(always)]
    fn from(v: DVec4) -> Self {
        Self {
            x: v.x as f32,
            y: v.y as f32,
            z: v.z as f32,
            w: v.w as f32,
        }
    }
}

impl<T: PrimNum> Into<[T; 4]> for Vector4<T> {
    #[inline(always)]
    fn into(self) -> [T; 4] {
        [self.x, self.y, self.z, self.w]
    }
}

impl<T: PrimNum> Add<Vector4<T>> for Vector4<T> {
    type Output = Self;

    #[inline(always)]
    fn add(self, v: Self) -> Self::Output {
        Self {
            x: self.x + v.x,
            y: self.y + v.y,
            z: self.z + v.z,
            w: self.w + v.w,
        }
    }
}

impl<T: PrimNum> Add<T> for Vector4<T> {
    type Output = Self;

    #[inline(always)]
    fn add(self, s: T) -> Self::Output {
        Self {
            x: self.x + s,
            y: self.y + s,
            z: self.z + s,
            w: self.w + s,
        }
    }
}

impl<T: PrimNum> AddAssign<Vector4<T>> for Vector4<T> {
    #[inline(always)]
    fn add_assign(&mut self, v: Self) {
        self.x += v.x;
        self.y += v.y;
        self.z += v.z;
        self.w += v.w;
    }
}

impl<T: PrimNum> Sub<Vector4<T>> for Vector4<T> {
    type Output = Self;

    #[inline(always)]
    fn sub(self, v: Self) -> Self::Output {
        Self {
            x: self.x - v.x,
            y: self.y - v.y,
            z: self.z - v.z,
            w: self.w - v.w,
        }
    }
}

impl<T: PrimNum> Sub<T> for Vector4<T> {
    type Output = Self;

    #[inline(always)]
    fn sub(self, s: T) -> Self::Output {
        Self {
            x: self.x - s,
            y: self.y - s,
            z: self.z - s,
            w: self.w - s,
        }
    }
}

impl<T: PrimNum> SubAssign<Vector4<T>> for Vector4<T> {
    #[inline(always)]
    fn sub_assign(&mut self, v: Self) {
        self.x -= v.x;
        self.y -= v.y;
        self.z -= v.z;
        self.w -= v.w;
    }
}

/// Divides a vector by another vector, returns a vector of each result
impl<T: PrimNum> Div<Vector4<T>> for Vector4<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, v: Vector4<T>) -> Self::Output {
        Self {
            x: self.x / v.x,
            y: self.y / v.y,
            z: self.z / v.z,
            w: self.w / v.w,
        }
    }
}

/// Divides and assigns a vector by another vector
impl<T: PrimNum> DivAssign<Vector4<T>> for Vector4<T> {
    #[inline(always)]
    fn div_assign(&mut self, v: Vector4<T>) {
        self.x /= v.x;
        self.y /= v.y;
        self.z /= v.z;
        self.w /= v.w;
    }
}

impl<T: PrimNum> Div<T> for Vector4<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, s: T) -> Self::Output {
        Self {
            x: self.x / s,
            y: self.y / s,
            z: self.z / s,
            w: self.w / s,
        }
    }
}

impl<T: PrimNum> DivAssign<T> for Vector4<T> {
    #[inline(always)]
    fn div_assign(&mut self, s: T) {
        self.x /= s;
        self.y /= s;
        self.z /= s;
        self.w /= s;
    }
}

impl<T: PrimNum> Mul<Vector4<T>> for Vector4<T> {
    type Output = T;

    #[inline(always)]
    fn mul(self, v: Self) -> Self::Output {
        self.x * v.x + self.y * v.y + self.z * v.z + self.w * v.w
    }
}

impl<T: PrimNum> Mul<T> for Vector4<T> {
    type Output = Self;

    #[inline(always)]
    fn mul(self, s: T) -> Self::Output {
        Self {
            x: self.x * s,
            y: self.y * s,
            z: self.z * s,
            w: self.w * s,
        }
    }
}

impl<T: PrimNum> MulAssign<T> for Vector4<T> {
    #[inline(always)]
    fn mul_assign(&mut self, s: T) {
        self.x *= s;
        self.y *= s;
        self.z *= s;
        self.w *= s;
    }
}

/// Scales a Vec4 (right vector)
impl Mul<Vec4> for f32 {
    type Output = Vec4;

    #[inline(always)]
    fn mul(self, v: Vec4) -> Vec4 {
        v * self
    }
}

/// Scales a DVec4 (right vector)
impl Mul<DVec4> for f64 {
    type Output = DVec4;

    #[inline(always)]
    fn mul(self, v: DVec4) -> DVec4 {
        v * self
    }
}

impl<T: PrimNum + Neg<Output = T>> Neg for Vector4<T> {
    type Output = Self;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        Self {
            x: -self.x,
            y: -self.y,
            z: -self.z,
            w: -self.w,
        }
    }
}

impl<T: PrimNum> Index<usize> for Vector4<T> {
    type Output = T;

    #[inline(always)]
    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            3 => &self.w,
            _ => panic!("out of range"),
        }
    }
}

impl<T: FloatNum> PartialEq for Vector4<T> {
    fn eq(&self, v: &Self) -> bool {
        self.x == v.x && self.y == v.y && self.z == v.z && self.w == v.w
    }
}

impl<T: FloatNum> Eq for Vector4<T> {}
