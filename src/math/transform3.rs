use crate::math::*;

impl Vec3 {
    /// rotate a vector counterclockwise in the direction of x
    pub fn rotate_x(&mut self, angle: f32) {
        let (s, c) = angle.sin_cos();
        let old = *self;

        self.y = old.y * c - old.z * s;
        self.z = old.y * s + old.z * c;
    }

    /// rotate a vector counterclockwise in the direction of y
    pub fn rotate_y(&mut self, angle: f32) {
        let (s, c) = angle.sin_cos();
        let old = *self;

        self.z = old.z * c - old.x * s;
        self.x = old.z * s + old.x * c;
    }

    /// rotate a vector counterclockwise in the direction of z
    pub fn rotate_z(&mut self, angle: f32) {
        let (s, c) = angle.sin_cos();
        let old = *self;

        self.x = old.x * c - old.y * s;
        self.y = old.x * s + old.y * c;
    }

    /// rotate a vector counterclockwise with respect to a pivot
    pub fn rotate_at(&mut self, angle: f32, pivot: Vec3) {
        let versor = Quat::versor(angle, pivot);
        *self = versor.rotate(*self)
    }

    /// general translation
    pub fn translate(&mut self, offset: Vec3) {
        *self += offset;
    }

    /// translate in the direction of x
    pub fn translate_x(&mut self, offset: f32) {
        self.x += offset;
    }

    /// translate in the direction of y
    pub fn translate_y(&mut self, offset: f32) {
        self.y += offset;
    }

    /// translate in the direction of z
    pub fn translate_z(&mut self, offset: f32) {
        self.z += offset;
    }

    /// produce a translated vector
    pub fn translate_to(&self, offset: Vec3) -> Vec3 {
        *self + offset
    }
}

/// produce a matrix of general scaling
pub fn scale(nv: Vec3) -> Mat4 {
    Mat4::from([
        [nv.x, 0.0, 0.0, 0.0],
        [0.0, nv.y, 0.0, 0.0],
        [0.0, 0.0, nv.z, 0.0],
        [0.0, 0.0, 0.0, 1.0],
    ])
}

/// produce a matrix of uniform scaling with respect to pivot `o`
pub fn scale_uniform(nv: Vec3, o: Vec3) -> Mat4 {
    Mat4::from([
        [nv.x, 0.0, 0.0, (1.0 - nv.x) * o.x],
        [0.0, nv.x, 0.0, (1.0 - nv.y) * o.y],
        [0.0, 0.0, nv.x, (1.0 - nv.z) * o.z],
        [0.0, 0.0, 0.0, 1.0],
    ])
}

/// produce a matrix of nonuniform scaling with respect to pivot `o` along axis `u`
/// `u` does not have to be normalized
pub fn scale_nonuniform(nv: Vec3, o: Vec3, mut u: Vec3) -> Mat4 {
    u.normalize();

    let mut mt = Mat3::from([
        [u.x * u.x, u.x * u.y, u.x * u.z],
        [u.y * u.x, u.y * u.y, u.y * u.z],
        [u.z * u.x, u.z * u.y, u.z * u.z],
    ]);

    let ms = Mat3::from([
        [1.0 - nv.x, 0.0, 0.0],
        [0.0, 1.0 - nv.y, 0.0],
        [0.0, 0.0, 1.0 - nv.z],
    ]);

    mt *= ms;
    mt.v1.x = 1.0 - mt.v1.x;
    mt.v2.y = 1.0 - mt.v2.y;
    mt.v3.z = 1.0 - mt.v3.z;

    let v = ms * (u * (u * o));

    let mut m = Mat4::from(mt);
    m.v1.w = v.x;
    m.v2.w = v.y;
    m.v3.w = v.z;

    m
}

/// produce a matrix of rotation in the direction of x
pub fn rotate_x(angle: f32) -> Mat4 {
    let (s, c) = angle.sin_cos();

    Mat4::from([
        [1.0, 0.0, 0.0, 0.0],
        [0.0, c, -s, 0.0],
        [0.0, s, c, 0.0],
        [0.0, 0.0, 0.0, 1.0],
    ])
}

/// produce a matrix of rotation in the direction of y
pub fn rotate_y(angle: f32) -> Mat4 {
    let (s, c) = angle.sin_cos();

    Mat4::from([
        [c, 0.0, s, 0.0],
        [0.0, 1.0, 0.0, 0.0],
        [-s, 0.0, c, 0.0],
        [0.0, 0.0, 0.0, 1.0],
    ])
}

/// produce a matrix of rotation in the direction of z
pub fn rotate_z(angle: f32) -> Mat4 {
    let (s, c) = angle.sin_cos();

    Mat4::from([
        [c, -s, 0.0, 0.0],
        [s, c, 0.0, 0.0],
        [0.0, 1.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 1.0],
    ])
}

/// produce a matrix of rotation with respect to a pivot
pub fn rotate_at(angle: f32, pivot: Vec3) -> Mat4 {
    let (s, c) = angle.sin_cos();
    let u = Vec4::from(pivot.normal());

    let m = Mat4::from([
        [c, -s * u.z, s * u.y, 0.0],
        [s * u.z, c, -s * u.x, 0.0],
        [-s * u.y, s * u.x, c, 0.0],
        [0.0, 0.0, 0.0, 1.0],
    ]);

    let mt = Mat4::tensor(u, u);

    m - ((m - Mat4::identity()) * mt)
}

/// produce a matrix of translation along x axis
pub fn translate_x(offset: f32) -> Mat4 {
    Mat4::from([
        [1.0, 0.0, 0.0, offset],
        [0.0, 1.0, 0.0, 0.0],
        [0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 1.0],
    ])
}

/// produce a matrix of translation along y axis
pub fn translate_y(offset: f32) -> Mat4 {
    Mat4::from([
        [1.0, 0.0, 0.0, 0.0],
        [0.0, 1.0, 0.0, offset],
        [0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 1.0],
    ])
}

/// produce a matrix of translation along z axis
pub fn translate_z(offset: f32) -> Mat4 {
    Mat4::from([
        [1.0, 0.0, 0.0, 0.0],
        [0.0, 1.0, 0.0, 0.0],
        [0.0, 0.0, 1.0, offset],
        [0.0, 0.0, 0.0, 1.0],
    ])
}

/// produce a matrix of general translation
pub fn translate(translation: Vec3) -> Mat4 {
    Mat4::from([
        [1.0, 0.0, 0.0, translation.x],
        [0.0, 1.0, 0.0, translation.y],
        [0.0, 0.0, 1.0, translation.z],
        [0.0, 0.0, 0.0, 1.0],
    ])
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::math::quat::*;
    use std::f32::consts::PI;

    #[test]
    fn rotate_z() {
        let two_sqrt = 2.0_f32.sqrt();
        let mut x = Vec3::x();
        let result = Vec3::new(1.0 / two_sqrt, 1.0 / two_sqrt, 0.0);

        x.rotate_z(PI / 4.0);
        let x2 = super::rotate_z(PI / 4.0) * Vec3::x();

        assert_eq!(result, x);
        assert_eq!(result, x2);
    }

    #[test]
    fn rotate_x() {
        let two_sqrt = 2.0_f32.sqrt();
        let mut y = Vec3::y();
        let result = Vec3::new(0.0, 1.0 / two_sqrt, 1.0 / two_sqrt);

        y.rotate_x(PI / 4.0);
        let y2 = super::rotate_x(PI / 4.0) * Vec3::y();

        assert_eq!(result, y);
        assert_eq!(result, y2);
    }

    #[test]
    fn rotate_y() {
        let two_sqrt = 2.0_f32.sqrt();
        let mut z = Vec3::z();
        let result = Vec3::new(1.0 / two_sqrt, 0.0, 1.0 / two_sqrt);

        z.rotate_y(PI / 4.0);
        let z2 = super::rotate_y(PI / 4.0) * Vec3::z();

        assert_eq!(result, z);
        assert_eq!(result, z2);
    }

    #[test]
    fn rotate_at() {
        let v = Vec3::new(1.0, 1.0, 1.0);
        let x = Vec3::x();
        let angle = PI / 4.0;

        // quaternion rotation has been tested and generally more accurate and
        // faster than matrix (no simd), but matrix is generally preferred
        // since it can be chained with other transformations
        let q = Quat::versor(angle, v);
        let m = Mat3::from(super::rotate_at(angle, v));

        let v1 = q.rotate(x);
        let v2 = m * x;

        assert!(v1.eq_approx(v2));
    }
}
