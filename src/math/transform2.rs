use crate::math::{Mat2, Vec2};

impl Vec2 {
    /// rotate a vector counterclockwise
    pub fn rotate(&mut self, angle: f32) {
        let (s, c) = angle.sin_cos();
        let old = *self;

        self.x = old.x * c - old.y * s;
        self.y = old.x * s + old.y * c;
    }

    /// produce a vector that is rotated counterclockwise
    pub fn rotate_to(&self, angle: f32) -> Self {
        let (s, c) = angle.sin_cos();
        let old = *self;

        Self {
            x: old.x * c - old.y * s,
            y: old.x * s + old.y * c,
        }
    }

    /// rotate with respect to a pivot
    pub fn rotate_at(&mut self, angle: f32, pivot: Vec2) {
        self.translate(-pivot);
        self.rotate(angle);
        self.translate(pivot);
    }

    /// general translation
    pub fn translate(&mut self, offset: Vec2) {
        *self += offset;
    }

    /// translate in the direction of x
    pub fn translate_x(&mut self, offset: f32) {
        self.x += offset;
    }

    /// translate in the direction of y
    pub fn translate_y(&mut self, offset: f32) {
        self.y += offset;
    }

    /// produce a translated vector
    pub fn translate_to(&self, offset: Vec2) -> Vec2 {
        *self + offset
    }
}

pub fn rotate(angle: f32) -> Mat2 {
    let (s, c) = angle.sin_cos();

    Mat2::new(c, -s, s, c)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::f32::consts::PI;

    #[test]
    fn rotate() {
        let two_sqrt = 2.0_f32.sqrt();
        let mut x = Vec2::x();
        let result = Vec2::new(1.0 / two_sqrt, 1.0 / two_sqrt);

        x.rotate(PI / 4.0);
        let x2 = super::rotate(PI / 4.0) * Vec2::x();

        assert_eq!(result, x);
        assert_eq!(result, x2);
    }
}
