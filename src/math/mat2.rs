use crate::math::{mat3::Matrix3, mat4::Matrix4, vec2::Vector2, *};

// members are row vector, not conventional column vector due to cache
#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct Matrix2<T> {
    pub v1: Vector2<T>,
    pub v2: Vector2<T>,
}

/// 32 bit float matrix
pub type Mat2 = Matrix2<f32>;
/// 64 bit float matrix
pub type DMat2 = Matrix2<f64>;

impl<T: FloatNum> Matrix2<T> {
    /// make a matrix from each component from left to right
    /// highly discouraged, use from() with an array instead
    #[inline(always)]
    pub fn new(m00: T, m01: T, m10: T, m11: T) -> Self {
        Self {
            v1: Vector2::new(m00, m01),
            v2: Vector2::new(m10, m11),
        }
    }

    /// make a matrix from 2 column vectors
    #[inline(always)]
    pub fn from_columns(v1: Vector2<T>, v2: Vector2<T>) -> Self {
        Self {
            v1: Vector2::new(v1.x, v2.x),
            v2: Vector2::new(v1.y, v2.y),
        }
    }

    /// return 2 column vectors
    #[inline(always)]
    pub fn into_columns(&self) -> [Vector2<T>; 2] {
        [
            Vector2::new(self.v1.x, self.v2.x),
            Vector2::new(self.v1.y, self.v2.y),
        ]
    }

    /// return a zero'd matrix
    #[inline(always)]
    pub fn zero() -> Self {
        Self {
            v1: Vector2::zero(),
            v2: Vector2::zero(),
        }
    }

    /// return an identity matrix
    #[inline(always)]
    pub fn identity() -> Self {
        let one: T = One::one();

        Matrix2::from(one)
    }

    /// make a tensor out of 2 vectors, `v1` is the column vector and `v2` is
    /// the row vector
    #[inline(always)]
    pub fn tensor(v1: Vector2<T>, v2: Vector2<T>) -> Self {
        Self {
            v1: v2 * v1.x,
            v2: v2 * v1.y,
        }
    }

    /// return a matrix transpose
    #[inline(always)]
    pub fn transpose(&self) -> Self {
        Self {
            v1: Vector2::new(self.v1.x, self.v2.x),
            v2: Vector2::new(self.v1.y, self.v2.y),
        }
    }

    /// compute the determinant
    #[inline(always)]
    pub fn det(&self) -> T {
        self.v1.x * self.v2.y - self.v1.y * self.v2.x
    }

    /// compute the inverse
    #[inline(always)]
    pub fn inverse(&self) -> Self {
        let det = self.det();

        assert!(!det.eq_zero());

        Self {
            v1: Vector2::new(self.v2.y, -self.v1.y),
            v2: Vector2::new(-self.v2.x, self.v1.x),
        } / det
    }

    /// compute the trace, a.k.a sum of diagonal components
    #[inline(always)]
    pub fn trace(&self) -> T {
        self.v1.x + self.v2.y
    }
}

impl<T: FloatNum> Default for Matrix2<T> {
    fn default() -> Self {
        Self::identity()
    }
}

impl<T: fmt::Display> fmt::Display for Matrix2<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}\n {}]", self.v1, self.v2)
    }
}

impl<T: FloatNum> From<T> for Matrix2<T> {
    #[inline(always)]
    fn from(s: T) -> Self {
        Self {
            v1: Vector2::new(s, Zero::zero()),
            v2: Vector2::new(Zero::zero(), s),
        }
    }
}

impl<T: FloatNum> From<Vector2<T>> for Matrix2<T> {
    #[inline(always)]
    fn from(v: Vector2<T>) -> Self {
        Self {
            v1: Vector2::new(v.x, Zero::zero()),
            v2: Vector2::new(Zero::zero(), v.y),
        }
    }
}

impl<T: FloatNum> From<[T; 4]> for Matrix2<T> {
    #[inline(always)]
    fn from(m: [T; 4]) -> Self {
        Self {
            v1: Vector2::new(m[0], m[1]),
            v2: Vector2::new(m[3], m[4]),
        }
    }
}

impl<T: FloatNum> From<[[T; 2]; 2]> for Matrix2<T> {
    #[inline(always)]
    fn from(m: [[T; 2]; 2]) -> Self {
        Self {
            v1: Vector2::from(m[0]),
            v2: Vector2::from(m[1]),
        }
    }
}

impl<T: FloatNum> From<[Vector2<T>; 2]> for Matrix2<T> {
    #[inline(always)]
    fn from(m: [Vector2<T>; 2]) -> Self {
        Self { v1: m[0], v2: m[1] }
    }
}

impl<T: FloatNum> From<Matrix3<T>> for Matrix2<T> {
    #[inline(always)]
    fn from(m: Matrix3<T>) -> Self {
        Self {
            v1: Vector2::from(m.v1),
            v2: Vector2::from(m.v2),
        }
    }
}

impl<T: FloatNum> From<Matrix4<T>> for Matrix2<T> {
    #[inline(always)]
    fn from(m: Matrix4<T>) -> Self {
        Self {
            v1: Vector2::from(m.v1),
            v2: Vector2::from(m.v2),
        }
    }
}

impl<T: FloatNum> Into<[[T; 2]; 2]> for Matrix2<T> {
    #[inline(always)]
    fn into(self) -> [[T; 2]; 2] {
        [[self.v1.x, self.v1.y], [self.v2.x, self.v2.y]]
    }
}

impl<T: FloatNum> Into<[Vector2<T>; 2]> for Matrix2<T> {
    #[inline(always)]
    fn into(self) -> [Vector2<T>; 2] {
        [self.v1, self.v2]
    }
}

impl<T: FloatNum> Add<Matrix2<T>> for Matrix2<T> {
    type Output = Self;

    #[inline(always)]
    fn add(self, m: Self) -> Self::Output {
        Self {
            v1: self.v1 + m.v1,
            v2: self.v2 + m.v2,
        }
    }
}

impl<T: FloatNum> AddAssign<Matrix2<T>> for Matrix2<T> {
    #[inline(always)]
    fn add_assign(&mut self, m: Self) {
        self.v1 += m.v1;
        self.v2 += m.v2;
    }
}

impl<T: FloatNum> Sub<Matrix2<T>> for Matrix2<T> {
    type Output = Self;

    #[inline(always)]
    fn sub(self, m: Self) -> Self::Output {
        Self {
            v1: self.v1 - m.v1,
            v2: self.v2 - m.v2,
        }
    }
}

impl<T: FloatNum> SubAssign<Matrix2<T>> for Matrix2<T> {
    #[inline(always)]
    fn sub_assign(&mut self, m: Self) {
        self.v1 -= m.v1;
        self.v2 -= m.v2;
    }
}

impl<T: FloatNum> Div<Matrix2<T>> for Matrix2<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, m: Self) -> Self::Output {
        self * m.inverse()
    }
}

impl<T: FloatNum> DivAssign<Matrix2<T>> for Matrix2<T> {
    #[inline(always)]
    fn div_assign(&mut self, m: Self) {
        *self *= m.inverse()
    }
}

impl<T: FloatNum> Div<T> for Matrix2<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, s: T) -> Self::Output {
        Self {
            v1: self.v1 / s,
            v2: self.v2 / s,
        }
    }
}

impl<T: FloatNum> DivAssign<T> for Matrix2<T> {
    #[inline(always)]
    fn div_assign(&mut self, s: T) {
        self.v1 /= s;
        self.v2 /= s;
    }
}

impl<T: FloatNum> Mul<Matrix2<T>> for Matrix2<T> {
    type Output = Self;

    #[inline(always)]
    fn mul(self, m: Self) -> Self::Output {
        Self {
            v1: Vector2::new(
                self.v1.x * m.v1.x + self.v1.y * m.v2.x,
                self.v1.x * m.v1.y + self.v1.y * m.v2.y,
            ),
            v2: Vector2::new(
                self.v2.x * m.v1.x + self.v2.y * m.v2.x,
                self.v2.x * m.v1.y + self.v2.y * m.v2.y,
            ),
        }
    }
}

impl<T: FloatNum> MulAssign<Matrix2<T>> for Matrix2<T> {
    #[inline(always)]
    fn mul_assign(&mut self, m: Self) {
        let v1 = Vector2::new(
            self.v1.x * m.v1.x + self.v1.y * m.v2.x,
            self.v1.x * m.v1.y + self.v1.y * m.v2.y,
        );
        let v2 = Vector2::new(
            self.v2.x * m.v1.x + self.v2.y * m.v2.x,
            self.v2.x * m.v1.y + self.v2.y * m.v2.y,
        );

        self.v1 = v1;
        self.v2 = v2;
    }
}

impl<T: FloatNum> Mul<T> for Matrix2<T> {
    type Output = Self;

    #[inline(always)]
    fn mul(self, s: T) -> Self::Output {
        Self {
            v1: self.v1 * s,
            v2: self.v2 * s,
        }
    }
}

impl<T: FloatNum> MulAssign<T> for Matrix2<T> {
    #[inline(always)]
    fn mul_assign(&mut self, s: T) {
        self.v1 *= s;
        self.v2 *= s;
    }
}

impl Mul<Mat2> for f32 {
    type Output = Mat2;

    #[inline(always)]
    fn mul(self, m: Mat2) -> Self::Output {
        m * self
    }
}

impl Mul<DMat2> for f64 {
    type Output = DMat2;

    #[inline(always)]
    fn mul(self, m: DMat2) -> Self::Output {
        m * self
    }
}

impl<T: FloatNum> Mul<Vector2<T>> for Matrix2<T> {
    type Output = Vector2<T>;

    #[inline(always)]
    fn mul(self, v: Vector2<T>) -> Self::Output {
        Vector2::new(self.v1 * v, self.v2 * v)
    }
}

impl<T: FloatNum> Neg for Matrix2<T> {
    type Output = Self;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        Self {
            v1: -self.v1,
            v2: -self.v2,
        }
    }
}

impl<T: FloatNum> Index<usize> for Matrix2<T> {
    type Output = Vector2<T>;

    #[inline(always)]
    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.v1,
            1 => &self.v2,
            _ => panic!("out of range"),
        }
    }
}
