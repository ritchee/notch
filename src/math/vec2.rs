use crate::math::{mat2::*, vec3::Vector3, vec4::Vector4, *};

/// A 2-dimensional vector
#[derive(Debug, Copy, Clone, Default)]
#[repr(C)]
pub struct Vector2<T> {
    pub x: T,
    pub y: T,
}

/// Int32 Vector
pub type IVec2 = Vector2<i32>;

/// UInt32 Vector
pub type UVec2 = Vector2<u32>;

/// Float32 Vector
pub type Vec2 = Vector2<f32>;

/// Float64 Vector
pub type DVec2 = Vector2<f64>;

/// The struct Vector2 accepts generic T type with PrimNum trait, which is in short for primitive number
impl<T: PrimNum> Vector2<T> {
    /// Creates a new vector
    #[inline(always)]
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }

    /// Creates a vector with the default value '0'
    #[inline(always)]
    pub fn zero() -> Self {
        Self {
            x: Zero::zero(),
            y: Zero::zero(),
        }
    }

    /// Creates a vector with the default value '1'
    #[inline(always)]
    pub fn one() -> Self {
        Self {
            x: One::one(),
            y: One::one(),
        }
    }

    /// Computes cross product
    #[inline(always)]
    pub fn cross(&self, v: Self) -> T {
        self.x * v.y - self.y * v.x
    }

    /// Computes the tensor matrix
    #[inline(always)]
    pub fn tensor(&self) -> Matrix2<T> {
        Matrix2 {
            v1: *self * self.x,
            v2: *self * self.y,
        }
    }
}

/// The struct Vector2 accepts generic T type with FloatNum trait, which is in short for float number
impl<T: FloatNum> Vector2<T> {
    /// produce an x vector
    #[inline(always)]
    pub fn x() -> Self {
        Self {
            x: One::one(),
            y: Zero::zero(),
        }
    }

    /// produce an y vector
    #[inline(always)]
    pub fn y() -> Self {
        Self {
            x: Zero::zero(),
            y: One::one(),
        }
    }

    /// Returns true if both x, y are 0, else Returns false
    #[inline(always)]
    pub fn is_zero(&self) -> bool {
        self.x.eq_zero() && self.y.eq_zero()
    }

    /// normalize current vector
    #[inline(always)]
    pub fn normalize(&mut self) {
        let length = self.len();

        self.x /= length;
        self.y /= length;
    }

    /// Creates a normalized vector
    #[inline(always)]
    pub fn normal(&self) -> Self {
        let length = self.len();

        Self {
            x: self.x / length,
            y: self.y / length,
        }
    }

    /// Returns the sum of x square and y square
    #[inline(always)]
    pub fn len2(&self) -> T {
        self.x * self.x + self.y * self.y
    }

    /// Returns the magnitude of a vector
    #[inline(always)]
    pub fn len(&self) -> T {
        self.len2().sqrt()
    }

    /// Computes vector projection
    #[inline(always)]
    pub fn project(&self, v: Self) -> Self {
        v * ((*self * v) / (v * v))
    }

    /// Project onto a unit vector
    #[inline(always)]
    pub fn project_n(&self, n: Self) -> Self {
        n * (*self * n)
    }

    /// Compute the reject vector of a projection
    #[inline(always)]
    pub fn reject(&self, v: Self) -> Self {
        *self - v * ((*self * v) / (v * v))
    }

    /// Returns the distance
    #[inline(always)]
    pub fn distance(&self, v: Self) -> T {
        (*self - v).len()
    }

    /// Computes angle between two vectors using dot product
    #[inline(always)]
    pub fn angle(&self, v: Self) -> T {
        let dot = *self * v;
        let denom = (self.len2() * v.len2()).sqrt();

        (dot / denom).acos()
    }

    /// Computes counterclockwise normalized orthogonal of vector
    #[inline(always)]
    pub fn ortho(&self) -> Self {
        self.left_ortho().normal()
    }

    /// Computes counterclockwise unnormalized orthogonal of vector
    #[inline(always)]
    pub fn left_ortho(&self) -> Self {
        Self {
            x: -self.y,
            y: self.x,
        }
    }

    /// Computes clockwise unnormalized orthogonal of vector
    #[inline(always)]
    pub fn right_ortho(&self) -> Self {
        Self {
            x: self.y,
            y: -self.x,
        }
    }

    /// Computes absolute values of a vector
    #[inline(always)]
    pub fn abs(&self) -> Self {
        Self {
            x: self.x.abs(),
            y: self.y.abs(),
        }
    }

    /// Returns a vector with minimum values
    #[inline(always)]
    pub fn min_vec(&self, v: Self) -> Self {
        Self {
            x: self.x.min(v.x),
            y: self.y.min(v.y),
        }
    }

    /// Returns a vector with maximum values
    #[inline(always)]
    pub fn max_vec(&self, v: Self) -> Self {
        Self {
            x: self.x.max(v.x),
            y: self.y.max(v.y),
        }
    }

    /// Compare if 2 vectors are approximately equal
    #[inline(always)]
    pub fn eq_approx(&self, v: Self) -> bool {
        self.x.eq_approx(v.x) && self.y.eq_approx(v.y)
    }
}

/// Implements Display trait for struct Vector2
/// Implements this trait for a type will automatically implement the ToString trait for the type, allowing the usage of the .to_string() method.
impl<T: fmt::Display> fmt::Display for Vector2<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}, {}]", self.x, self.y)
    }
}

/// Converts an array of primitive numbers to Vector2
impl<T: PrimNum> From<[T; 2]> for Vector2<T> {
    #[inline(always)]
    fn from(arr: [T; 2]) -> Self {
        Self {
            x: arr[0],
            y: arr[1],
        }
    }
}

/// Converts a reference to array to Vector2
impl<T: PrimNum> From<&[T; 2]> for Vector2<T> {
    #[inline(always)]
    fn from(arr: &[T; 2]) -> Self {
        Self {
            x: arr[0],
            y: arr[1],
        }
    }
}

/// Converts a tuple of primitive numbers to Vector2
impl<T: PrimNum> From<(T, T)> for Vector2<T> {
    #[inline(always)]
    fn from(arg: (T, T)) -> Self {
        Self { x: arg.0, y: arg.1 }
    }
}

/// Splats a number into a vector
impl<T: PrimNum> From<T> for Vector2<T> {
    #[inline(always)]
    fn from(s: T) -> Self {
        Self { x: s, y: s }
    }
}

/// Converts 3D-vector into Vector2
impl<T: PrimNum> From<Vector3<T>> for Vector2<T> {
    #[inline(always)]
    fn from(v: Vector3<T>) -> Self {
        Self { x: v.x, y: v.y }
    }
}

/// Converts 4D-vector into Vector2
impl<T: PrimNum> From<Vector4<T>> for Vector2<T> {
    #[inline(always)]
    fn from(v: Vector4<T>) -> Self {
        Self { x: v.x, y: v.y }
    }
}

/// Expands precision of Vector2
impl From<Vec2> for DVec2 {
    #[inline(always)]
    fn from(v: Vec2) -> Self {
        Self {
            x: v.x as f64,
            y: v.y as f64,
        }
    }
}

/// TODO
impl From<DVec2> for Vec2 {
    #[inline(always)]
    fn from(v: DVec2) -> Self {
        Self {
            x: v.x as f32,
            y: v.y as f32,
        }
    }
}

/// Converts from Vector2 into an array
impl<T: PrimNum> Into<[T; 2]> for Vector2<T> {
    #[inline(always)]
    fn into(self) -> [T; 2] {
        [self.x, self.y]
    }
}

/// Adds 2 2D-vectors
impl<T: PrimNum> Add<Vector2<T>> for Vector2<T> {
    type Output = Self;

    #[inline(always)]
    fn add(self, v: Self) -> Self::Output {
        Self {
            x: self.x + v.x,
            y: self.y + v.y,
        }
    }
}

/// Adds a constant to Vector2's components
impl<T: PrimNum> Add<T> for Vector2<T> {
    type Output = Self;

    #[inline(always)]
    fn add(self, s: T) -> Self::Output {
        Self {
            x: self.x + s,
            y: self.y + s,
        }
    }
}

/// Adds and assigns a vector
impl<T: PrimNum> AddAssign<Vector2<T>> for Vector2<T> {
    #[inline(always)]
    fn add_assign(&mut self, v: Self) {
        self.x += v.x;
        self.y += v.y;
    }
}

/// Subtracts 2 2D-vectors
impl<T: PrimNum> Sub<Vector2<T>> for Vector2<T> {
    type Output = Self;

    #[inline(always)]
    fn sub(self, v: Self) -> Self::Output {
        Self {
            x: self.x - v.x,
            y: self.y - v.y,
        }
    }
}

/// Subtracts a constant from a vector
impl<T: PrimNum> Sub<T> for Vector2<T> {
    type Output = Self;

    #[inline(always)]
    fn sub(self, s: T) -> Self::Output {
        Self {
            x: self.x - s,
            y: self.y - s,
        }
    }
}

/// Subtract and assigns a vector
impl<T: PrimNum> SubAssign<Vector2<T>> for Vector2<T> {
    #[inline(always)]
    fn sub_assign(&mut self, v: Self) {
        self.x -= v.x;
        self.y -= v.y;
    }
}

/// Divides a vector by another vector, returns a vector of each result
impl<T: PrimNum> Div<Vector2<T>> for Vector2<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, v: Vector2<T>) -> Self::Output {
        Self {
            x: self.x / v.x,
            y: self.y / v.y,
        }
    }
}

/// Divides and assigns a vector by another vector
impl<T: PrimNum> DivAssign<Vector2<T>> for Vector2<T> {
    #[inline(always)]
    fn div_assign(&mut self, v: Vector2<T>) {
        self.x /= v.x;
        self.y /= v.y;
    }
}

/// Divides a constant from a vector
impl<T: PrimNum> Div<T> for Vector2<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, s: T) -> Self::Output {
        Self {
            x: self.x / s,
            y: self.y / s,
        }
    }
}

/// Divides and assigns a vector
impl<T: PrimNum> DivAssign<T> for Vector2<T> {
    #[inline(always)]
    fn div_assign(&mut self, s: T) {
        self.x /= s;
        self.y /= s;
    }
}

/// Computes dot product
impl<T: PrimNum> Mul<Vector2<T>> for Vector2<T> {
    type Output = T;

    #[inline(always)]
    fn mul(self, v: Self) -> Self::Output {
        self.x * v.x + self.y * v.y
    }
}

/// Scales a vector (left vector)
impl<T: PrimNum> Mul<T> for Vector2<T> {
    type Output = Self;

    #[inline(always)]
    fn mul(self, s: T) -> Self::Output {
        Self {
            x: self.x * s,
            y: self.y * s,
        }
    }
}

/// Multiplies and assigns Vector2 with a factor
impl<T: PrimNum> MulAssign<T> for Vector2<T> {
    #[inline(always)]
    fn mul_assign(&mut self, s: T) {
        self.x *= s;
        self.y *= s;
    }
}

/// Scales a Vec2 (right vector)
impl Mul<Vec2> for f32 {
    type Output = Vec2;

    #[inline(always)]
    fn mul(self, v: Vec2) -> Vec2 {
        v * self
    }
}

/// Scales a DVec2 (right vector)
impl Mul<DVec2> for f64 {
    type Output = DVec2;

    #[inline(always)]
    fn mul(self, v: DVec2) -> DVec2 {
        v * self
    }
}

/// Negates a vector
impl<T: PrimNum + Neg<Output = T>> Neg for Vector2<T> {
    type Output = Self;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        Self {
            x: -self.x,
            y: -self.y,
        }
    }
}

/// Returns Vector2's components based on indexes
impl<T: PrimNum> Index<usize> for Vector2<T> {
    type Output = T;

    #[inline(always)]
    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.x,
            1 => &self.y,
            _ => panic!("out of range"),
        }
    }
}

/// Returns true if x and y are equal
impl<T: FloatNum> PartialEq for Vector2<T> {
    fn eq(&self, v: &Self) -> bool {
        self.x == v.x && self.y == v.y
    }
}

impl<T: FloatNum> Eq for Vector2<T> {}
