use crate::math::{mat4::Matrix4, vec3::Vector3, vec4::Vector4, *};

#[derive(Debug, Copy, Clone, Default)]
#[repr(C)]
pub struct Quaternion<T> {
    pub x: T,
    pub y: T,
    pub z: T,
    /// real component
    pub w: T,
}

/// 32 bit float quaternion
pub type Quat = Quaternion<f32>;
/// 64 bit float quaternion
pub type DQuat = Quaternion<f64>;

impl<T: FloatNum> Quaternion<T> {
    #[inline(always)]
    fn two() -> T {
        // must do this so type of One::one() can be inferred
        let one: T = One::one();

        one + one
    }

    /// make a new quaternion from 4 components, `w` is the real component
    #[inline(always)]
    pub fn new(x: T, y: T, z: T, w: T) -> Self {
        Self { x, y, z, w }
    }

    /// make a versor, a.k.a rotation quaternion from `angle` and a `pivot`
    #[inline(always)]
    pub fn versor(mut angle: T, mut pivot: Vector3<T>) -> Self {
        // bruh, 2.0 cant be statically determined as T
        angle /= Self::two();
        pivot.normalize();
        pivot *= angle.sin();

        Self {
            x: pivot.x,
            y: pivot.y,
            z: pivot.z,
            w: angle.cos(),
        }
    }

    /// zero quarternion
    #[inline(always)]
    pub fn zero() -> Self {
        Self {
            x: Zero::zero(),
            y: Zero::zero(),
            z: Zero::zero(),
            w: Zero::zero(),
        }
    }

    /// quaternion filled with 1s
    #[inline(always)]
    pub fn one() -> Self {
        Self {
            x: One::one(),
            y: One::one(),
            z: One::one(),
            w: One::one(),
        }
    }

    /// identity quaternion where only real component is 1, others are zero
    #[inline(always)]
    pub fn identity() -> Self {
        Self {
            x: Zero::zero(),
            y: Zero::zero(),
            z: Zero::zero(),
            w: One::one(),
        }
    }

    /// normalize quaternion like a vector
    #[inline(always)]
    pub fn normalize(&mut self) {
        let length = self.len();

        self.x /= length;
        self.y /= length;
        self.z /= length;
        self.w /= length;
    }

    /// return a normalized quaternion of `self`
    #[inline(always)]
    pub fn normal(&self) -> Self {
        let length = self.len();

        Self {
            x: self.x / length,
            y: self.y / length,
            z: self.z / length,
            w: self.w / length,
        }
    }

    /// return the imaginary or vector part of the quaternion
    #[inline(always)]
    pub fn vec(&self) -> Vector3<T> {
        Vector3::from(*self)
    }

    /// return the pivot of a versor
    #[inline(always)]
    pub fn pivot(&self) -> Vector3<T> {
        Vector3::from(*self).normal()
    }

    /// return the angle of a versor
    #[inline(always)]
    pub fn angle(&self) -> T {
        let c = self.w;
        let s = self.vec().len();

        s.atan2(c) * Self::two()
    }

    /// q^2
    #[inline(always)]
    pub fn square(&self) -> Self {
        let x = self.w * self.x + self.w * self.x;
        let y = self.w * self.y + self.w * self.y;
        let z = self.w * self.z + self.w * self.z;
        let w = self.w * self.w - self.x * self.x - self.y * self.y - self.z * self.z;

        Self { x, y, z, w }
    }

    /// return magnitude of a quaternion
    #[inline(always)]
    pub fn len(&self) -> T {
        self.len2().sqrt()
    }

    /// magnitude^2
    #[inline(always)]
    pub fn len2(&self) -> T {
        Vector4::from(*self).len2()
    }

    /// dot product because quaternion multiplication is different
    #[inline(always)]
    pub fn dot(&self, q: Quaternion<T>) -> T {
        Vector4::from(*self) * Vector4::from(q)
    }

    /// conjugate of a quaternion, qq* = 1
    #[inline(always)]
    pub fn conj(&self) -> Self {
        Self {
            x: -self.x,
            y: -self.y,
            z: -self.z,
            w: self.w,
        }
    }

    /// inverse quaternion, q^-1 = q* / ||q||^2
    #[inline(always)]
    pub fn inverse(&self) -> Self {
        let d = self.len2();

        Self {
            x: -self.x / d,
            y: -self.y / d,
            z: -self.z / d,
            w: self.w / d,
        }
    }

    /// quaternion difference (not through substraction)
    #[inline(always)]
    pub fn diff(&self, q: Quaternion<T>) -> Self {
        //self.inverse() * q
        *self * q.inverse()
    }

    /// rotate a vector using a version
    #[inline(always)]
    pub fn rotate(&self, v: Vector3<T>) -> Vector3<T> {
        let qinv = self.inverse();
        let p = Self::from(v);

        Vector3::from(*self * p * qinv)
    }

    /// add an angular rotation to the current versor
    /// magnitude of `omega` should be the angle and its normalized vector
    /// is the pivot
    #[inline(always)]
    pub fn add_rotation(&mut self, omega: Vector3<T>) {
        let one: T = One::one();
        let two = one + one;
        let omega = Quaternion::from(omega);
        let old_q = *self;

        let mut q = old_q + omega * old_q / two;

        let len = q.len();

        if !(len - one).eq_zero() {
            q.normalize();
        }

        *self = q;
    }

    /// exponentiation for quaternion, q^`e`
    #[inline(always)]
    pub fn pow(&self, e: T) -> Self {
        if self.w.abs() >= One::one() {
            *self
        } else {
            let ang = self.w.acos();
            let nang = ang * e;
            let nsin = nang.sin() / ang.sin();

            Self {
                x: self.x * nsin,
                y: self.y * nsin,
                z: self.z * nsin,
                w: nang.cos(),
            }
        }
    }

    /// linear lerp for quaternion
    #[inline(always)]
    pub fn nlerp(&self, q: Quaternion<T>, s: T) -> Self {
        let d = self.dot(q);
        let qlerp = lerp(*self, q * d.signum(), s).normal();

        qlerp
    }

    /// spherical lerp for quaternion
    #[inline(always)]
    pub fn slerp(&self, q: Quaternion<T>, s: T) -> Self {
        let mut d = self.dot(q);
        let mut k1: T = One::one();
        let k2: T;

        let qq = if d.is_sign_negative() {
            d = -d;
            -q
        } else {
            q
        };

        if d > One::one() {
            k1 -= s;
            k2 = s;
        } else {
            let mut u: T = One::one();
            let w: T = u.atan2(d);

            u -= d * d;
            k1 = (k1 - s) * w;
            k1 = k1.sin() / u;
            k2 = (s * w).sin() / u;
        }

        *self * k1 + qq * k2
    }

    /// Compare if 2 vectors are approximately equal
    #[inline(always)]
    pub fn eq_approx(&self, v: Self) -> bool {
        self.x.eq_approx(v.x)
            && self.y.eq_approx(v.y)
            && self.z.eq_approx(v.z)
            && self.w.eq_approx(v.w)
    }
}

impl<T: fmt::Display> fmt::Display for Quaternion<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {}, {}, {})", self.x, self.y, self.z, self.w)
    }
}

impl<T: FloatNum> From<[T; 4]> for Quaternion<T> {
    #[inline(always)]
    fn from(arr: [T; 4]) -> Self {
        Self {
            x: arr[0],
            y: arr[1],
            z: arr[2],
            w: arr[3],
        }
    }
}

impl<T: FloatNum> From<&[T; 4]> for Quaternion<T> {
    #[inline(always)]
    fn from(arr: &[T; 4]) -> Self {
        Self {
            x: arr[0],
            y: arr[1],
            z: arr[2],
            w: arr[3],
        }
    }
}

/// Converts a tuple of primitive numbers to quaternion
impl<T: PrimNum> From<(T, T, T, T)> for Quaternion<T> {
    #[inline(always)]
    fn from(arg: (T, T, T, T)) -> Self {
        Self {
            x: arg.0,
            y: arg.1,
            z: arg.2,
            w: arg.3,
        }
    }
}

impl<T: FloatNum> From<T> for Quaternion<T> {
    #[inline(always)]
    fn from(s: T) -> Self {
        Self {
            x: s,
            y: s,
            z: s,
            w: s,
        }
    }
}

impl<T: FloatNum> From<Vector3<T>> for Quaternion<T> {
    #[inline(always)]
    fn from(v: Vector3<T>) -> Self {
        Self {
            x: v.x,
            y: v.y,
            z: v.z,
            w: Zero::zero(),
        }
    }
}

impl<T: FloatNum> From<Vector4<T>> for Quaternion<T> {
    #[inline(always)]
    fn from(v: Vector4<T>) -> Self {
        Self {
            x: v.x,
            y: v.y,
            z: v.z,
            w: v.w,
        }
    }
}

impl<T: FloatNum> From<Matrix3<T>> for Quaternion<T> {
    #[inline(always)]
    fn from(m: Matrix3<T>) -> Self {
        let mut q: Quaternion<T> = Self::zero();
        let tr = m[0][0] + m[1][1] + m[2][2];
        let one = One::one();
        let half = one / (one + one);

        if tr >= Zero::zero() {
            let s = (tr + one).sqrt();
            q.w = s * half;
            let s = half / s;
            q.x = (m[2][1] - m[1][2]) * s;
            q.y = (m[0][2] - m[2][0]) * s;
            q.z = (m[1][0] - m[0][1]) * s;
        } else {
            let mut i = 0;

            if m[1][1] > m[0][0] {
                i = 1;
            }
            if m[2][2] > m[i][i] {
                i = 2;
            }

            if i == 0 {
                let s = (m[0][0] - (m[1][1] + m[2][2]) + one).sqrt();
                q.x = half * s;
                let s = half / s;
                q.y = (m[0][1] + m[1][0]) * s;
                q.z = (m[2][0] + m[0][2]) * s;
                q.w = (m[2][1] - m[1][2]) * s;
            } else if i == 1 {
                let s = (m[1][1] - (m[2][2] + m[0][0]) + one).sqrt();
                q.y = half * s;
                let s = half / s;
                q.z = (m[1][2] + m[2][1]) * s;
                q.x = (m[0][1] + m[1][0]) * s;
                q.w = (m[0][2] - m[2][0]) * s;
            } else {
                let s = (m[2][2] - (m[0][0] + m[1][1]) + one).sqrt();
                q.z = half * s;
                let s = half / s;
                q.x = (m[2][0] + m[0][2]) * s;
                q.y = (m[1][2] + m[2][1]) * s;
                q.w = (m[1][0] - m[0][1]) * s;
            }
        }

        q
    }
}

impl<T: FloatNum> From<Matrix4<T>> for Quaternion<T> {
    #[inline(always)]
    fn from(m: Matrix4<T>) -> Self {
        let mut q: Quaternion<T> = Self::zero();
        let tr = m[0][0] + m[1][1] + m[2][2];
        let one = One::one();
        let half = one / (one + one);

        if tr >= Zero::zero() {
            let s = (tr + one).sqrt();
            q.w = s * half;
            let s = half / s;
            q.x = (m[2][1] - m[1][2]) * s;
            q.y = (m[0][2] - m[2][0]) * s;
            q.z = (m[1][0] - m[0][1]) * s;
        } else {
            let mut i = 0;

            if m[1][1] > m[0][0] {
                i = 1;
            }
            if m[2][2] > m[i][i] {
                i = 2;
            }

            if i == 0 {
                let s = (m[0][0] - (m[1][1] + m[2][2]) + one).sqrt();
                q.x = half * s;
                let s = half / s;
                q.y = (m[0][1] + m[1][0]) * s;
                q.z = (m[2][0] + m[0][2]) * s;
                q.w = (m[2][1] - m[1][2]) * s;
            } else if i == 1 {
                let s = (m[1][1] - (m[2][2] + m[0][0]) + one).sqrt();
                q.y = half * s;
                let s = half / s;
                q.z = (m[1][2] + m[2][1]) * s;
                q.x = (m[0][1] + m[1][0]) * s;
                q.w = (m[0][2] - m[2][0]) * s;
            } else {
                let s = (m[2][2] - (m[0][0] + m[1][1]) + one).sqrt();
                q.z = half * s;
                let s = half / s;
                q.x = (m[2][0] + m[0][2]) * s;
                q.y = (m[1][2] + m[2][1]) * s;
                q.w = (m[1][0] - m[0][1]) * s;
            }
        }

        q
    }
}

impl<T: FloatNum> Into<[T; 4]> for Quaternion<T> {
    #[inline(always)]
    fn into(self) -> [T; 4] {
        [self.x, self.y, self.z, self.w]
    }
}

impl<T: FloatNum> Add<Quaternion<T>> for Quaternion<T> {
    type Output = Self;

    #[inline(always)]
    fn add(self, q: Self) -> Self::Output {
        Self {
            x: self.x + q.x,
            y: self.y + q.y,
            z: self.z + q.z,
            w: self.w + q.w,
        }
    }
}

impl<T: FloatNum> Add<T> for Quaternion<T> {
    type Output = Self;

    #[inline(always)]
    fn add(self, s: T) -> Self::Output {
        Self {
            x: self.x + s,
            y: self.y + s,
            z: self.z + s,
            w: self.w + s,
        }
    }
}

impl<T: FloatNum> AddAssign<Quaternion<T>> for Quaternion<T> {
    #[inline(always)]
    fn add_assign(&mut self, q: Self) {
        self.x += q.x;
        self.y += q.y;
        self.z += q.z;
        self.w += q.w;
    }
}

impl<T: FloatNum> Sub<Quaternion<T>> for Quaternion<T> {
    type Output = Self;

    #[inline(always)]
    fn sub(self, q: Self) -> Self::Output {
        Self {
            x: self.x - q.x,
            y: self.y - q.y,
            z: self.z - q.z,
            w: self.w - q.w,
        }
    }
}

impl<T: FloatNum> Sub<T> for Quaternion<T> {
    type Output = Self;

    #[inline(always)]
    fn sub(self, s: T) -> Self::Output {
        Self {
            x: self.x - s,
            y: self.y - s,
            z: self.z - s,
            w: self.w - s,
        }
    }
}

impl<T: FloatNum> SubAssign<Quaternion<T>> for Quaternion<T> {
    #[inline(always)]
    fn sub_assign(&mut self, q: Self) {
        self.x -= q.x;
        self.y -= q.y;
        self.z -= q.z;
        self.w -= q.w;
    }
}

impl<T: FloatNum> Div<Quaternion<T>> for Quaternion<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, q: Self) -> Self::Output {
        self * q.inverse()
    }
}

impl<T: FloatNum> Div<T> for Quaternion<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, s: T) -> Self::Output {
        Self {
            x: self.x / s,
            y: self.y / s,
            z: self.z / s,
            w: self.w / s,
        }
    }
}

impl<T: FloatNum> DivAssign<Quaternion<T>> for Quaternion<T> {
    #[inline(always)]
    fn div_assign(&mut self, q: Self) {
        *self *= q.inverse()
    }
}

impl<T: FloatNum> DivAssign<T> for Quaternion<T> {
    #[inline(always)]
    fn div_assign(&mut self, s: T) {
        self.x /= s;
        self.y /= s;
        self.z /= s;
        self.w /= s;
    }
}

impl<T: FloatNum> Mul<Quaternion<T>> for Quaternion<T> {
    type Output = Self;

    fn mul(self, q: Self) -> Self::Output {
        let a = Vector3::from(self);
        let b = Vector3::from(q);
        let v = b * self.w + a * q.w + a.cross(b);

        Self {
            x: v.x,
            y: v.y,
            z: v.z,
            w: self.w * q.w - a * b,
        }
    }
}

impl<T: FloatNum> MulAssign<Quaternion<T>> for Quaternion<T> {
    fn mul_assign(&mut self, q: Self) {
        let a = Vector3::from(*self);
        let b = Vector3::from(q);
        let v = b * self.w + a * q.w + a.cross(b);

        self.x = v.x;
        self.y = v.y;
        self.z = v.z;
        self.w = self.w * q.w - a * b;
    }
}

impl<T: FloatNum> Mul<T> for Quaternion<T> {
    type Output = Self;

    #[inline(always)]
    fn mul(self, s: T) -> Self::Output {
        Self {
            x: self.x * s,
            y: self.y * s,
            z: self.z * s,
            w: self.w * s,
        }
    }
}

impl<T: FloatNum> MulAssign<T> for Quaternion<T> {
    #[inline(always)]
    fn mul_assign(&mut self, s: T) {
        self.x *= s;
        self.y *= s;
        self.z *= s;
        self.w *= s;
    }
}

impl Mul<Quat> for f32 {
    type Output = Quat;

    #[inline(always)]
    fn mul(self, q: Quat) -> Self::Output {
        q * self
    }
}

impl Mul<DQuat> for f64 {
    type Output = DQuat;

    #[inline(always)]
    fn mul(self, q: DQuat) -> Self::Output {
        q * self
    }
}

impl<T: FloatNum> Neg for Quaternion<T> {
    type Output = Self;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        Self {
            x: -self.x,
            y: -self.y,
            z: -self.z,
            w: -self.w,
        }
    }
}

impl<T: PrimNum> Index<usize> for Quaternion<T> {
    type Output = T;

    #[inline(always)]
    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            3 => &self.w,
            _ => panic!("out of range"),
        }
    }
}

impl<T: FloatNum> PartialEq for Quaternion<T> {
    fn eq(&self, q: &Self) -> bool {
        self.x == q.x && self.y == q.y && self.z == q.z && self.w == q.w
    }
}

impl<T: FloatNum> Eq for Quaternion<T> {}

#[cfg(test)]
mod tests {
    use super::*;
    use std::f32::consts::PI;

    #[test]
    fn rotate() {
        let pivot = Vec3::z();

        let q = Quat::versor(PI / 2.0, pivot);

        let x = Vec3::x();
        let result = q.rotate(x);

        println!("rotate {} 90 degrees results in {}", x, result);

        assert!(result.eq_approx(Vec3::y()));
    }

    #[test]
    fn lerp() {
        let z = Vec3::z();
        let q0 = Quat::versor(0.0, z);
        let q1 = Quat::versor(PI / 2.0, z);

        q0.nlerp(q1, 0.7);
    }

    #[test]
    fn coords() {
        let q = Quat::versor(PI / 4.0, Vec3::new(1.0, 1.0, 1.0));
        let m = Mat3::from(q).transpose();
        let x = q.rotate(Vec3::x());
        let y = q.rotate(Vec3::y());
        let z = q.rotate(Vec3::z());

        assert_eq!(m.v1, x);
        assert_eq!(m.v2, y);
        assert_eq!(m.v3, z);
    }
}
