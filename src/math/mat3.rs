use crate::math::{mat2::Matrix2, mat4::Matrix4, quat::*, vec3::Vector3, *};

// members are row vector, not conventional column vector due to cache
#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct Matrix3<T> {
    pub v1: Vector3<T>,
    pub v2: Vector3<T>,
    pub v3: Vector3<T>,
}

/// 32 bit float matrix
pub type Mat3 = Matrix3<f32>;
/// 64 bit float matrix
pub type DMat3 = Matrix3<f64>;

impl<T: FloatNum> Matrix3<T> {
    /// make a matrix from each component from left to right
    /// highly discouraged, use from() with an array instead
    #[inline(always)]
    pub fn new(m00: T, m01: T, m02: T, m10: T, m11: T, m12: T, m20: T, m21: T, m22: T) -> Self {
        Self {
            v1: Vector3::new(m00, m01, m02),
            v2: Vector3::new(m10, m11, m12),
            v3: Vector3::new(m20, m21, m22),
        }
    }

    /// make a matrix from 2 column vectors
    #[inline(always)]
    pub fn from_columns(v1: Vector3<T>, v2: Vector3<T>, v3: Vector3<T>) -> Self {
        Self {
            v1: Vector3::new(v1.x, v2.x, v3.x),
            v2: Vector3::new(v1.y, v2.y, v3.y),
            v3: Vector3::new(v1.z, v2.z, v3.z),
        }
    }

    /// return 2 column vectors
    #[inline(always)]
    pub fn into_columns(&self) -> [Vector3<T>; 3] {
        [
            Vector3::new(self.v1.x, self.v2.x, self.v3.x),
            Vector3::new(self.v1.y, self.v2.y, self.v3.y),
            Vector3::new(self.v1.z, self.v2.z, self.v3.z),
        ]
    }

    /// return a zero'd matrix
    #[inline(always)]
    pub fn zero() -> Self {
        Self {
            v1: Vector3::zero(),
            v2: Vector3::zero(),
            v3: Vector3::zero(),
        }
    }

    /// return an identity matrix
    #[inline(always)]
    pub fn identity() -> Self {
        let one: T = One::one();

        Matrix3::from(one)
    }

    /// make a tensor out of 2 vectors, `v1` is the column vector and `v2` is
    /// the row vector
    #[inline(always)]
    pub fn tensor(v1: Vector3<T>, v2: Vector3<T>) -> Self {
        Self {
            v1: v2 * v1.x,
            v2: v2 * v1.y,
            v3: v2 * v1.z,
        }
    }

    /// return a matrix transpose
    #[inline(always)]
    pub fn transpose(&self) -> Self {
        Self {
            v1: Vector3::new(self.v1.x, self.v2.x, self.v3.x),
            v2: Vector3::new(self.v1.y, self.v2.y, self.v3.y),
            v3: Vector3::new(self.v1.z, self.v2.z, self.v3.z),
        }
    }

    /// compute the determinant
    #[inline(always)]
    pub fn det(&self) -> T {
        self.v1.x * (self.v2.y * self.v3.z - self.v2.z * self.v3.y)
            - self.v2.x * (self.v1.y * self.v3.z - self.v1.z * self.v3.y)
            + self.v3.x * (self.v1.y * self.v2.z - self.v1.z * self.v2.y)
    }

    /// compute the inverse
    #[inline(always)]
    pub fn inverse(&self) -> Self {
        let new_v1x = self.v2.y * self.v3.z - self.v2.z * self.v3.y;
        let new_v2x = self.v2.z * self.v3.x - self.v2.x * self.v3.z;
        let new_v3x = self.v2.x * self.v3.y - self.v2.y * self.v3.x;

        let det = self.v1.x * new_v1x + self.v1.y * new_v2x + self.v1.z * new_v3x;

        assert!(!det.eq_zero());

        Self {
            v1: Vector3::new(
                new_v1x,
                self.v1.z * self.v3.y - self.v1.y * self.v3.z,
                self.v1.y * self.v2.z - self.v1.z * self.v2.y,
            ),
            v2: Vector3::new(
                new_v2x,
                self.v1.x * self.v3.z - self.v1.z * self.v3.x,
                self.v1.z * self.v2.x - self.v1.x * self.v2.z,
            ),
            v3: Vector3::new(
                new_v3x,
                self.v1.y * self.v3.x - self.v1.x * self.v3.y,
                self.v1.x * self.v2.y - self.v1.y * self.v2.x,
            ),
        } / det
    }

    /// compute the trace, a.k.a sum of diagonal components
    #[inline(always)]
    pub fn trace(&self) -> T {
        self.v1.x + self.v2.y + self.v3.z
    }
}

impl<T: FloatNum> Default for Matrix3<T> {
    fn default() -> Self {
        Self::identity()
    }
}

impl<T: fmt::Display> fmt::Display for Matrix3<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}\n {}\n {}]", self.v1, self.v2, self.v3)
    }
}

impl<T: FloatNum> From<T> for Matrix3<T> {
    #[inline(always)]
    fn from(s: T) -> Self {
        Self {
            v1: Vector3::new(s, Zero::zero(), Zero::zero()),
            v2: Vector3::new(Zero::zero(), s, Zero::zero()),
            v3: Vector3::new(Zero::zero(), Zero::zero(), s),
        }
    }
}

impl<T: FloatNum> From<Vector3<T>> for Matrix3<T> {
    #[inline(always)]
    fn from(v: Vector3<T>) -> Self {
        Self {
            v1: Vector3::new(v.x, Zero::zero(), Zero::zero()),
            v2: Vector3::new(Zero::zero(), v.y, Zero::zero()),
            v3: Vector3::new(Zero::zero(), Zero::zero(), v.z),
        }
    }
}

impl<T: FloatNum> From<[T; 9]> for Matrix3<T> {
    #[inline(always)]
    fn from(m: [T; 9]) -> Self {
        Self {
            v1: Vector3::new(m[0], m[1], m[2]),
            v2: Vector3::new(m[3], m[4], m[5]),
            v3: Vector3::new(m[6], m[7], m[8]),
        }
    }
}

impl<T: FloatNum> From<[[T; 3]; 3]> for Matrix3<T> {
    #[inline(always)]
    fn from(m: [[T; 3]; 3]) -> Self {
        Self {
            v1: Vector3::from(m[0]),
            v2: Vector3::from(m[1]),
            v3: Vector3::from(m[2]),
        }
    }
}

impl<T: FloatNum> From<[Vector3<T>; 3]> for Matrix3<T> {
    #[inline(always)]
    fn from(m: [Vector3<T>; 3]) -> Self {
        Self {
            v1: m[0],
            v2: m[1],
            v3: m[2],
        }
    }
}

impl<T: FloatNum> From<Matrix2<T>> for Matrix3<T> {
    #[inline(always)]
    fn from(m: Matrix2<T>) -> Self {
        Self {
            v1: Vector3::from(m.v1),
            v2: Vector3::from(m.v2),
            v3: Vector3::new(Zero::zero(), Zero::zero(), One::one()),
        }
    }
}

impl<T: FloatNum> From<Matrix4<T>> for Matrix3<T> {
    #[inline(always)]
    fn from(m: Matrix4<T>) -> Self {
        Self {
            v1: Vector3::from(m.v1),
            v2: Vector3::from(m.v2),
            v3: Vector3::from(m.v3),
        }
    }
}

impl<T: FloatNum> From<Quaternion<T>> for Matrix3<T> {
    #[inline(always)]
    fn from(q: Quaternion<T>) -> Self {
        let one: T = One::one();
        let two: T = one + one;
        let x2 = q.x * q.x;
        let y2 = q.y * q.y;
        let z2 = q.z * q.z;
        let xw = q.x * q.w;
        let yw = q.y * q.w;
        let zw = q.z * q.w;

        Self::from([
            one - two * (y2 + z2),
            two * (q.x * q.y - zw),
            two * (q.x * q.z + yw),
            two * (q.x * q.y + zw),
            one - two * (x2 + z2),
            two * (q.y * q.z - xw),
            two * (q.x * q.z - yw),
            two * (q.y * q.z + xw),
            one - two * (x2 + y2),
        ])
    }
}

impl<T: FloatNum> Into<[[T; 3]; 3]> for Matrix3<T> {
    #[inline(always)]
    fn into(self) -> [[T; 3]; 3] {
        [
            [self.v1.x, self.v1.y, self.v1.z],
            [self.v2.x, self.v2.y, self.v2.z],
            [self.v3.x, self.v3.y, self.v3.z],
        ]
    }
}

impl<T: FloatNum> Into<[Vector3<T>; 3]> for Matrix3<T> {
    #[inline(always)]
    fn into(self) -> [Vector3<T>; 3] {
        [self.v1, self.v2, self.v3]
    }
}

impl<T: FloatNum> Add<Matrix3<T>> for Matrix3<T> {
    type Output = Self;

    #[inline(always)]
    fn add(self, m: Self) -> Self::Output {
        Self {
            v1: self.v1 + m.v1,
            v2: self.v2 + m.v2,
            v3: self.v3 + m.v3,
        }
    }
}

impl<T: FloatNum> AddAssign<Matrix3<T>> for Matrix3<T> {
    #[inline(always)]
    fn add_assign(&mut self, m: Self) {
        self.v1 += m.v1;
        self.v2 += m.v2;
        self.v3 += m.v3;
    }
}

impl<T: FloatNum> Sub<Matrix3<T>> for Matrix3<T> {
    type Output = Self;

    #[inline(always)]
    fn sub(self, m: Self) -> Self::Output {
        Self {
            v1: self.v1 - m.v1,
            v2: self.v2 - m.v2,
            v3: self.v3 - m.v3,
        }
    }
}

impl<T: FloatNum> SubAssign<Matrix3<T>> for Matrix3<T> {
    #[inline(always)]
    fn sub_assign(&mut self, m: Self) {
        self.v1 -= m.v1;
        self.v2 -= m.v2;
        self.v3 -= m.v3;
    }
}

impl<T: FloatNum> Div<Matrix3<T>> for Matrix3<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, m: Self) -> Self::Output {
        self * m.inverse()
    }
}

impl<T: FloatNum> DivAssign<Matrix3<T>> for Matrix3<T> {
    #[inline(always)]
    fn div_assign(&mut self, m: Self) {
        *self *= m.inverse()
    }
}

impl<T: FloatNum> Div<T> for Matrix3<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, s: T) -> Self::Output {
        Self {
            v1: self.v1 / s,
            v2: self.v2 / s,
            v3: self.v3 / s,
        }
    }
}

impl<T: FloatNum> DivAssign<T> for Matrix3<T> {
    #[inline(always)]
    fn div_assign(&mut self, s: T) {
        self.v1 /= s;
        self.v2 /= s;
        self.v3 /= s;
    }
}

impl<T: FloatNum> Mul<Matrix3<T>> for Matrix3<T> {
    type Output = Self;

    #[inline(always)]
    fn mul(self, m: Self) -> Self::Output {
        Self {
            v1: Vector3::new(
                self.v1.x * m.v1.x + self.v1.y * m.v2.x + self.v1.z * m.v3.x,
                self.v1.x * m.v1.y + self.v1.y * m.v2.y + self.v1.z * m.v3.y,
                self.v1.x * m.v1.z + self.v1.y * m.v2.z + self.v1.z * m.v3.z,
            ),
            v2: Vector3::new(
                self.v2.x * m.v1.x + self.v2.y * m.v2.x + self.v2.z * m.v3.x,
                self.v2.x * m.v1.y + self.v2.y * m.v2.y + self.v2.z * m.v3.y,
                self.v2.x * m.v1.z + self.v2.y * m.v2.z + self.v2.z * m.v3.z,
            ),
            v3: Vector3::new(
                self.v3.x * m.v1.x + self.v3.y * m.v2.x + self.v3.z * m.v3.x,
                self.v3.x * m.v1.y + self.v3.y * m.v2.y + self.v3.z * m.v3.y,
                self.v3.x * m.v1.z + self.v3.y * m.v2.z + self.v3.z * m.v3.z,
            ),
        }
    }
}

impl<T: FloatNum> MulAssign<Matrix3<T>> for Matrix3<T> {
    #[inline(always)]
    fn mul_assign(&mut self, m: Self) {
        let v1 = Vector3::new(
            self.v1.x * m.v1.x + self.v1.y * m.v2.x + self.v1.z * m.v3.x,
            self.v1.x * m.v1.y + self.v1.y * m.v2.y + self.v1.z * m.v3.y,
            self.v1.x * m.v1.z + self.v1.y * m.v2.z + self.v1.z * m.v3.z,
        );
        let v2 = Vector3::new(
            self.v2.x * m.v1.x + self.v2.y * m.v2.x + self.v2.z * m.v3.x,
            self.v2.x * m.v1.y + self.v2.y * m.v2.y + self.v2.z * m.v3.y,
            self.v2.x * m.v1.z + self.v2.y * m.v2.z + self.v2.z * m.v3.z,
        );
        let v3 = Vector3::new(
            self.v3.x * m.v1.x + self.v3.y * m.v2.x + self.v3.z * m.v3.x,
            self.v3.x * m.v1.y + self.v3.y * m.v2.y + self.v3.z * m.v3.y,
            self.v3.x * m.v1.z + self.v3.y * m.v2.z + self.v3.z * m.v3.z,
        );

        self.v1 = v1;
        self.v2 = v2;
        self.v3 = v3;
    }
}

impl<T: FloatNum> Mul<T> for Matrix3<T> {
    type Output = Self;

    #[inline(always)]
    fn mul(self, s: T) -> Self::Output {
        Self {
            v1: self.v1 * s,
            v2: self.v2 * s,
            v3: self.v3 * s,
        }
    }
}

impl<T: FloatNum> MulAssign<T> for Matrix3<T> {
    #[inline(always)]
    fn mul_assign(&mut self, s: T) {
        self.v1 *= s;
        self.v2 *= s;
        self.v3 *= s;
    }
}

impl Mul<Mat3> for f32 {
    type Output = Mat3;

    #[inline(always)]
    fn mul(self, m: Mat3) -> Self::Output {
        m * self
    }
}

impl Mul<DMat3> for f64 {
    type Output = DMat3;

    #[inline(always)]
    fn mul(self, m: DMat3) -> Self::Output {
        m * self
    }
}

impl<T: FloatNum> Mul<Vector3<T>> for Matrix3<T> {
    type Output = Vector3<T>;

    #[inline(always)]
    fn mul(self, v: Vector3<T>) -> Self::Output {
        Vector3::new(self.v1 * v, self.v2 * v, self.v3 * v)
    }
}

impl<T: FloatNum> Neg for Matrix3<T> {
    type Output = Self;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        Self {
            v1: -self.v1,
            v2: -self.v2,
            v3: -self.v3,
        }
    }
}

impl<T: FloatNum> Index<usize> for Matrix3<T> {
    type Output = Vector3<T>;

    #[inline(always)]
    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.v1,
            1 => &self.v2,
            2 => &self.v3,
            _ => panic!("out of range"),
        }
    }
}
