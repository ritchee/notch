use crate::math::{mat3::*, quat::Quaternion, vec2::Vector2, vec4::Vector4, *};

/// A 3-dimensional vector
#[derive(Debug, Copy, Clone, Default)]
#[repr(C)]
pub struct Vector3<T> {
    pub x: T,
    pub y: T,
    pub z: T,
}

/// Int32 Vector
pub type IVec3 = Vector3<i32>;

/// UInt32 Vector
pub type UVec3 = Vector3<u32>;

/// Float32 Vector
pub type Vec3 = Vector3<f32>;

/// Float64 Vector
pub type DVec3 = Vector3<f64>;

/// The struct Vector3 accepts generic T type with PrimNum trait, which is in short for primitive number
impl<T: PrimNum> Vector3<T> {
    /// Creates a new vector
    #[inline(always)]
    pub fn new(x: T, y: T, z: T) -> Self {
        Self { x, y, z }
    }

    /// Creates a vector with the default value '0'
    #[inline(always)]
    pub fn zero() -> Self {
        Self {
            x: Zero::zero(),
            y: Zero::zero(),
            z: Zero::zero(),
        }
    }

    /// Creates a vector with the default value '1'
    #[inline(always)]
    pub fn one() -> Self {
        Self {
            x: One::one(),
            y: One::one(),
            z: One::one(),
        }
    }

    /// Computes cross product
    #[inline(always)]
    pub fn cross(&self, v: Self) -> Self {
        Self {
            x: self.y * v.z - self.z * v.y,
            y: self.z * v.x - self.x * v.z,
            z: self.x * v.y - self.y * v.x,
        }
    }

    /// Computes the tensor matrix
    #[inline(always)]
    pub fn tensor(&self) -> Matrix3<T> {
        Matrix3 {
            v1: *self * self.x,
            v2: *self * self.y,
            v3: *self * self.z,
        }
    }
}

/// The struct Vector3 accepts generic T type with FloatNum trait, which is in short for float number
impl<T: FloatNum> Vector3<T> {
    /// produce an x vector
    #[inline(always)]
    pub fn x() -> Self {
        Self {
            x: One::one(),
            y: Zero::zero(),
            z: Zero::zero(),
        }
    }

    /// produce an y vector
    #[inline(always)]
    pub fn y() -> Self {
        Self {
            x: Zero::zero(),
            y: One::one(),
            z: Zero::zero(),
        }
    }

    /// produce an z vector
    #[inline(always)]
    pub fn z() -> Self {
        Self {
            x: Zero::zero(),
            y: Zero::zero(),
            z: One::one(),
        }
    }

    /// Returns true if both x, y are 0, else Returns false
    #[inline(always)]
    pub fn is_zero(&self) -> bool {
        self.x.eq_zero() && self.y.eq_zero() && self.z.eq_zero()
    }

    /// normalize current vector
    #[inline(always)]
    pub fn normalize(&mut self) {
        let length = self.len();

        self.x /= length;
        self.y /= length;
        self.z /= length;
    }

    /// Creates a normalized vector
    #[inline(always)]
    pub fn normal(&self) -> Self {
        let length = self.len();

        Self {
            x: self.x / length,
            y: self.y / length,
            z: self.z / length,
        }
    }

    /// Returns the sum of x square, y square, and z square
    #[inline(always)]
    pub fn len2(&self) -> T {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    /// Returns the magnitude of a vector
    #[inline(always)]
    pub fn len(&self) -> T {
        self.len2().sqrt()
    }

    /// Computes vector projection
    #[inline(always)]
    pub fn project(&self, v: Self) -> Self {
        v * ((*self * v) / (v * v))
    }

    /// Projects onto a unit vector
    #[inline(always)]
    pub fn project_n(&self, n: Self) -> Self {
        n * (*self * n)
    }

    /// Compute the reject vector of a projection
    #[inline(always)]
    pub fn reject(&self, v: Self) -> Self {
        *self - v * ((*self * v) / (v * v))
    }

    /// Returns the distance
    #[inline(always)]
    pub fn distance(&self, v: Self) -> T {
        (*self - v).len()
    }

    /// Computes angle between two vectors using dot product
    #[inline(always)]
    pub fn angle(&self, v: Self) -> T {
        let dot = *self * v;
        let denom = (self.len2() * v.len2()).sqrt();

        (dot / denom).acos()
    }

    /// Computes counterclockwise normalized orthogonal of vector
    #[inline(always)]
    pub fn ortho(&self) -> Self {
        self.left_ortho().normal()
    }

    /// Computes counterclockwise unnormalized orthogonal of vector
    #[inline(always)]
    pub fn left_ortho(&self) -> Self {
        Self {
            x: -self.y,
            y: self.x,
            z: Zero::zero(),
        }
    }

    /// Computes clockwise unnormalized orthogonal of vector
    #[inline(always)]
    pub fn right_ortho(&self) -> Self {
        Self {
            x: self.y,
            y: -self.x,
            z: Zero::zero(),
        }
    }

    /// Computes absolute values of a vector
    #[inline(always)]
    pub fn abs(&self) -> Self {
        Self {
            x: self.x.abs(),
            y: self.y.abs(),
            z: self.z.abs(),
        }
    }

    /// Returns a vector with minimum values
    #[inline(always)]
    pub fn min_vec(&self, v: Self) -> Self {
        Self {
            x: self.x.min(v.x),
            y: self.y.min(v.y),
            z: self.z.min(v.z),
        }
    }

    /// Returns a vector with maximum values
    #[inline(always)]
    pub fn max_vec(&self, v: Self) -> Self {
        Self {
            x: self.x.max(v.x),
            y: self.y.max(v.y),
            z: self.z.max(v.z),
        }
    }

    /// make a cross product matrix
    #[inline(always)]
    pub fn to_cross(&self) -> Matrix3<T> {
        Matrix3::new(
            Zero::zero(),
            -self.z,
            self.y,
            self.z,
            Zero::zero(),
            -self.x,
            -self.y,
            self.x,
            Zero::zero(),
        )
    }

    /// Compare if 2 vectors are approximately equal
    #[inline(always)]
    pub fn eq_approx(&self, v: Self) -> bool {
        self.x.eq_approx(v.x) && self.y.eq_approx(v.y) && self.z.eq_approx(v.z)
    }
}

/// Implements Display trait for struct Vector3
/// Implements this trait for a type will automatically implement the ToString trait for the type, allowing the usage of the .to_string() method.
impl<T: fmt::Display> fmt::Display for Vector3<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}, {}, {}]", self.x, self.y, self.z)
    }
}

/// Converts an array of primitive numbers to Vector3
impl<T: PrimNum> From<[T; 3]> for Vector3<T> {
    #[inline(always)]
    fn from(arr: [T; 3]) -> Self {
        Self {
            x: arr[0],
            y: arr[1],
            z: arr[2],
        }
    }
}

/// Converts a reference to array to Vector3
impl<T: PrimNum> From<&[T; 3]> for Vector3<T> {
    #[inline(always)]
    fn from(arr: &[T; 3]) -> Self {
        Self {
            x: arr[0],
            y: arr[1],
            z: arr[2],
        }
    }
}

/// Converts a tuple of primitive numbers to Vector3
impl<T: PrimNum> From<(T, T, T)> for Vector3<T> {
    #[inline(always)]
    fn from(arg: (T, T, T)) -> Self {
        Self {
            x: arg.0,
            y: arg.1,
            z: arg.2,
        }
    }
}

/// Splats a number into a vector
impl<T: PrimNum> From<T> for Vector3<T> {
    #[inline(always)]
    fn from(s: T) -> Self {
        Self { x: s, y: s, z: s }
    }
}

/// Converts a 2D-vector into Vector3
impl<T: PrimNum> From<Vector2<T>> for Vector3<T> {
    #[inline(always)]
    fn from(v: Vector2<T>) -> Self {
        Self {
            x: v.x,
            y: v.y,
            z: Zero::zero(),
        }
    }
}

/// Converts a 4D-vector into Vector3
impl<T: PrimNum> From<Vector4<T>> for Vector3<T> {
    #[inline(always)]
    fn from(v: Vector4<T>) -> Self {
        Self {
            x: v.x,
            y: v.y,
            z: v.z,
        }
    }
}

/// Extract imaginary vector from a quaternion
impl<T: FloatNum> From<Quaternion<T>> for Vector3<T> {
    #[inline(always)]
    fn from(q: Quaternion<T>) -> Self {
        Self {
            x: q.x,
            y: q.y,
            z: q.z,
        }
    }
}

/// Expands precision of Vector3
impl From<Vec3> for DVec3 {
    #[inline(always)]
    fn from(v: Vec3) -> Self {
        Self {
            x: v.x as f64,
            y: v.y as f64,
            z: v.z as f64,
        }
    }
}

/// TODO
impl From<DVec3> for Vec3 {
    #[inline(always)]
    fn from(v: DVec3) -> Self {
        Self {
            x: v.x as f32,
            y: v.y as f32,
            z: v.z as f32,
        }
    }
}

/// Converts from Vector3 into an array
impl<T: PrimNum> Into<[T; 3]> for Vector3<T> {
    #[inline(always)]
    fn into(self) -> [T; 3] {
        [self.x, self.y, self.z]
    }
}

/// Adds 2 3D-vectors
impl<T: PrimNum> Add<Vector3<T>> for Vector3<T> {
    type Output = Self;

    #[inline(always)]
    fn add(self, v: Self) -> Self::Output {
        Self {
            x: self.x + v.x,
            y: self.y + v.y,
            z: self.z + v.z,
        }
    }
}

/// Adds a constant to Vector3's components
impl<T: PrimNum> Add<T> for Vector3<T> {
    type Output = Self;

    #[inline(always)]
    fn add(self, s: T) -> Self::Output {
        Self {
            x: self.x + s,
            y: self.y + s,
            z: self.z + s,
        }
    }
}

/// Adds and assigns a vector
impl<T: PrimNum> AddAssign<Vector3<T>> for Vector3<T> {
    #[inline(always)]
    fn add_assign(&mut self, v: Self) {
        self.x += v.x;
        self.y += v.y;
        self.z += v.z;
    }
}

/// Subtracts 2 3D-vectors
impl<T: PrimNum> Sub<Vector3<T>> for Vector3<T> {
    type Output = Self;

    #[inline(always)]
    fn sub(self, v: Self) -> Self::Output {
        Self {
            x: self.x - v.x,
            y: self.y - v.y,
            z: self.z - v.z,
        }
    }
}

/// Subtracts a constant from a vector
impl<T: PrimNum> Sub<T> for Vector3<T> {
    type Output = Self;

    #[inline(always)]
    fn sub(self, s: T) -> Self::Output {
        Self {
            x: self.x - s,
            y: self.y - s,
            z: self.z - s,
        }
    }
}

/// Subtracts and assigns a vector
impl<T: PrimNum> SubAssign<Vector3<T>> for Vector3<T> {
    #[inline(always)]
    fn sub_assign(&mut self, v: Self) {
        self.x -= v.x;
        self.y -= v.y;
        self.z -= v.z;
    }
}

/// Divides a vector by another vector, returns a vector of each result
impl<T: PrimNum> Div<Vector3<T>> for Vector3<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, v: Vector3<T>) -> Self::Output {
        Self {
            x: self.x / v.x,
            y: self.y / v.y,
            z: self.z / v.z,
        }
    }
}

/// Divides and assigns a vector by another vector
impl<T: PrimNum> DivAssign<Vector3<T>> for Vector3<T> {
    #[inline(always)]
    fn div_assign(&mut self, v: Vector3<T>) {
        self.x /= v.x;
        self.y /= v.y;
        self.z /= v.z;
    }
}

/// Divides a constant from a vector
impl<T: PrimNum> Div<T> for Vector3<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, s: T) -> Self::Output {
        Self {
            x: self.x / s,
            y: self.y / s,
            z: self.z / s,
        }
    }
}

/// Divides and assigns a vector
impl<T: PrimNum> DivAssign<T> for Vector3<T> {
    #[inline(always)]
    fn div_assign(&mut self, s: T) {
        self.x /= s;
        self.y /= s;
        self.z /= s;
    }
}

/// Computes dot product
impl<T: PrimNum> Mul<Vector3<T>> for Vector3<T> {
    type Output = T;

    #[inline(always)]
    fn mul(self, v: Self) -> Self::Output {
        self.x * v.x + self.y * v.y + self.z * v.z
    }
}

/// Scales a vector
impl<T: PrimNum> Mul<T> for Vector3<T> {
    type Output = Self;

    #[inline(always)]
    fn mul(self, s: T) -> Self::Output {
        Self {
            x: self.x * s,
            y: self.y * s,
            z: self.z * s,
        }
    }
}

/// Multiplies and assigns Vector2 with a factor
impl<T: PrimNum> MulAssign<T> for Vector3<T> {
    #[inline(always)]
    fn mul_assign(&mut self, s: T) {
        self.x *= s;
        self.y *= s;
        self.z *= s;
    }
}

/// Scales a Vec3 (right vector)
impl Mul<Vec3> for f32 {
    type Output = Vec3;

    #[inline(always)]
    fn mul(self, v: Vec3) -> Vec3 {
        v * self
    }
}

/// Scales a DVec3 (right vector)
impl Mul<DVec3> for f64 {
    type Output = DVec3;

    #[inline(always)]
    fn mul(self, v: DVec3) -> DVec3 {
        v * self
    }
}

/// Negates a vector
impl<T: PrimNum + Neg<Output = T>> Neg for Vector3<T> {
    type Output = Self;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        Self {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

/// Returns Vector3's components based on indexes
impl<T: PrimNum> Index<usize> for Vector3<T> {
    type Output = T;

    #[inline(always)]
    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            _ => panic!("out of range"),
        }
    }
}

/// Returns true if x and y are equal
impl<T: FloatNum> PartialEq for Vector3<T> {
    fn eq(&self, v: &Self) -> bool {
        self.x == v.x && self.y == v.y && self.z == v.z
    }
}

impl<T: FloatNum> Eq for Vector3<T> {}
