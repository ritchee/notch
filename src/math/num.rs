use super::*;

pub const ERROR_THRESHOLD: f32 = 1e-2;

/// actual epsilon values, not machine epsilon or ulp
pub const F32_EPSILON: f32 = 1e-6;
pub const F64_EPSILON: f64 = 1e-9;

/// FloatEq allows comparing floating point values approximately
pub trait FloatEq {
    /// hybrid comparison
    fn eq_approx(self, num: Self) -> bool;
    /// relative float comparison method, do not use this on zero
    fn eq_rel(self, num: Self) -> bool;
    /// absolute float comparison method, should be used on small values
    fn eq_abs(self, num: Self) -> bool;
    /// compare a value with zero, used this to confirm if a value is zero
    fn eq_zero(self) -> bool;
}

impl FloatEq for f32 {
    fn eq_approx(self, num: Self) -> bool {
        let max = self.max(num).max(1.0);
        let diff = num - self;

        diff.abs() < F32_EPSILON * max
    }

    fn eq_rel(self, num: Self) -> bool {
        let max = self.max(num);
        let diff = num - self;

        diff.abs() < F32_EPSILON * max
    }

    fn eq_abs(self, num: Self) -> bool {
        let diff = num - self;

        diff.abs() < F32_EPSILON
    }

    fn eq_zero(self) -> bool {
        self.abs() < F32_EPSILON
    }
}

impl FloatEq for f64 {
    fn eq_approx(self, num: Self) -> bool {
        let max = self.max(num);
        let diff = num - self;

        diff.abs() < F64_EPSILON * max.max(1.0)
    }

    fn eq_rel(self, num: Self) -> bool {
        let max = self.max(num);
        let diff = num - self;

        diff.abs() < max * F64_EPSILON
    }

    fn eq_abs(self, num: Self) -> bool {
        let diff = num - self;

        diff.abs() < F64_EPSILON
    }

    fn eq_zero(self) -> bool {
        self.abs() < F64_EPSILON
    }
}

/// Internal boilerplate trait for primitive number types
pub trait PrimNum:
    Copy + Clone + PartialOrd + PartialEq + Sized + Num + Bounded + One + Zero + NumOps + NumAssignOps
{
}

impl PrimNum for i32 {}
impl PrimNum for u32 {}
impl PrimNum for f32 {}
impl PrimNum for f64 {}

/// like PrimNum but specifically for floating point values
pub trait FloatNum: PrimNum + Float + FloatEq {}

impl FloatNum for f32 {}
impl FloatNum for f64 {}
