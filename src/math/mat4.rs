use crate::math::{mat2::Matrix2, mat3::Matrix3, quat::*, vec4::Vector4, *};

// members are row vector, not conventional column vector due to cache and mul
#[derive(Debug, Copy, Clone)]
#[repr(C)]
pub struct Matrix4<T> {
    pub v1: Vector4<T>,
    pub v2: Vector4<T>,
    pub v3: Vector4<T>,
    pub v4: Vector4<T>,
}

/// 32 bit float matrix
pub type Mat4 = Matrix4<f32>;
/// 64 bit float matrix
pub type DMat4 = Matrix4<f64>;

impl<T: FloatNum> Matrix4<T> {
    /// make a matrix from each component from left to right
    /// highly discouraged, use from() with an array instead
    #[inline(always)]
    pub fn new(
        m00: T,
        m01: T,
        m02: T,
        m03: T,
        m10: T,
        m11: T,
        m12: T,
        m13: T,
        m20: T,
        m21: T,
        m22: T,
        m23: T,
        m30: T,
        m31: T,
        m32: T,
        m33: T,
    ) -> Self {
        Self {
            v1: Vector4::new(m00, m01, m02, m03),
            v2: Vector4::new(m10, m11, m12, m13),
            v3: Vector4::new(m20, m21, m22, m23),
            v4: Vector4::new(m30, m31, m32, m33),
        }
    }

    /// make a matrix from 2 column vectors
    #[inline(always)]
    pub fn from_columns(v1: Vector4<T>, v2: Vector4<T>, v3: Vector4<T>, v4: Vector4<T>) -> Self {
        Self {
            v1: Vector4::new(v1.x, v2.x, v3.x, v4.x),
            v2: Vector4::new(v1.y, v2.y, v3.y, v4.y),
            v3: Vector4::new(v1.z, v2.z, v3.z, v4.z),
            v4: Vector4::new(v1.w, v2.w, v3.w, v4.w),
        }
    }

    /// return 2 column vectors
    #[inline(always)]
    pub fn into_columns(&self) -> [Vector4<T>; 4] {
        [
            Vector4::new(self.v1.x, self.v2.x, self.v3.x, self.v4.x),
            Vector4::new(self.v1.y, self.v2.y, self.v3.y, self.v4.y),
            Vector4::new(self.v1.z, self.v2.z, self.v3.z, self.v4.z),
            Vector4::new(self.v1.w, self.v2.w, self.v3.w, self.v4.w),
        ]
    }

    /// return a zero'd matrix
    #[inline(always)]
    pub fn zero() -> Self {
        Self {
            v1: Vector4::zero(),
            v2: Vector4::zero(),
            v3: Vector4::zero(),
            v4: Vector4::zero(),
        }
    }

    /// return an identity matrix
    #[inline(always)]
    pub fn identity() -> Self {
        let one: T = One::one();

        Matrix4::from(one)
    }

    /// make a tensor out of 2 vectors, `v1` is the column vector and `v2` is
    /// the row vector
    #[inline(always)]
    pub fn tensor(v1: Vector4<T>, v2: Vector4<T>) -> Self {
        Self {
            v1: v2 * v1.x,
            v2: v2 * v1.y,
            v3: v2 * v1.z,
            v4: v2 * v1.w,
        }
    }

    /// return a matrix transpose
    #[inline(always)]
    pub fn transpose(&self) -> Self {
        Self {
            v1: Vector4::new(self.v1.x, self.v2.x, self.v3.x, self.v4.x),
            v2: Vector4::new(self.v1.y, self.v2.y, self.v3.y, self.v4.y),
            v3: Vector4::new(self.v1.z, self.v2.z, self.v3.z, self.v4.z),
            v4: Vector4::new(self.v1.w, self.v2.w, self.v3.w, self.v4.w),
        }
    }

    /// compute the determinant
    /// TODO: compute some common terms
    #[inline(always)]
    pub fn det(&self) -> T {
        (self.v1.x
            * (self.v2.y * self.v3.z * self.v4.w
                + self.v2.z * self.v3.w * self.v4.y
                + self.v2.w * self.v3.y * self.v4.z
                - self.v2.w * self.v3.z * self.v4.y
                - self.v2.z * self.v3.y * self.v4.w
                - self.v2.y * self.v3.w * self.v4.z))
            - (self.v2.x
                * (self.v1.y * self.v3.z * self.v4.w
                    + self.v1.z * self.v3.w * self.v4.y
                    + self.v1.w * self.v3.y * self.v4.z
                    - self.v1.w * self.v3.z * self.v4.y
                    - self.v1.z * self.v3.y * self.v4.w
                    - self.v1.y * self.v3.w * self.v4.z))
            + (self.v3.x
                * (self.v1.y * self.v2.z * self.v4.w
                    + self.v1.z * self.v2.w * self.v4.y
                    + self.v1.w * self.v2.y * self.v4.z
                    - self.v1.w * self.v2.z * self.v4.y
                    - self.v1.z * self.v2.y * self.v4.w
                    - self.v1.y * self.v2.w * self.v4.z))
            - (self.v4.x
                * (self.v1.y * self.v2.z * self.v3.w
                    + self.v1.z * self.v2.w * self.v3.y
                    + self.v1.w * self.v2.y * self.v3.z
                    - self.v1.w * self.v2.z * self.v3.y
                    - self.v1.z * self.v2.y * self.v3.w
                    - self.v1.y * self.v2.w * self.v3.z))
    }

    /// compute the inverse
    /// very unefficient currently
    /// do use inverse on a matrix3 and convert to matrix4 for now
    /// TODO: compute some common terms and inline determinant computation
    #[inline(always)]
    pub fn inverse(&self) -> Self {
        let det = self.det();

        assert!(!det.eq_zero());

        // Cayley–Hamilton method
        // very inefficient than hardcoded version due to the matrix being
        // powered
        let m = *self;
        let m2 = m * m;
        let m3 = m2 * m;
        let tr = m.trace();
        let tr2 = m2.trace();
        let tr3 = m3.trace();

        // worst thing to do when generics is inverseolved
        let two: T = One::one();
        let two = two + One::one();
        let three = two + One::one();
        let six = two * three;

        (Matrix4::from(((tr.powi(3)) - three * tr * tr2 + two * tr3) / six)
            - m * ((tr.powi(2) - tr2) / two)
            + m2 * tr
            - m3)
            / det
    }

    /// compute the trace, a.k.a sum of diagonal components
    #[inline(always)]
    pub fn trace(&self) -> T {
        self.v1.x + self.v2.y + self.v3.z + self.v4.w
    }
}

impl<T: FloatNum> Default for Matrix4<T> {
    fn default() -> Self {
        Self::identity()
    }
}

impl<T: fmt::Display> fmt::Display for Matrix4<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}\n {}\n {}\n {}]", self.v1, self.v2, self.v3, self.v4)
    }
}

impl<T: FloatNum> From<T> for Matrix4<T> {
    #[inline(always)]
    fn from(s: T) -> Self {
        Self {
            v1: Vector4::new(s, Zero::zero(), Zero::zero(), Zero::zero()),
            v2: Vector4::new(Zero::zero(), s, Zero::zero(), Zero::zero()),
            v3: Vector4::new(Zero::zero(), Zero::zero(), s, Zero::zero()),
            v4: Vector4::new(Zero::zero(), Zero::zero(), Zero::zero(), s),
        }
    }
}

impl<T: FloatNum> From<Vector4<T>> for Matrix4<T> {
    #[inline(always)]
    fn from(v: Vector4<T>) -> Self {
        Self {
            v1: Vector4::new(v.x, Zero::zero(), Zero::zero(), Zero::zero()),
            v2: Vector4::new(Zero::zero(), v.y, Zero::zero(), Zero::zero()),
            v3: Vector4::new(Zero::zero(), Zero::zero(), v.z, Zero::zero()),
            v4: Vector4::new(Zero::zero(), Zero::zero(), Zero::zero(), v.w),
        }
    }
}

impl<T: FloatNum> From<[T; 16]> for Matrix4<T> {
    #[inline(always)]
    fn from(m: [T; 16]) -> Self {
        Self {
            v1: Vector4::new(m[0], m[1], m[2], m[3]),
            v2: Vector4::new(m[4], m[5], m[6], m[7]),
            v3: Vector4::new(m[8], m[9], m[10], m[11]),
            v4: Vector4::new(m[12], m[13], m[14], m[15]),
        }
    }
}

impl<T: FloatNum> From<[[T; 4]; 4]> for Matrix4<T> {
    #[inline(always)]
    fn from(m: [[T; 4]; 4]) -> Self {
        Self {
            v1: Vector4::from(m[0]),
            v2: Vector4::from(m[1]),
            v3: Vector4::from(m[2]),
            v4: Vector4::from(m[3]),
        }
    }
}

impl<T: FloatNum> From<[Vector4<T>; 4]> for Matrix4<T> {
    #[inline(always)]
    fn from(m: [Vector4<T>; 4]) -> Self {
        Self {
            v1: m[0],
            v2: m[1],
            v3: m[2],
            v4: m[3],
        }
    }
}

impl<T: FloatNum> From<Matrix2<T>> for Matrix4<T> {
    #[inline(always)]
    fn from(m: Matrix2<T>) -> Self {
        Self {
            v1: Vector4::from(m.v1),
            v2: Vector4::from(m.v2),
            v3: Vector4::new(Zero::zero(), Zero::zero(), One::one(), Zero::zero()),
            v4: Vector4::new(Zero::zero(), Zero::zero(), Zero::zero(), One::one()),
        }
    }
}

impl<T: FloatNum> From<Matrix3<T>> for Matrix4<T> {
    #[inline(always)]
    fn from(m: Matrix3<T>) -> Self {
        Self {
            v1: Vector4::from(m.v1),
            v2: Vector4::from(m.v2),
            v3: Vector4::from(m.v3),
            v4: Vector4::new(Zero::zero(), Zero::zero(), Zero::zero(), One::one()),
        }
    }
}

impl<T: FloatNum> From<Quaternion<T>> for Matrix4<T> {
    #[inline(always)]
    fn from(q: Quaternion<T>) -> Self {
        let one: T = One::one();
        let two: T = one + one;
        let x2 = q.x * q.x;
        let y2 = q.y * q.y;
        let z2 = q.z * q.z;
        let xw = q.x * q.w;
        let yw = q.y * q.w;
        let zw = q.z * q.w;

        Self::from([
            one - two * (y2 + z2),
            two * (q.x * q.y - zw),
            two * (q.x * q.z + yw),
            Zero::zero(),
            two * (q.x * q.y + zw),
            one - two * (x2 + z2),
            two * (q.y * q.z - xw),
            Zero::zero(),
            two * (q.x * q.z - yw),
            two * (q.y * q.z + xw),
            one - two * (x2 + y2),
            Zero::zero(),
            Zero::zero(),
            Zero::zero(),
            Zero::zero(),
            One::one(),
        ])
    }
}

impl<T: FloatNum> Into<[[T; 4]; 4]> for Matrix4<T> {
    #[inline(always)]
    fn into(self) -> [[T; 4]; 4] {
        [
            [self.v1.x, self.v1.y, self.v1.z, self.v1.w],
            [self.v2.x, self.v2.y, self.v2.z, self.v2.w],
            [self.v3.x, self.v3.y, self.v3.z, self.v3.w],
            [self.v4.x, self.v4.y, self.v4.z, self.v4.w],
        ]
    }
}

impl<T: FloatNum> Into<[Vector4<T>; 4]> for Matrix4<T> {
    #[inline(always)]
    fn into(self) -> [Vector4<T>; 4] {
        [self.v1, self.v2, self.v3, self.v4]
    }
}

impl<T: FloatNum> Add<Matrix4<T>> for Matrix4<T> {
    type Output = Self;

    #[inline(always)]
    fn add(self, m: Self) -> Self::Output {
        Self {
            v1: self.v1 + m.v1,
            v2: self.v2 + m.v2,
            v3: self.v3 + m.v3,
            v4: self.v4 + m.v4,
        }
    }
}

impl<T: FloatNum> AddAssign<Matrix4<T>> for Matrix4<T> {
    #[inline(always)]
    fn add_assign(&mut self, m: Self) {
        self.v1 += m.v1;
        self.v2 += m.v2;
        self.v3 += m.v3;
        self.v4 += m.v4;
    }
}

impl<T: FloatNum> Sub<Matrix4<T>> for Matrix4<T> {
    type Output = Self;

    #[inline(always)]
    fn sub(self, m: Self) -> Self::Output {
        Self {
            v1: self.v1 - m.v1,
            v2: self.v2 - m.v2,
            v3: self.v3 - m.v3,
            v4: self.v4 - m.v4,
        }
    }
}

impl<T: FloatNum> SubAssign<Matrix4<T>> for Matrix4<T> {
    #[inline(always)]
    fn sub_assign(&mut self, m: Self) {
        self.v1 -= m.v1;
        self.v2 -= m.v2;
        self.v3 -= m.v3;
        self.v4 -= m.v4;
    }
}

impl<T: FloatNum> Div<Matrix4<T>> for Matrix4<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, m: Self) -> Self::Output {
        self * m.inverse()
    }
}

impl<T: FloatNum> DivAssign<Matrix4<T>> for Matrix4<T> {
    #[inline(always)]
    fn div_assign(&mut self, m: Self) {
        *self *= m.inverse()
    }
}

impl<T: FloatNum> Div<T> for Matrix4<T> {
    type Output = Self;

    #[inline(always)]
    fn div(self, s: T) -> Self::Output {
        Self {
            v1: self.v1 / s,
            v2: self.v2 / s,
            v3: self.v3 / s,
            v4: self.v4 / s,
        }
    }
}

impl<T: FloatNum> DivAssign<T> for Matrix4<T> {
    #[inline(always)]
    fn div_assign(&mut self, s: T) {
        self.v1 /= s;
        self.v2 /= s;
        self.v3 /= s;
        self.v4 /= s;
    }
}

impl<T: FloatNum> Mul<Matrix4<T>> for Matrix4<T> {
    type Output = Self;

    fn mul(self, m: Self) -> Self::Output {
        Self {
            v1: Vector4::new(
                self.v1.x * m.v1.x + self.v1.y * m.v2.x + self.v1.z * m.v3.x + self.v1.w * m.v4.x,
                self.v1.x * m.v1.y + self.v1.y * m.v2.y + self.v1.z * m.v3.y + self.v1.w * m.v4.y,
                self.v1.x * m.v1.z + self.v1.y * m.v2.z + self.v1.z * m.v3.z + self.v1.w * m.v4.z,
                self.v1.x * m.v1.w + self.v1.y * m.v2.w + self.v1.z * m.v3.w + self.v1.w * m.v4.w,
            ),
            v2: Vector4::new(
                self.v2.x * m.v1.x + self.v2.y * m.v2.x + self.v2.z * m.v3.x + self.v2.w * m.v4.x,
                self.v2.x * m.v1.y + self.v2.y * m.v2.y + self.v2.z * m.v3.y + self.v2.w * m.v4.y,
                self.v2.x * m.v1.z + self.v2.y * m.v2.z + self.v2.z * m.v3.z + self.v2.w * m.v4.z,
                self.v2.x * m.v1.w + self.v2.y * m.v2.w + self.v2.z * m.v3.w + self.v2.w * m.v4.w,
            ),
            v3: Vector4::new(
                self.v3.x * m.v1.x + self.v3.y * m.v2.x + self.v3.z * m.v3.x + self.v3.w * m.v4.x,
                self.v3.x * m.v1.y + self.v3.y * m.v2.y + self.v3.z * m.v3.y + self.v3.w * m.v4.y,
                self.v3.x * m.v1.z + self.v3.y * m.v2.z + self.v3.z * m.v3.z + self.v3.w * m.v4.z,
                self.v3.x * m.v1.w + self.v3.y * m.v2.w + self.v3.z * m.v3.w + self.v3.w * m.v4.w,
            ),
            v4: Vector4::new(
                self.v4.x * m.v1.x + self.v4.y * m.v2.x + self.v4.z * m.v3.x + self.v4.w * m.v4.x,
                self.v4.x * m.v1.y + self.v4.y * m.v2.y + self.v4.z * m.v3.y + self.v4.w * m.v4.y,
                self.v4.x * m.v1.z + self.v4.y * m.v2.z + self.v4.z * m.v3.z + self.v4.w * m.v4.z,
                self.v4.x * m.v1.w + self.v4.y * m.v2.w + self.v4.z * m.v3.w + self.v4.w * m.v4.w,
            ),
        }
    }
}

impl<T: FloatNum> MulAssign<Matrix4<T>> for Matrix4<T> {
    #[inline(always)]
    fn mul_assign(&mut self, m: Self) {
        let v1 = Vector4::new(
            self.v1.x * m.v1.x + self.v1.y * m.v2.x + self.v1.z * m.v3.x + self.v1.w * m.v4.x,
            self.v1.x * m.v1.y + self.v1.y * m.v2.y + self.v1.z * m.v3.y + self.v1.w * m.v4.y,
            self.v1.x * m.v1.z + self.v1.y * m.v2.z + self.v1.z * m.v3.z + self.v1.w * m.v4.z,
            self.v1.x * m.v1.w + self.v1.y * m.v2.w + self.v1.z * m.v3.w + self.v1.w * m.v4.w,
        );
        let v2 = Vector4::new(
            self.v2.x * m.v1.x + self.v2.y * m.v2.x + self.v2.z * m.v3.x + self.v2.w * m.v4.x,
            self.v2.x * m.v1.y + self.v2.y * m.v2.y + self.v2.z * m.v3.y + self.v2.w * m.v4.y,
            self.v2.x * m.v1.z + self.v2.y * m.v2.z + self.v2.z * m.v3.z + self.v2.w * m.v4.z,
            self.v2.x * m.v1.w + self.v2.y * m.v2.w + self.v2.z * m.v3.w + self.v2.w * m.v4.w,
        );
        let v3 = Vector4::new(
            self.v3.x * m.v1.x + self.v3.y * m.v2.x + self.v3.z * m.v3.x + self.v3.w * m.v4.x,
            self.v3.x * m.v1.y + self.v3.y * m.v2.y + self.v3.z * m.v3.y + self.v3.w * m.v4.y,
            self.v3.x * m.v1.z + self.v3.y * m.v2.z + self.v3.z * m.v3.z + self.v3.w * m.v4.z,
            self.v3.x * m.v1.w + self.v3.y * m.v2.w + self.v3.z * m.v3.w + self.v3.w * m.v4.w,
        );
        let v4 = Vector4::new(
            self.v4.x * m.v1.x + self.v4.y * m.v2.x + self.v4.z * m.v3.x + self.v4.w * m.v4.x,
            self.v4.x * m.v1.y + self.v4.y * m.v2.y + self.v4.z * m.v3.y + self.v4.w * m.v4.y,
            self.v4.x * m.v1.z + self.v4.y * m.v2.z + self.v4.z * m.v3.z + self.v4.w * m.v4.z,
            self.v4.x * m.v1.w + self.v4.y * m.v2.w + self.v4.z * m.v3.w + self.v4.w * m.v4.w,
        );

        self.v1 = v1;
        self.v2 = v2;
        self.v3 = v3;
        self.v4 = v4;
    }
}

impl<T: FloatNum> Mul<T> for Matrix4<T> {
    type Output = Self;

    #[inline(always)]
    fn mul(self, s: T) -> Self::Output {
        Self {
            v1: self.v1 * s,
            v2: self.v2 * s,
            v3: self.v3 * s,
            v4: self.v4 * s,
        }
    }
}

impl<T: FloatNum> MulAssign<T> for Matrix4<T> {
    #[inline(always)]
    fn mul_assign(&mut self, s: T) {
        self.v1 *= s;
        self.v2 *= s;
        self.v3 *= s;
        self.v4 *= s;
    }
}

impl Mul<Mat4> for f32 {
    type Output = Mat4;

    #[inline(always)]
    fn mul(self, m: Mat4) -> Self::Output {
        m * self
    }
}

impl Mul<DMat4> for f64 {
    type Output = DMat4;

    #[inline(always)]
    fn mul(self, m: DMat4) -> Self::Output {
        m * self
    }
}

impl<T: FloatNum> Mul<Vector4<T>> for Matrix4<T> {
    type Output = Vector4<T>;

    #[inline(always)]
    fn mul(self, v: Vector4<T>) -> Self::Output {
        Vector4::new(self.v1 * v, self.v2 * v, self.v3 * v, self.v4 * v)
    }
}

impl<T: FloatNum> Mul<Vector3<T>> for Matrix4<T> {
    type Output = Vector3<T>;

    #[inline(always)]
    fn mul(self, v: Vector3<T>) -> Self::Output {
        let v4 = Vector4::from(v);
        Vector4::new(self.v1 * v4, self.v2 * v4, self.v3 * v4, self.v4 * v4).into()
    }
}

impl<T: FloatNum> Neg for Matrix4<T> {
    type Output = Self;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        Self {
            v1: -self.v1,
            v2: -self.v2,
            v3: -self.v3,
            v4: -self.v4,
        }
    }
}

impl<T: FloatNum> Index<usize> for Matrix4<T> {
    type Output = Vector4<T>;

    #[inline(always)]
    fn index(&self, index: usize) -> &Self::Output {
        match index {
            0 => &self.v1,
            1 => &self.v2,
            2 => &self.v3,
            3 => &self.v4,
            _ => panic!("out of range"),
        }
    }
}
