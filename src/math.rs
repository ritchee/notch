// TODO:
// - implement Ord for vec and quat
// - reimplement matrix multiplication once SIMD is included
use num_traits::*;
use std::fmt;
use std::ops::*;

pub mod mat2;
pub mod mat3;
pub mod mat4;
pub mod num;
pub mod quat;
pub mod transform2;
pub mod transform3;
pub mod vec2;
pub mod vec3;
pub mod vec4;

pub use mat2::*;
pub use mat3::*;
pub use mat4::*;
pub use num::*;
pub use quat::*;
pub use transform2::*;
pub use transform3::*;
pub use vec2::*;
pub use vec3::*;
pub use vec4::*;

/// linear interpolation
/// `f` is from
/// `t` is to
/// `s` is scale, must be between 0.0 and 1.0
pub fn lerp<V, N>(f: V, t: V, s: N) -> V
where
    V: Copy + Clone + Add<Output = V> + Sub<Output = V> + Mul<N, Output = V>,
    N: PrimNum,
{
    f + (t - f) * s
}

/// bilinear interpolation
/// similar to lerp but now on two paths
pub fn bilerp<V, N>(f1: V, t1: V, f2: V, t2: V, s1: N, s2: N) -> V
where
    V: Copy + Clone + Add<Output = V> + Sub<Output = V> + Mul<N, Output = V>,
    N: PrimNum,
{
    let f = f1 + (t1 - f1) * s1;
    let t = f2 + (t2 - f2) * s1;
    f + (t - f) * s2
}
