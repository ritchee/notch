use cubes::*;
use macroquad::prelude::*;

fn window_conf() -> Conf {
    Conf {
        window_title: "rigidbodies".to_owned(),
        window_width: 1920,
        window_height: 1080,
        ..Default::default()
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut pitch: f32 = -RAD_UNIT * 15.0;
    let mut yaw: f32 = -RAD_UNIT * 135.0;
    let fragment_shader = DEFAULT_FRAGMENT_SHADER.to_string();
    let vertex_shader = DEFAULT_VERTEX_SHADER.to_string();

    let pipeline_params = PipelineParams {
        depth_write: true,
        depth_test: Comparison::LessOrEqual,
        ..Default::default()
    };

    let material = load_material(
        &vertex_shader,
        &fragment_shader,
        MaterialParams {
            pipeline_params,
            ..Default::default()
        },
    )
    .unwrap();

    let mut camera = Camera3D {
        position: vec3(15., 15., 15.),
        up: vec3(0., 1., 0.),
        target: vec3(0., 0., 0.),
        ..Default::default()
    };

    loop {
        if is_key_pressed(KeyCode::Escape) {
            break;
        }
        handle_camera(&mut camera, &mut pitch, &mut yaw);

        clear_background(GRAY);

        set_camera(&camera);

        draw_grid(
            20,
            1.,
            Color::new(0.55, 0.55, 0.55, 0.75),
            Color::new(0.75, 0.75, 0.75, 0.75),
        );

        gl_use_material(material);
        draw_cube(vec3(0.0, 0.0, 0.0), vec3(1.0, 1.0, 1.0), None, BLUE);
        gl_use_default_material();

        draw_line_3d(vec3(0.5, 0.5, 0.5), vec3(-0.5, 0.5, 0.5), BLACK);
        draw_line_3d(vec3(-0.5, 0.5, 0.5), vec3(-0.5, -0.5, 0.5), BLACK);
        draw_line_3d(vec3(-0.5, -0.5, 0.5), vec3(0.5, -0.5, 0.5), BLACK);
        draw_line_3d(vec3(0.5, -0.5, 0.5), vec3(0.5, 0.5, 0.5), BLACK);

        draw_line_3d(vec3(0.5, 0.5, -0.5), vec3(-0.5, 0.5, -0.5), BLACK);
        draw_line_3d(vec3(-0.5, 0.5, -0.5), vec3(-0.5, -0.5, -0.5), BLACK);
        draw_line_3d(vec3(-0.5, -0.5, -0.5), vec3(0.5, -0.5, -0.5), BLACK);
        draw_line_3d(vec3(0.5, -0.5, -0.5), vec3(0.5, 0.5, -0.5), BLACK);

        draw_line_3d(vec3(0.5, 0.5, 0.5), vec3(0.5, 0.5, -0.5), BLACK);
        draw_line_3d(vec3(-0.5, 0.5, 0.5), vec3(-0.5, 0.5, -0.5), BLACK);
        draw_line_3d(vec3(-0.5, -0.5, 0.5), vec3(-0.5, -0.5, -0.5), BLACK);
        draw_line_3d(vec3(0.5, -0.5, 0.5), vec3(0.5, -0.5, -0.5), BLACK);

        set_default_camera();

        next_frame().await
    }
}
