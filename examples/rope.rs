use egui::{containers::Frame, epaint::*, plot::*};
use macroquad::prelude::*;
use notch::dynamics::{constraint::*, force::*, particle::*};
use notch::math;
use std::default::*;

const REST_LEN: f32 = 50.0;
const MASS: f32 = 10.0;
const KS: f32 = 1000.0;
const KD: f32 = 200.0;
const PI: f32 = std::f32::consts::PI;
const G_ACCEL: f32 = PI * PI;

fn window_conf() -> Conf {
    Conf {
        window_title: "rope".to_owned(),
        window_width: 1920,
        window_height: 1080,
        ..Default::default()
    }
}

struct Graph {
    clock: f64,
    y: Vec<[f64; 2]>,
}

impl Graph {
    fn new() -> Self {
        Self {
            clock: 0.0,
            y: Vec::new(),
        }
    }

    fn update(&mut self, y: f32, tick: f32) {
        // Update the value of y and append it to the history
        if self.y.len() > 200 {
            // Keep only the last 100 values in history
            self.y.remove(0);
        }

        self.clock += tick as f64;
        let clock = self.clock;
        self.y.push([clock, y as f64]);
    }

    fn reset(&mut self) {
        self.y.truncate(self.y.len());
    }

    fn plot(&self, ui: &mut egui::Ui) {
        let points1 = Points::new(self.y.clone()).color(egui::Color32::LIGHT_BLUE);

        Plot::new("Graph")
            .view_aspect(1.0)
            .include_y(0.1)
            .show_x(true)
            .show_y(true)
            .width(300.0)
            .show(ui, |plot_ui| {
                plot_ui.points(points1);
            });
    }
}

fn gen_particles(mut begin: math::Vec3) -> Vec<Particle> {
    let mut ps: Vec<Particle> = Vec::new();
    let x = begin.x;

    for i in 0..10 {
        begin.x = x + REST_LEN * i as f32;
        ps.push(Particle::new(begin, MASS));
    }

    ps[0].set_free();

    ps
}

#[macroquad::main(window_conf)]
async fn main() {
    let g = G_ACCEL;
    let y = math::Vec3::y();

    let w = screen_width();
    let mut sys = ParticleSystem::new();
    let spring = MassSpring::new(KS, KD, REST_LEN);
    let joint = Joint::new(REST_LEN, math::Vec3::new(w / 2.0, 10.0, 0.0));
    let mut dummy = Particle::new(math::Vec3::new(REST_LEN / 2.0, 1.0, 0.0), 10.0);

    let mut graph = Graph::new();
    let mut dt: f32;
    let mut grabbed = false;
    let mut grabbed_i: usize = 0;
    let mut r_prev = math::Vec3::zero();

    let mut indices = Vec::new();
    let mut ps = gen_particles(math::Vec3::new(w / 2.0, 10.0, 0.0));

    for i in 0..ps.len() {
        indices.push(i);
        if i > 0 && i < ps.len() - 1 {
            indices.push(i)
        }
    }

    sys.borrow_mut().append(&mut ps);
    sys.add_force("gravity", Gravity::new(g, y), None);

    loop {
        dt = get_frame_time();

        if is_key_pressed(KeyCode::Escape) {
            break;
        }

        if is_mouse_button_down(MouseButton::Left) {
            let p_ref = sys.get_mut(grabbed_i);
            let (x, y) = mouse_position();

            if grabbed {
                p_ref.r.x = x;
                p_ref.r.y = y;
                p_ref.v = (p_ref.r - r_prev) / dt;
                r_prev = p_ref.r;
            } else {
                for (i, p) in sys.iter_mut().enumerate() {
                    if (math::Vec3::new(x, y, 0.0) - p.r).len2() < 100.0 {
                        p.set_free();
                        r_prev = p.r;
                        grabbed = true;
                        grabbed_i = i;
                        break;
                    }
                }
            }
        } else if is_mouse_button_released(MouseButton::Left) {
            grabbed = false;
            if grabbed_i > 0 {
                sys.get_mut(grabbed_i).reset();
            }
        }

        graph.update(sys.adapt_step(dt, &mut dummy), dt);
        sys.add_once_force(joint, Some(&indices));
        sys.add_once_force(spring, Some(&indices));
        sys.update(dt);

        clear_background(WHITE);

        egui_macroquad::ui(|egui_ctx| {
            let mut frame = Frame::default();
            frame.shadow = Shadow::NONE;
            frame.fill = Color32::BLACK;
            frame.stroke = Stroke::new(10.0, Color32::BLACK);

            egui::Window::new("Controller")
                .frame(frame)
                .resizable(false)
                .show(egui_ctx, |ui| {
                    ui.with_layout(
                        egui::Layout::top_down(egui::Align::Center).with_cross_justify(true),
                        |ui| {
                            if ui.button("reset").clicked() {
                                let mut new_ps = gen_particles(math::Vec3::new(w / 2.0, 10.0, 0.0));
                                sys.borrow_mut().clear();
                                sys.borrow_mut().append(&mut new_ps);
                                graph.reset();
                            }
                        },
                    );
                });

            // error graph
            egui::Window::new("Step size error")
                .frame(frame)
                .resizable(false)
                .show(egui_ctx, |ui| {
                    graph.plot(ui);
                });
        });

        let gravity = Gravity::new(g, y);
        sys.set_force("gravity", gravity);

        egui_macroquad::draw();

        let mut r_prev = sys[0].r;
        for i in 1..sys.len() {
            let r = sys[i].r;
            draw_line(r_prev.x, r_prev.y, r.x, r.y, 3.0, BLACK);
            r_prev = r;
        }

        for p in sys.iter() {
            draw_circle(p.r.x, p.r.y, 5.0, BLUE);
        }

        /*
        let rdir1 = (r2 - r1).normal();
        let rdir2 = (r3 - r2).normal();
        let rp1 = ((r2 - r1).len() - rest_len) / 2.0;
        let rp2 = ((r3 - r2).len() - rest_len) / 2.0;
        let rp1 = r1 + rdir1 * rp1;
        let rp2 = r2 + rdir2 * rp2;
        let re1 = rp1 + rdir1 * rest_len;
        let re2 = rp2 + rdir2 * rest_len;

        draw_line(r1.x, r1.y, r2.x, r2.y, 3.0, BLACK);
        draw_line(r2.x, r2.y, r3.x, r3.y, 3.0, BLACK);

        draw_line(re1.x, re1.y, rp1.x, rp1.y, 1.5, GREEN);
        draw_line(re2.x, re2.y, rp2.x, rp2.y, 1.5, GREEN);

        draw_circle(r1.x, r1.y, 10.0, BLUE);
        draw_circle(r2.x, r2.y, 10.0, BLUE);
        draw_circle(r3.x, r3.y, 10.0, BLUE);
        */

        next_frame().await
    }
}
