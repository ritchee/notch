//use egui::{containers::Frame, epaint::*, plot::*};
use macroquad::prelude::*;
use macroquad::prelude::{draw_circle, draw_line};
use notch::prelude::*;
use std::default::*;

const HEIGHT: f32 = 50.0;
const WIDTH: f32 = 20.0;
const MASS: f32 = 100.0;
const LINK_LEN: f32 = 50.0;

fn make_local_r() -> Vec<math::Vec3> {
    let mut sys: Vec<math::Vec3> = Vec::new();

    sys.push(math::Vec3::new(-HEIGHT / 3.0, 0.0, 0.0));
    sys.push(math::Vec3::new(HEIGHT / 3.0, 0.0, 0.0));

    sys.push(math::Vec3::new(-HEIGHT / 2.0, -WIDTH / 2.0, 0.0));
    sys.push(math::Vec3::new(HEIGHT / 2.0, -WIDTH / 2.0, 0.0));
    sys.push(math::Vec3::new(HEIGHT / 2.0, WIDTH / 2.0, 0.0));
    sys.push(math::Vec3::new(-HEIGHT / 2.0, WIDTH / 2.0, 0.0));

    sys
}

pub struct Chain {
    pub bodies: Vec<Body>,
    pub joint: Joint,
    pub spring: MassSpring,
    pub local_r: Vec<math::Vec3>,
}

impl Chain {
    pub fn new(w: f32) -> Chain {
        let begin = math::Vec3::new(w / 2.0, 10.0, 0.0);
        let mut end = begin + math::Vec3::new(LINK_LEN + HEIGHT / 3.0, 0.0, 0.0);
        let mut bodies: Vec<Body> = Vec::new();
        let mut spring = MassSpring::new(2000.0, 2000.0, LINK_LEN);
        spring.pivot = begin;
        let local_r = make_local_r();

        for _ in 0..6 {
            let mut body = Body::from_uniform(&local_r, MASS);
            body.r.translate(end);
            bodies.push(body);
            end.x += LINK_LEN + (HEIGHT / 3.0) * 2.0;
        }

        Self {
            bodies,
            joint: Joint::new(LINK_LEN, begin),
            spring,
            local_r,
        }
    }

    pub fn update(&mut self, dt: f32) {
        //self.rope.dt = dt;
        let g = Gravity::default();
        let drag = Drag::new(10.0);
        let mut sys: Vec<Vec<Particle>> = Vec::new();

        for b in self.bodies.iter() {
            let mut body_sys: Vec<Particle> = Vec::new();

            for r in self.local_r.iter() {
                body_sys.push(b.to_particle(*r, MASS));
            }

            sys.push(body_sys);
        }

        for psys in sys.iter_mut() {
            for p in psys.iter_mut() {
                g.accumulate(p);
                drag.accumulate(p);
            }
        }

        self.joint.accumulate(&mut sys[0][0]);
        //self.rope.accumulate_slice(&mut ends);
        self.spring.accumulate(&mut sys[0][0]);

        for i in 1..self.bodies.len() {
            let mut ends = [sys[i - 1][1], sys[i][0]];
            self.joint.accumulate_slice(&mut ends);
            //self.rope.accumulate_slice(&mut ends);
            self.spring.accumulate_slice(&mut ends);
            sys[i - 1][1] = ends[0];
            sys[i][0] = ends[1];
        }

        for i in 0..self.bodies.len() {
            for p in sys[i].iter() {
                self.bodies[i].add_force(p.force, p.r);
            }
        }

        for b in self.bodies.iter_mut() {
            b.integrate(dt);
        }
    }

    pub fn draw(&self) {
        let mut sys: Vec<math::Vec3> = Vec::new();
        let mut prev_r = self.joint.pivot;

        for i in 0..self.bodies.len() {
            for r in self.local_r.iter() {
                sys.push(self.bodies[i].transform(*r));
            }

            draw_line(sys[2].x, sys[2].y, sys[3].x, sys[3].y, 3.0, BLACK);
            draw_line(sys[3].x, sys[3].y, sys[4].x, sys[4].y, 3.0, BLACK);
            draw_line(sys[4].x, sys[4].y, sys[5].x, sys[5].y, 3.0, BLACK);
            draw_line(sys[5].x, sys[5].y, sys[2].x, sys[2].y, 3.0, BLACK);

            draw_line(prev_r.x, prev_r.y, sys[0].x, sys[0].y, 3.0, BLACK);

            for r in sys.iter() {
                draw_circle(r.x, r.y, 3.0, BLUE);
            }

            prev_r = sys[1];

            sys.clear();
        }
    }
}
fn window_conf() -> Conf {
    Conf {
        window_title: "test".to_owned(),
        window_width: 1920,
        window_height: 1080,
        ..Default::default()
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut chain = Chain::new(screen_width());

    loop {
        let dt = get_frame_time();

        if is_key_pressed(KeyCode::Escape) {
            break;
        }

        chain.update(dt);

        clear_background(WHITE);

        chain.draw();

        next_frame().await
    }
}
