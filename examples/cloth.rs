use egui::{containers::Frame, epaint::*, plot::*};
use macroquad::prelude::*;
use notch::dynamics::{force::*, particle::*};
use notch::math;
use std::default::*;

const REST_LEN: f32 = 50.0;
const MASS: f32 = 10.0;
const KS: f32 = 300.0;
const KD: f32 = 50.0; // anything higher will break
const PI: f32 = std::f32::consts::PI;
const G_ACCEL: f32 = PI * PI;
const ROW: usize = 11;
const COL: usize = 11;
const DRAG: f32 = 5.0;

fn window_conf() -> Conf {
    Conf {
        window_title: "simple cloth".to_owned(),
        window_width: 1920,
        window_height: 1080,
        ..Default::default()
    }
}

struct Graph {
    clock: f64,
    history: Vec<[f64; 2]>,
}

impl Graph {
    fn new() -> Self {
        Self {
            clock: 0.0,
            history: Vec::new(),
        }
    }

    fn update(&mut self, y: f32, tick: f32) {
        // Update the value of y and append it to the history
        if self.history.len() > 200 {
            // Keep only the last 100 values in history
            self.history.remove(0);
        }

        self.clock += tick as f64;
        let clock = self.clock;
        self.history.push([clock, y as f64]);
    }

    fn reset(&mut self) {
        self.history = Vec::new();
    }

    fn plot(&self, ui: &mut egui::Ui) {
        let points = Points::new(self.history.clone()).color(egui::Color32::LIGHT_BLUE);

        Plot::new("Graph")
            .view_aspect(1.0)
            .include_y(0.1)
            .show_x(true)
            .show_y(true)
            .width(300.0)
            .show(ui, |plot_ui| plot_ui.points(points));
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut ks = KS;
    let mut cv = DRAG;
    let mut rest_len = REST_LEN;
    let mut g = G_ACCEL;
    let g_dir = math::Vec3::new(0.0, 1.0, 0.0);
    let mut graph = Graph::new();
    let mut dt: f32;
    let mut m = MASS;
    let mut pause = false;

    let w = screen_width();
    let mut sys = ParticleSystem::new();
    let x_begin = (w - (REST_LEN * COL as f32)) / 2.0;
    let y_begin = 10.0;
    let p00 = Particle::new(math::Vec3::new(x_begin, y_begin, 0.0), MASS);
    let spring = MassSpring::new(KS, KD, REST_LEN);
    let mut r_prev = math::Vec3::zero();
    let mut grabbed = false;
    let mut initial_r: Vec<math::Vec3> = Vec::new();
    let mut indices: Vec<usize> = Vec::new();
    let mut affected: Vec<usize> = Vec::new();
    let mut corner_i: usize = 0;
    let mut dummy = Particle::new(math::Vec3::new(REST_LEN / 2.0, 1.0, 0.0), 10.0);

    for i in 0..ROW {
        for j in 0..COL {
            let mut pij = p00;
            pij.r.x += j as f32 * REST_LEN;
            pij.r.y += i as f32 * REST_LEN;
            indices.push(sys.push(pij));
            initial_r.push(pij.r);
        }
    }

    for i in 0..ROW {
        for j in 0..COL {
            affected.push(indices[i * COL + j]);
            if j != 0 && j != COL - 1 {
                affected.push(indices[i * COL + j]);
            }
        }
    }

    for i in 0..COL {
        for j in 0..ROW {
            affected.push(indices[j * COL + i]);
            if j != 0 && j != ROW - 1 {
                affected.push(indices[j * COL + i]);
            }
        }
    }

    sys.get_mut(0).set_free();
    sys.get_mut((ROW - 1) * COL).set_free();
    sys.get_mut(ROW * COL - 1).set_free();
    sys.get_mut(COL - 1).set_free();
    sys.add_force("spring", spring, Some(&affected));
    sys.add_force("gravity", Gravity::new(g, g_dir), None);
    sys.add_force("drag", Drag::new(DRAG), None);

    loop {
        dt = get_frame_time();

        if is_key_pressed(KeyCode::Escape) {
            break;
        }

        if is_key_pressed(KeyCode::Space) {
            pause = !pause;
        }

        if !pause {
            let (x, y) = mouse_position();

            if is_mouse_button_pressed(MouseButton::Right) {
                for p in sys.iter_mut() {
                    if (math::Vec3::new(x, y, 0.0) - p.r).len2() < 25.0 {
                        if p.free() {
                            p.reset();
                        } else {
                            p.set_free();
                        }
                        break;
                    }
                }
            }

            if is_mouse_button_down(MouseButton::Left) {
                if grabbed {
                    let pf_ref = sys.get_mut(corner_i);
                    pf_ref.r.x = x;
                    pf_ref.r.y = y;
                    pf_ref.v = (pf_ref.r - r_prev) / dt;
                    r_prev = pf_ref.r;
                } else {
                    for i in indices.iter() {
                        let pf_ref = sys.get_mut(*i);
                        if (math::Vec3::new(x, y, 0.0) - pf_ref.r).len2() < 25.0 {
                            r_prev = pf_ref.r;
                            grabbed = true;
                            corner_i = *i;
                            break;
                        }
                    }
                }
            } else if is_mouse_button_released(MouseButton::Left) {
                grabbed = false;
            }

            graph.update(sys.adapt_step(dt, &mut dummy), dt);
            sys.update(dt);
        }

        clear_background(WHITE);

        egui_macroquad::ui(|egui_ctx| {
            let mut frame = Frame::default();
            frame.shadow = Shadow::NONE;
            frame.fill = Color32::BLACK;
            frame.stroke = Stroke::new(10.0, Color32::BLACK);

            egui::Window::new("Controller")
                .frame(frame)
                .resizable(false)
                .show(egui_ctx, |ui| {
                    ui.label("Left click and hold to drag a point mass");
                    ui.label("Right click to pin/unpin a point mass");
                    ui.add(egui::Slider::new(&mut ks, 100.0..=1000.0).text("stiffness"));
                    ui.add(egui::Slider::new(&mut cv, 1.0..=50.0).text("viscous drag"));
                    ui.add(egui::Slider::new(&mut rest_len, 10.0..=100.0).text("rest length"));
                    ui.add(egui::Slider::new(&mut g, G_ACCEL..=100.0).text("gravity"));
                    ui.add(egui::Slider::new(&mut m, 10.0..=100.0).text("point mass"));
                    ui.with_layout(
                        egui::Layout::top_down(egui::Align::Center).with_cross_justify(true),
                        |ui| {
                            if ui.button("reset").clicked() {
                                for (i, p) in sys.iter_mut().enumerate() {
                                    p.r = initial_r[i];
                                    p.v = math::Vec3::zero();
                                }

                                graph.reset();
                            }

                            if !pause && ui.button("pause").clicked() {
                                pause = true;
                            }
                            if pause && ui.button("resume").clicked() {
                                pause = false;
                            }
                        },
                    );
                });

            // error graph
            egui::Window::new("Step size error")
                .frame(frame)
                .resizable(false)
                .show(egui_ctx, |ui| {
                    graph.plot(ui);
                });
        });

        if !pause {
            sys.set_force("spring", MassSpring::new(ks, KD, rest_len));
            sys.set_force("gravity", Gravity::new(g, g_dir));
            sys.set_force("drag", Drag::new(cv));
            for p in sys.iter_mut().filter(|p| !p.free()) {
                p.mass = m;
            }
        }

        egui_macroquad::draw();

        for i in 0..ROW {
            let mut r = sys[i * COL].r;
            for j in 1..COL {
                let rf = sys[i * COL + j].r;
                draw_line(r.x, r.y, rf.x, rf.y, 3.0, BLACK);
                r = rf;
            }
        }

        for j in 0..COL {
            let mut r = sys[j].r;
            for i in 1..ROW {
                let rf = sys[i * COL + j].r;
                draw_line(r.x, r.y, rf.x, rf.y, 3.0, BLACK);
                r = rf;
            }
        }

        for i in 1..ROW {
            for j in 1..COL {
                let r00 = sys[(i - 1) * COL + j - 1].r;
                let r11 = sys[i * COL + j].r;
                let r10 = sys[(i - 1) * COL + j].r;
                let r01 = sys[i * COL + j - 1].r;
                draw_line(r00.x, r00.y, r11.x, r11.y, 3.0, BLACK);
                draw_line(r10.x, r10.y, r01.x, r01.y, 3.0, BLACK);
            }
        }

        for p in sys.iter() {
            draw_circle(p.r.x, p.r.y, 5.0, BLUE);
        }

        next_frame().await
    }
}
