use egui::{containers::Frame, epaint::*, plot::*};
use macroquad::prelude::*;
use notch::dynamics::{force::*, particle::*};
use notch::math;
use std::default::*;

const REST_LEN: f32 = 100.0;
const MASS: f32 = 50.0;
const KS: f32 = 50.0;
const KD: f32 = 50.0;
const PI: f32 = std::f32::consts::PI;
const G_ACCEL: f32 = PI * PI;

fn window_conf() -> Conf {
    Conf {
        window_title: "pendulum".to_owned(),
        window_width: 1920,
        window_height: 1080,
        ..Default::default()
    }
}

struct Graph {
    clock: f64,
    y: Vec<[f64; 2]>,
}

impl Graph {
    fn new() -> Self {
        Self {
            clock: 0.0,
            y: Vec::new(),
        }
    }

    fn update(&mut self, y: f32, tick: f32) {
        // Update the value of y and append it to the history
        if self.y.len() > 200 {
            // Keep only the last 100 values in history
            self.y.remove(0);
        }

        self.clock += tick as f64;
        let clock = self.clock;
        self.y.push([clock, y as f64]);
    }

    fn reset(&mut self) {
        self.y.truncate(self.y.len());
    }

    fn plot(&self, ui: &mut egui::Ui) {
        let points1 = Points::new(self.y.clone()).color(egui::Color32::LIGHT_BLUE);

        Plot::new("Graph")
            .view_aspect(1.0)
            .include_y(0.1)
            .show_x(true)
            .show_y(true)
            .width(300.0)
            .show(ui, |plot_ui| {
                plot_ui.points(points1);
            });
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut ks = KS;
    let mut kd = KD;
    let mut rest_len = REST_LEN;
    let mut g = G_ACCEL;
    let mut m = MASS;
    let y = math::Vec3::y();

    let w = screen_width();
    let mut sys = ParticleSystem::new();
    let spring = MassSpring::new(ks, kd, REST_LEN);
    let mut p0 = Particle::new(math::Vec3::new(w as f32 / 2.0, 100.0, 0.0), MASS);
    p0.set_free();
    let p1 = Particle::new(math::Vec3::new(w as f32 / 2.0 + REST_LEN, 100.0, 0.0), MASS);
    let p2 = Particle::new(
        math::Vec3::new(w as f32 / 2.0 + REST_LEN * 2.0, 100.0, 0.0),
        MASS,
    );
    let mut dummy = Particle::new(math::Vec3::new(REST_LEN / 2.0, 1.0, 0.0), 10.0);

    let mut graph = Graph::new();
    let mut dt: f32;
    let mut grabbed = false;
    let mut grabbed_i: usize = 0;
    let mut r_prev = math::Vec3::zero();

    let mut indices = Vec::new();
    indices.push(sys.push(p0));
    let id = sys.push(p1);
    indices.push(id);
    indices.push(id);
    indices.push(sys.push(p2));
    sys.add_force("gravity", Gravity::new(g, y), None);
    sys.add_force("spring", spring, Some(&indices));

    loop {
        dt = get_frame_time();

        if is_key_pressed(KeyCode::Escape) {
            break;
        }

        if is_mouse_button_down(MouseButton::Left) {
            let p_ref = sys.get_mut(grabbed_i);
            let (x, y) = mouse_position();

            if grabbed {
                p_ref.r.x = x;
                p_ref.r.y = y;
                p_ref.v = (p_ref.r - r_prev) / dt;
                r_prev = p_ref.r;
            } else {
                for (i, p) in sys.iter_mut().enumerate() {
                    if (math::Vec3::new(x, y, 0.0) - p.r).len2() < 100.0 {
                        p.set_free();
                        r_prev = p.r;
                        grabbed = true;
                        grabbed_i = i;
                        break;
                    }
                }
            }
        } else if grabbed && is_mouse_button_released(MouseButton::Left) {
            grabbed = false;
            if grabbed_i > 0 {
                sys.get_mut(grabbed_i).reset();
            }
        }

        graph.update(sys.adapt_step(dt, &mut dummy), dt);
        sys.update(dt);

        let r1 = sys[0].r;
        let r2 = sys[1].r;
        let r3 = sys[2].r;

        clear_background(WHITE);

        egui_macroquad::ui(|egui_ctx| {
            let mut frame = Frame::default();
            frame.shadow = Shadow::NONE;
            frame.fill = Color32::BLACK;
            frame.stroke = Stroke::new(10.0, Color32::BLACK);

            egui::Window::new("Controller")
                .frame(frame)
                .resizable(false)
                .show(egui_ctx, |ui| {
                    ui.add(egui::Slider::new(&mut ks, 10.0..=100.0).text("stiffness"));
                    ui.add(egui::Slider::new(&mut kd, 10.0..=100.0).text("damping constant"));
                    ui.add(egui::Slider::new(&mut rest_len, 50.0..=200.0).text("rest length"));
                    ui.add(egui::Slider::new(&mut m, 1.0..=100.0).text("point mass"));
                    ui.add(egui::Slider::new(&mut g, G_ACCEL..=100.0).text("gravity"));
                    ui.with_layout(
                        egui::Layout::top_down(egui::Align::Center).with_cross_justify(true),
                        |ui| {
                            if ui.button("reset").clicked() {
                                sys.borrow_mut()[0] = p0;
                                sys.borrow_mut()[1] = p1;
                                sys.borrow_mut()[2] = p2;
                                graph.reset();
                            }
                        },
                    );
                });

            // error graph
            egui::Window::new("Step size error")
                .frame(frame)
                .resizable(false)
                .show(egui_ctx, |ui| {
                    graph.plot(ui);
                });
        });

        let spring = MassSpring::new(ks, kd, rest_len);
        let gravity = Gravity::new(g, y);
        sys.set_force("spring", spring);
        sys.set_force("gravity", gravity);
        for p in sys.iter_mut().filter(|p| !p.free()) {
            p.mass = m;
        }

        egui_macroquad::draw();

        let rdir1 = (r2 - r1).normal();
        let rdir2 = (r3 - r2).normal();
        let rp1 = ((r2 - r1).len() - rest_len) / 2.0;
        let rp2 = ((r3 - r2).len() - rest_len) / 2.0;
        let rp1 = r1 + rdir1 * rp1;
        let rp2 = r2 + rdir2 * rp2;
        let re1 = rp1 + rdir1 * rest_len;
        let re2 = rp2 + rdir2 * rest_len;

        draw_line(r1.x, r1.y, r2.x, r2.y, 3.0, BLACK);
        draw_line(r2.x, r2.y, r3.x, r3.y, 3.0, BLACK);

        draw_line(re1.x, re1.y, rp1.x, rp1.y, 1.5, GREEN);
        draw_line(re2.x, re2.y, rp2.x, rp2.y, 1.5, GREEN);

        draw_circle(r1.x, r1.y, 10.0, BLUE);
        draw_circle(r2.x, r2.y, 10.0, BLUE);
        draw_circle(r3.x, r3.y, 10.0, BLUE);

        next_frame().await
    }
}
