# About the project 
NOTCH is a Physics Engine that was the product of team NOTCH from the courses CSE 4316-4317 at UTA. The engine was a passion project, and now aims to be a alternative game physics solution in the Rust ecosystem.

## NOTE
The project currently not available on crates.io yet.

## Installation
```
git clone https://gitlab.com/ritchee/notch
git checkout 0.2.1

# Now put this in your project's Cargo.toml
[dependencies]
notch = { path = "path/to/notch" }
```

## Documentation
Since the project is not available on crates.io yet, the documentations can only be generated manually.

```
cd notch
cargo doc --no-deps
# Documentations are now located at target/notch
```

## Version
0.3.4

## License
GPLv2
